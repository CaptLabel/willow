function pie(elt1, elt2, elt3, elt4)
{
    new Chart(elt4, {
        type: 'pie',
        data: {
            datasets: [{
                data: [elt1, elt2, elt3],
                backgroundColor: [
                    '#1b5e20',
                    '#7cb342',
                    '#c8e6c9',
                ],
                hoverOffset: 4
            }],
        },
        options: {
            layout: {
                padding: 5
            },
            plugins: {
                legend: false
            },
        },
    });
}
function lineEnquiryPerDay(elt1, elt2, elt3)
{
    new Chart(elt3, {
        type: 'line',
        data: {
            datasets: [{
                data: elt2,
                fill: false,
                borderColor: '#9e9d24',
                tension: 0.1
            }],
            labels: elt1,
        },
        options: {
            plugins: {
                legend: false
            },
        },
    });
}
function barBiggestEnquiries(elt1, elt2, elt3)
{
    new Chart(elt1,{
        type: 'bar',
        data: {
            labels: elt2,
            datasets: [{
                axis: 'y',
                data: elt3,
                fill: false,
                backgroundColor: '#dcedc8',
                barPercentage: 0.4,
            }]
        },
        options: {
            plugins: {
                legend: false
            },
            indexAxis: 'y',
        },
    });
}
