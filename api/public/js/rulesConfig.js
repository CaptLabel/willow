function checkinBeforeCheckout(elt1, elt2)
{
    var date_in = new Date(elt1);
    var date_out = new Date(elt2);
    var ret = true;
    if (date_in >= date_out) {
        alert('Check in date must be before check out date.');
        ret = false;
    }

    return ret;
}

function moreThan10Nights(elt1, elt2)
{
    var date_in = new Date(elt1);
    var date_out = new Date(elt2);
    var date_diff = dateDiff(date_in, date_out);
    var ret = true;
    if (date_diff >= 10) {
        if (!confirm('You selected more than 10 nights, do you want to go ahead?')) {
            ret = false;
        }
    }

    return ret;
}

function dateInThePast(elt)
{
    var date_in = new Date(elt);
    var today = new Date();
    var ret = true;
    if (date_in < today) {
        if (!confirm('You selected dates in the past, do you want to go ahead?')) {
            ret = false;
        }
    }

    return ret;
}

function errorDateInThePast(elt)
{
    var date_in = new Date(elt);
    var today = new Date();
    var ret = true;
    if (date_in < today) {
        alert('You selected a date in the past, you need to select a future date.')
        ret = false;
    }

    return ret;
}

function dateFromBeforeDateUntil(elt1, elt2)
{
    var date_in = new Date(elt1);
    var date_out = new Date(elt2);
    var ret = true;
    if (date_in > date_out) {
        alert('Date from must be before Date until.')
        ret = false;
    }

    return ret;
}

function maxLengthBiggerMinLength(elt1, elt2)
{
    var min_length = +(elt1);
    var max_length = +(elt2);
    var ret = true;
    if (min_length > max_length && min_length > 0) {
        alert('Max length of stay must be bigger than min length of stay.')
        ret = false;
    }

    return ret;
}

function maxPaxBiggerMinPax(elt1, elt2)
{
    var min_pax = +(elt1);
    var max_pax = +(elt2);
    var ret = true;
    if (min_pax > max_pax && min_pax > 0) {
        alert('Max number of persons must be bigger than min number of persons.')
        ret = false;
    }

    return ret;
}

function maxOccBiggerMinOcc(elt1, elt2)
{
    var min_occ = +(elt1);
    var max_occ = +(elt2);
    var ret = true;
    if (min_occ > max_occ && min_occ > 0) {
        alert('Max occupancy must be bigger than min occupancy.')
        ret = false;
    }

    return ret;
}

function AgeMaxBiggerAgeMin(elt1, elt2)
{
    var age_min = +elt1;
    var age_max = +elt2;
    var ret = true;
    if (age_min > age_max || elt2 === undefined && age_min > 0) {
        alert('Max age range must be bigger than min age range.')
        ret = false;
    }

    return ret;

}