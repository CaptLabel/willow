<?php

namespace App\Tests\Func\Controller;

use App\Repository\LanguageRepository;
use App\Tests\Func\Login\LoginTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TemplateProposalControllerTest extends WebTestCase
{
    use LoginTrait;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\KernelBrowser
     */
    private KernelBrowser $client;

    /**
     * @return void
     */
    public function setUp(): void
    {
        $this->client = $this->logIn();
    }

//    /**
//     * @return void
//     */
//    public function testNew()
//    {
//        $crawlerNew = $this->client->request('GET', '/template_proposal/new');
//        $this->assertResponseIsSuccessful();
//        $nodeButton = $crawlerNew->selectButton('Save');
//        $form = $nodeButton->form();
//        $uuid = uniqid();
//        $form['template_proposal[active]'] = '1';
//        $form['template_proposal[label]'] = "filter with label : $uuid";
//        $form['template_proposal[description]'] = 'admin';
//        $form['template_proposal[pax_min]'] = '1';
//        $brandRepository = static::$container->get(LanguageRepository::class);
//        $brand = $brandRepository->findOneBy([], ['id' => 'asc']);
//        $ageRepository = static::$container->get(AgeRepository::class);
//        $ages = $ageRepository->findByBrand($brand->getId());
//        $form['filter[ageMin]'] = $ages[0]->getId();
//        $form['filter[ageMax]'] = $ages[1]->getId();
//        $date = new \DateTime();
//        $dateMin = clone $date;
//        $dateMin = $dateMin->modify('+1 day');
//        $form['filter[dateMin]'] = $dateMin->format('Y-m-d');
//        $dateMax = clone $date;
//        $dateMax = $dateMax->modify('+4 day');
//        $form['filter[dateMax]'] = $dateMax->format('Y-m-d');
//        $this->client->submit($form);
//        $this->client->followRedirect();
//        $this->assertResponseIsSuccessful();
//        $this->assertSelectorTextContains('body', "filter with label : $uuid");
//    }
}