<?php

namespace App\Tests\Func\Controller;

use App\Repository\AgeRepository;
use App\Repository\BrandRepository;
use App\Repository\FilterRepository;
use App\Tests\Func\Login\LoginTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FilterControllerTest extends WebTestCase
{
    use LoginTrait;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\KernelBrowser
     */
    private KernelBrowser $client;

    /**
     * @return void
     */
    public function setUp(): void
    {
        $this->client = $this->logIn();
    }

    /**
     * @return void
     */
    public function testIndex()
    {
        $crawlerPage = $this->client->request('GET', '/revenue-configuration');

        $this->assertSame('Revenue configuration', $crawlerPage->filter('h3')->text());
//        $this->assertSame('no records found', $crawlerPage->filter('td')->eq(0)->text());
    }

    /**
     * @return void
     */
    public function testNew()
    {
        $crawlerNew = $this->client->request('GET', '/filter/new');
        $this->assertResponseIsSuccessful();
        $nodeButton = $crawlerNew->selectButton('Save');
        $form = $nodeButton->form();
        $uuid = uniqid();
        $form['filter[active]'] = '1';
        $form['filter[label]'] = "filter with label : $uuid";
        $form['filter[description]'] = 'admin';
        $form['filter[pax_min]'] = '1';
        $form['filter[pax_max]'] = '2';
        $form['filter[night_min]'] = '1';
        $form['filter[night_max]'] = '2';
        $form['filter[occupancyMin]'] = '1';
        $form['filter[occupancyMax]'] = '2';
        $brandRepository = static::$container->get(BrandRepository::class);
        $brand = $brandRepository->findOneBy([], ['id' => 'asc']);
        $ageRepository = static::$container->get(AgeRepository::class);
        $ages = $ageRepository->findByBrand($brand->getId());
        $form['filter[ageMin]'] = $ages[0]->getId();
        $form['filter[ageMax]'] = $ages[1]->getId();
        $date = new \DateTime();
        $dateMin = clone $date;
        $dateMin = $dateMin->modify('+1 day');
        $form['filter[dateMin]'] = $dateMin->format('Y-m-d');
        $dateMax = clone $date;
        $dateMax = $dateMax->modify('+4 day');
        $form['filter[dateMax]'] = $dateMax->format('Y-m-d');
        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('body', "filter with label : $uuid");
    }

    /**
     * @return void
     */
    public function testEdit(): void
    {
        $filterRepository = static::$container->get(FilterRepository::class);
        $lastFilter = $filterRepository->findOneBy([], ['id' => 'desc']);
        $crawlerPage = $this->client->request('GET', "/filter/{$lastFilter->getId()}/edit");
        $this->assertResponseIsSuccessful();
        $nodeButton = $crawlerPage->selectButton('Update');
        $form = $nodeButton->form();
        $form['filter[label]'] = "label edited";
        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('body', "label edited");
    }

    /**
     * @return void
     */
    public function testDelete(): void
    {
        $crawlerPage = $this->client->request('GET', '/revenue-configuration');
        $this->assertResponseIsSuccessful();
        $nodeButton = $crawlerPage->filter('#general_filter tr')->last()->selectButton('Delete');
        $form = $nodeButton->form();
        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextNotContains('body', "label edited");
    }
}
