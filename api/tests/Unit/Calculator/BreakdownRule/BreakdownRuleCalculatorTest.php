<?php

namespace App\Tests\Unit\Calculator\BreakdownRule;

use App\Calculator\BreakdownRule\BreakdownRuleCalculator;
use App\Entity\Enquiry;
use App\Repository\BreakdownRuleAddRepository;
use App\Repository\BreakdownRuleRemainingRepository;
use App\Repository\BreakdownRuleTotalRepository;
use App\Tests\Unit\Provider\BreakdownRuleAddProvider;
use App\Tests\Unit\Provider\BreakdownRuleRemainingProvider;
use App\Tests\Unit\Provider\BreakdownRuleTotalProvider;
use App\Tests\Unit\Provider\RoomTypeProvider;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

class BreakdownRuleCalculatorTest extends TestCase
{
    /**
     * rule for total is winning cause there need to be at least 7 twin still available
     */
    public function test1(): void
    {
        $this->createCalculatorTest(
            ['paxNumber' => 47],
            [
                "add" => [
                    [
                        "room" => 'Twin',
                        "quantity" => 3,
                        "ratio" => null
                    ]
                ],
                "remaining" => [
                    [
                        "room" => 'Twin',
                        "percentage" => 50
                    ]
                ],
                "total" => [
                    [
                        "room" => 'Twin',
                        "percentage" => 32
                    ]
                ]
            ],
            [
                ["Twin", 2, 20, true, null],
            ],
            [
                "Twin" => 12,
            ],
            [
                "Twin" => [
                    "available" => 2,
                    "picked" => 3,
                ]
            ]
        );
    }

    /**
     * rule for total is winning cause there need to be at least 7 twin still available with ratio
     */
    public function test2(): void
    {
        $this->createCalculatorTest(
            ['paxNumber' => 47],
            [
                "add" => [
                    [
                        "room" => 'Twin',
                        "quantity" => null,
                        "ratio" => 15
                    ]
                ],
                "remaining" => [
                    [
                        "room" => 'Twin',
                        "percentage" => 50
                    ]
                ],
                "total" => [
                    [
                        "room" => 'Twin',
                        "percentage" => 32
                    ]
                ]
            ],
            [
                ["Twin", 2, 20, true, null],
            ],
            [
                "Twin" => 12,
            ],
            [
                "Twin" => [
                    "available" => 2,
                    "picked" => 3,
                ]
            ]
        );
    }

    /**
     * rule for remaining is winning cause there need to be at least 6 twin still available
     */
    public function test3(): void
    {
        $this->createCalculatorTest(
            ['paxNumber' => 47],
            [
                "add" => [
                    [
                        "room" => 'Twin',
                        "quantity" => 2,
                        "ratio" => null
                    ]
                ],
                "remaining" => [
                    [
                        "room" => 'Twin',
                        "percentage" => 51
                    ]
                ],
                "total" => [
                    [
                        "room" => 'Twin',
                        "percentage" => 9
                    ]
                ]
            ],
            [
                ["Twin", 2, 20, true, null],
            ],
            [
                "Twin" => 12,
            ],
            [
                "Twin" => [
                    "available" => 3,
                    "picked" => 2,
                ]
            ]
        );
    }

    /**
     * cannot give twin due to total rule
     */
    public function test4(): void
    {
        $this->createCalculatorTest(
            ['paxNumber' => 47],
            [
                "add" => [
                    [
                        "room" => 'Twin',
                        "quantity" => 1,
                        "ratio" => null
                    ]
                ],
                "total" => [
                    [
                        "room" => 'Twin',
                        "percentage" => 30
                    ]
                ]
            ],
            [
                ["Twin", 2, 20, true, null],
            ],
            [
                "Twin" => 5,
            ],
            [
                "Twin" => [
                    "available" => 0,
                    "picked" => 0,
                ]
            ]
        );
    }

    /**
     * can only give 2 twin due to remaining rules
     */
    public function test5(): void
    {
        $this->createCalculatorTest(
            ['paxNumber' => 47],
            [
                "add" => [
                    [
                        "room" => 'Twin',
                        "quantity" => 3,
                        "ratio" => null
                    ]
                ],
                "remaining" => [
                    [
                        "room" => 'Twin',
                        "percentage" => 50
                    ]
                ]
            ],
            [
                ["Twin", 2, 20, true, null],
            ],
            [
                "Twin" => 5,
            ],
            [
                "Twin" => [
                    "available" => 0,
                    "picked" => 2,
                ]
            ]
        );
    }

    private function createCalculatorTest(
        array $paxNumber,
        array $breakdownRules,
        array $roomtypes,
        array $availabilies,
        array $expectedResult
    ) {
        $roomTypes = (new RoomTypeProvider())->get($roomtypes);

        $breakdownRuleAdds = [];
        if (array_key_exists('add', $breakdownRules)) {
            foreach ($breakdownRules['add'] as $add) {
                foreach ($roomTypes as $room) {
                    if ($room->getLabel() === $add['room']) {
                        $breakdownRuleAdds[] = (new BreakdownRuleAddProvider())->get($room, $add);
                    }
                }
            }
        }

        $breakdownRuleRemaining = [];
        if (array_key_exists('remaining', $breakdownRules)) {
            foreach ($breakdownRules['remaining'] as $add) {
                foreach ($roomTypes as $room) {
                    if ($room->getLabel() === $add['room']) {
                        $breakdownRuleRemaining[] = (new BreakdownRuleRemainingProvider())->get($room, $add);
                    }
                }
            }
        }

        $breakdownRuleTotal = [];
        if (array_key_exists('total', $breakdownRules)) {
            foreach ($breakdownRules['total'] as $add) {
                foreach ($roomTypes as $room) {
                    if ($room->getLabel() === $add['room']) {
                        $breakdownRuleTotal[] = (new BreakdownRuleTotalProvider())->get($room, $add);
                    }
                }
            }
        }

        $enquiryRepository = $this->createMock(BreakdownRuleAddRepository::class);
        $enquiryRepository->expects($this->any())
            ->method('findAddRules')
            ->willReturn(
                $breakdownRuleAdds
            );


        $r2 = $this->createMock(BreakdownRuleRemainingRepository::class);
        $r2->expects($this->any())
            ->method('findRemainingRules')
            ->willReturn(
                $breakdownRuleRemaining
            );

        $r3 = $this->createMock(BreakdownRuleTotalRepository::class);
        $r3->expects($this->any())
            ->method('findTotalRules')
            ->willReturn(
                $breakdownRuleTotal
            );

        $objectManager = $this->createMock(EntityManagerInterface::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls(
                $enquiryRepository,
                $r2,
                $r3
            );
        $breakdownRuleCalculator = new BreakdownRuleCalculator($objectManager);

        $enquiry = new Enquiry();
        $breakdownRuleCalculator->setEnquiry($enquiry);
        $breakdownRuleCalculator->calculate();
        $rp = 0;
        foreach ($breakdownRuleCalculator->getSaved() as $roomLabel => $calculators) {
            $calculatorsTransformed = [];
            foreach ($calculators as $calculator) {
                $calculator->setRoomAvailability($availabilies[$roomLabel]);
                $calculator->setPaxNumber($paxNumber['paxNumber']);
                $calculator->calculate();
                $calculatorsTransformed[] = $calculator;
            }
            uasort($calculatorsTransformed, function ($a, $b) {
                if ($a->getRoomsToRemove() == $b->getRoomsToRemove()) {
                    return 0;
                }
                return ($a->getRoomsToRemove() > $b->getRoomsToRemove()) ? -1 : 1;
            });
            $availabilies[$roomLabel] -= current($calculatorsTransformed)->getRoomsToRemove();
        }

        foreach ($breakdownRuleCalculator->getAdds() as $roomLabel => $calculators) {
            foreach ($calculators as $calculator) {
                $calculator->setRoomAvailability($availabilies[$roomLabel]);
                $calculator->setPaxNumber($paxNumber['paxNumber']);
                $calculator->calculate();
                $rp += $calculator->getRoomsPickedToAdd();
                $availabilies[$roomLabel] -= $calculator->getRoomsToRemove();
            }
        }

        foreach ($availabilies as $label => $quantity) {
            $this->assertEquals($expectedResult[$label]['available'], max($quantity, 0));
            $this->assertEquals($expectedResult[$label]['picked'], max($rp, 0));
        }
    }
}
