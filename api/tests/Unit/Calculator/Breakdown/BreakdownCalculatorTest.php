<?php

namespace App\Tests\Unit\Calculator\Breakdown;

use App\Calculator\Breakdown\BreakdownCalculator;
use App\Calculator\BreakdownRule\BreakdownRuleCalculator;
use App\Entity\Enquiry;
use App\Formater\ResultFormater;
use App\Repository\RoomTypeRepository;
use App\Tests\Unit\Provider\AvailabilityProvider;
use App\Tests\Unit\Provider\RoomTypeProvider;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

class BreakdownCalculatorTest extends TestCase
{
    const TEST_PRIORITY_10_14_16 = [
       "roomtypes" => [
           ["10BR", 10, 4, true, 1],
           ["14BR", 14, 3, true, 2],
           ["16BR", 16, 2, true, 3],
       ],
        "availabilities" => [
            [4, "10BR"],
            [3, "14BR"],
            [2, "16BR"],
        ]
    ];

    const TEST_2_PRIORITY_7_5 = [
        "roomtypes" => [
            ["7BR", 7, 3, true, 1],
            ["5BR", 5, 10, true, 2],
            ["15BR", 15, 2, true, 3],
        ],
        "availabilities" => [
            [4, "5BR"],
            [4, "7BR"],
            [4, "15BR"],
        ]
    ];

    const TEST_3_PRIORITY_5_7 = [
        "roomtypes" => [
            ["5BR", 5, 10, true, 1],
            ["7BR", 7, 3, true, 2],
            ["15BR", 15, 2, true, 3],
        ],
        "availabilities" => [
            [4, "7BR"],
            [4, "5BR"],
            [4, "15BR"],
        ]
    ];

    const TEST_3_PRIORITY_5_7_2 = [
        "roomtypes" => [
            ["5BR", 5, 10, true, 1],
            ["7BR", 7, 3, true, 2],
            ["16BR", 15, 2, true, 3],
        ],
        "availabilities" => [
            [4, "7BR"],
            [4, "5BR"],
            [4, "15BR"],
        ]
    ];

    const TEST_3_PRIORITY_5_7_3 = [
        "roomtypes" => [
            ["5BR", 5, 10, true, 1],
            ["7BR", 7, 3, true, 2],
            ["15BR", 15, 2, true, 3],
        ],
        "availabilities" => [
            [4, "7BR"],
            [4, "5BR"],
            [4, "15BR"],
        ]
    ];

    const TEST_PRIORITY_5_7 = [
        "roomtypes" => [
            ["5BR", 5, 10, true, 1],
            ["7BR", 7, 3, true, 2],
            ["15BR", 15, 2, true, 3],
        ],
        "availabilities" => [
            [4, "7BR"],
            [4, "5BR"],
            [4, "15BR"],
        ]
    ];

    const TEST_3_PRIORITY_15_5_7 = [
        "roomtypes" => [
            ["15BR", 15, 2, true, 1],
            ["5BR", 5, 10, true, 2],
            ["7BR", 7, 3, true, 3],
        ],
        "availabilities" => [
            [4, "7BR"],
            [4, "5BR"],
            [4, "15BR"],
        ]
    ];

    const TEST_AVAILATILITY_4_10 = [
        "roomtypes" => [
            ["4BR", 4, 2, true, 1],
            ["20BR", 20, 6, true, 3],
        ],
        "availabilities" => [
            [4, "4BR"],
            [1, "20BR"],
        ]
    ];

    const TEST_WITHOUT_PRIORITY = [
        "roomtypes" => [
            ["10BR", 10, 4, true, null],
            ["14BR", 14, 3, true, null],
            ["16BR", 16, 2, true, null],
        ],
        "availabilities" => [
            [4, "10BR"],
            [4, "14BR"],
            [6, "16BR"],
        ]
    ];

    const TEST_1_AVAILABILITY_4_6_7 = [
        "roomtypes" => [
            ["4BR", 4, 1, true, 1],
            ["6BR", 6, 4, true, 2],
            ["7BR", 7, 4, true, 3],
        ],
        "availabilities" => [
            [1, "4BR"],
            [4, "6BR"],
            [4, "7BR"],
        ]
    ];

    const TEST_1_PRIORITY_6_7_20 = [
        "roomtypes" => [
            ["6BR", 6, 4, true, 1],
            ["7BR", 7, 4, true, 2],
            ["20BR", 20, 4, true, 3],
        ],
        "availabilities" => [
            [4, "6BR"],
            [4, "7BR"],
            [4, "20BR"],
        ]
    ];

    const TEST_1_PRIORITY_7_6_20 = [
        "roomtypes" => [
            ["7BR", 7, 4, true, 1],
            ["6BR", 6, 4, true, 2],
            ["20BR", 20, 4, true, 3],
        ],
        "availabilities" => [
            [4, "7BR"],
            [4, "6BR"],
            [4, "20BR"],
        ]
    ];

    const TEST_1_PRIORITY_4_7_6_20 = [
        "roomtypes" => [
            ["4BR", 4, 3, true, 1],
            ["7BR", 7, 4, true, 2],
            ["6BR", 6, 4, true, 3],
            ["20BR", 20, 4, true, 4],
        ],
        "availabilities" => [
            [3, "4BR"],
            [7, "7BR"],
            [6, "6BR"],
            [2, "20BR"],
        ]
    ];

    const TEST_1_PRIORITY_20_4_6_7 = [
        "roomtypes" => [
            ["20BR", 20, 4, true, 4],
            ["4BR", 4, 3, true, 1],
            ["6BR", 6, 4, true, 3],
            ["7BR", 7, 4, true, 2],
        ],
        "availabilities" => [
            [1, "20BR"],
            [1, "4BR"],
            [3, "6BR"],
            [4, "7BR"],
        ]
    ];

    const TEST_2_PRIORITY_20_4_6_7 = [
        "roomtypes" => [
            ["20BR", 20, 4, true, 1],
            ["4BR", 4, 3, true, 2],
            ["6BR", 6, 4, true, 3],
            ["7BR", 7, 4, true, 4],
        ],
        "availabilities" => [
            [4, "20BR"],
            [2, "4BR"],
            [10, "6BR"],
            [8, "7BR"],
        ]
    ];

    const TEST_1_PRIORITY_4_11_9_20 = [
        "roomtypes" => [
            ["4BR", 4, 3, true, 2],
            ["11BR", 11, 4, true, 1],
            ["9BR", 9, 3, true, 2],
            ["20BR", 20, 4, true, 1],
        ],
        "availabilities" => [
            [2, "4BR"],
            [2, "11BR"],
            [10, "9BR"],
            [5, "20BR"],
        ]
    ];

    const TEST_2_PRIORITY_4_11_9_20 = [
        "roomtypes" => [
            ["4BR", 4, 3, true, 2],
            ["11BR", 11, 4, true, 1],
            ["9BR", 9, 3, true, 2],
            ["20BR", 20, 4, true, 1],
        ],
        "availabilities" => [
            [10, "4BR"],
            [6, "11BR"],
            [10, "9BR"],
            [5, "20BR"],
        ]
    ];

    const TEST_DOUBLON_CAPACITY = [
        "roomtypes" => [
            ["10BR", 10, 4, true, 1],
            ["10BB", 10, 4, true, 1],
        ],
        "availabilities" => [
            [1, "10BR"],
            [1, "10BB"]
        ]
    ];

    const TEST_50 = [
        "roomtypes" => [
            ["50BR", 50, 4, true, 1],
        ],
        "availabilities" => [
            [1, "50BR"]
        ]
    ];

    const TEST_DOUBLON_ROOMPRIO_ROOMROUNDOWN = [
        "roomtypes" => [
            ["4BR", 4, 3, true, 2],
            ["4BB", 4, 4, true, 1],
        ],
        "availabilities" => [
            [10, "4BR"],
            [6, "4BB"],
        ]
    ];

    public function testEnquiry7paxTooMany(): void
    {
        $this->createCalculatorTest(
            7,
            self::TEST_PRIORITY_10_14_16,
            [
                '10BR' => '1.7',
                '14BR' => '0.0',
                '16BR' => '0.0'
            ]
        );
    }

    public function testEnquiry12paxTooManyBetween(): void
    {
        $this->createCalculatorTest(
            12,
            self::TEST_PRIORITY_10_14_16,
            [
                '10BR' => '0.0',
                '14BR' => '1.12',
                '16BR' => '0.0'
            ]
        );
    }

    public function testEnquiry18paxTooMany2rooms(): void
    {
        $this->createCalculatorTest(
            18,
            self::TEST_PRIORITY_10_14_16,
            [
                '10BR' => '2.18',
                '14BR' => '0.0',
                '16BR' => '0.0'
            ]
        );
    }

    public function testEnquiry20pax(): void
    {
        $this->createCalculatorTest(
            20,
            self::TEST_PRIORITY_10_14_16,
            [
                '10BR' => '2.20',
                '14BR' => '0.0',
                '16BR' => '0.0'
            ]
        );
    }

    public function testEnquiry22pax(): void
    {
        $this->createCalculatorTest(
            22,
            self::TEST_PRIORITY_10_14_16,
            [
                '10BR' => '1.9',
                '14BR' => '1.13',
                '16BR' => '0.0'
            ]
        );
    }

    public function testEnquiry28pax(): void
    {
        $this->createCalculatorTest(
            28,
            self::TEST_PRIORITY_10_14_16,
            [
                '10BR' => '3.28',
                '14BR' => '0.0',
                '16BR' => '0.0'
            ]
        );
    }

    public function testEnquiry32pax(): void
    {
        $this->createCalculatorTest(
            32,
            self::TEST_PRIORITY_10_14_16,
            [
                '10BR' => '2.19',
                '14BR' => '1.13',
                '16BR' => '0.0'
            ]
        );
    }

    public function testEnquiry48pax(): void
    {
        $this->createCalculatorTest(
            48,
            self::TEST_PRIORITY_10_14_16,
            [
                '10BR' => '2.20',
                '14BR' => '2.28',
                '16BR' => '0.0'
            ]
        );
    }

    public function testEnquiry60pax(): void
    {
        $this->createCalculatorTest(
            60,
            self::TEST_PRIORITY_10_14_16,
            [
                '14BR' => '1.14',
                '10BR' => '3.30',
                '16BR' => '1.16'
            ]
        );
    }

    public function testEnquiry82pax(): void
    {
        $this->createCalculatorTest(
            82,
            self::TEST_PRIORITY_10_14_16,
            [
                '10BR' => '4.40',
                '14BR' => '2.27',
                '16BR' => '1.15'
            ]
        );
    }

    public function testEnquiry8paxOdd(): void
    {
        $this->createCalculatorTest(
            8,
            self::TEST_3_PRIORITY_5_7,
            [
                '5BR' => '2.8',
                '7BR' => '0.0',
                '15BR' => '0.0'
            ]
        );
    }
    public function testEnquiry9paxOdd(): void
    {
        $this->createCalculatorTest(
            9,
            self::TEST_3_PRIORITY_5_7,
            [
                '5BR' => '2.9',
                '7BR' => '0.0',
                '15BR' => '0.0'
            ]
        );
    }

    public function testEnquiry13paxOdd(): void
    {
        $this->createCalculatorTest(
            13,
            self::TEST_2_PRIORITY_7_5,
            [
                '5BR' => '0.0',
                '7BR' => '2.13',
                '15BR' => '0.0'
            ]
        );
    }

    public function testEnquiry13paxPriority5(): void
    {
        $this->createCalculatorTest(
            13,
            self::TEST_3_PRIORITY_5_7,
            [
                '5BR' => '3.13',
                '7BR' => '0.0',
                '15BR' => '0.0'
            ]
        );
    }

    public function testEnquiry15paxPriority5(): void
    {
        $this->createCalculatorTest(
            13,
            self::TEST_3_PRIORITY_15_5_7,
            [
                '5BR' => '3.13',
                '7BR' => '0.0',
                '15BR' => '0.0'
            ]
        );
    }

    public function testEnquiry20paxAvailability(): void
    {
        $this->createCalculatorTest(
            24,
            self::TEST_AVAILATILITY_4_10,
            [
                '4BR' => '1.4',
                '20BR' => '1.20',
            ]
        );
    }

    public function testEnquiry18paxAvailability(): void
    {
        $this->createCalculatorTest(
            18,
            self::TEST_AVAILATILITY_4_10,
            [
                '4BR' => '0.0',
                '20BR' => '1.18',
            ]
        );
    }

    public function testEnquiry16paxAvailability(): void
    {
        $this->createCalculatorTest(
            15,
            self::TEST_AVAILATILITY_4_10,
            [
                '4BR' => '4.15',
                '20BR' => '0.0',
            ]
        );
    }

    /**
     * Implement max availability in Dormtoobig for the smallest room
     * Implement empty beds for example 1 dorm of 6 with 4 pax in it in that case
     */
    public function testEnquiry8pax_4_6_7(): void
    {
        $this->createCalculatorTest(
            8,
            self::TEST_1_AVAILABILITY_4_6_7,
            [
                '4BR' => '1.3',
                '6BR' => '1.5',
                '7BR' => '0.0'
            ]
        );
    }

    /**
     * Test with dorm of 6 and 7 prio 6
     */
    public function testEnquiry14pax_6_7_20(): void
    {
        $this->createCalculatorTest(
            14,
            self::TEST_1_PRIORITY_6_7_20,
            [
                '6BR' => '3.14',
                '7BR' => '0.0',
                '20BR' => '0.0'
            ]
        );
    }

    /**
     * Test with dorm of 6 and 7 prio 7
     */
    public function testEnquiry14pax_7_6_20(): void
    {
        $this->createCalculatorTest(
            14,
            self::TEST_1_PRIORITY_7_6_20,
            [
                '7BR' => '2.14',
                '6BR' => '0.0',
                '20BR' => '0.0'
            ]
        );
    }

    /**
     * Test with one big dorm room assign by prio 1
     */
    public function testEnquiry35pax_4_7_6_20(): void
    {
        $this->createCalculatorTest(
            35,
            self::TEST_1_PRIORITY_4_7_6_20,
            [
                '4BR' => '3.12',
                '7BR' => '2.12',
                '6BR' => '2.11',
                '20BR' => '0.0'
            ]
        );
    }

    /**
     * Test with one big dorm room for a big group
     */
    public function testEnquiry144pax_20_4_6_7(): void
    {
        $this->createCalculatorTest(
            144,
            self::TEST_2_PRIORITY_20_4_6_7,
            [
                '20BR' => '2.40',
                '4BR' => '2.8',
                '6BR' => '9.54',
                '7BR' => '6.42'
            ]
        );
    }

    /**
     * Test with one big dorm room assign by prio 2
     */
    public function testEnquiry28pax_20_4_6_7(): void
    {
        $this->createCalculatorTest(
            28,
            self::TEST_1_PRIORITY_20_4_6_7,
            [
                '20BR' => '1.19',
                '4BR' => '1.4',
                '6BR' => '1.5',
                '7BR' => '0.0'
            ]
        );
    }

    /**
     * Test with one big dorm room for a big group
     */
    public function testEnquiry163pax_4_7_6_20(): void
    {
        $this->createCalculatorTest(
            163,
            self::TEST_1_PRIORITY_4_11_9_20,
            [
                '4BR' => '2.8',
                '9BR' => '9.78',
                '11BR' => '2.20',
                '20BR' => '3.57'
            ]
        );
    }

    /**
     * Test with one big dorm room for a big group
     */
    public function test2Enquiry163pax_4_11_9_20(): void
    {
        $this->createCalculatorTest(
            163,
            self::TEST_2_PRIORITY_4_11_9_20,
            [
                '4BR' => '9.36',
                '9BR' => '6.54',
                '11BR' => '3.33',
                '20BR' => '2.40'
            ]
        );
    }

    /**
     * Test with one big dorm room for a big group
     */
    public function test50(): void
    {
        $this->createCalculatorTest(
            1,
            self::TEST_50,
            [
                '50BR' => '1.1'
            ]
        );
    }

    /**
     * Test doublon capacity
     */
    public function testDoublonCapacity(): void
    {
        $this->createCalculatorTest(
            15,
            self::TEST_DOUBLON_CAPACITY,
            [
                '10BR' => '1.7',
                '10BB' => '1.8'
            ]
        );
    }

    /**
     * Test doublon capacity
     */
    public function testDoublonRoomprio(): void
    {
        $this->createCalculatorTest(
            17,
            self::TEST_DOUBLON_ROOMPRIO_ROOMROUNDOWN,
            [
                '4BR' => '3.9',
                '4BB' => '2.8',
            ]
        );
    }

    private function createCalculatorTest(int $numberOfPersons, array $params, array $breakdownExpected)
    {
        $roomTypes = (new RoomTypeProvider())->get($params['roomtypes']);
        $start = new DateTimeImmutable();
        $end = $start->modify('+5 day');
        $enquiry = new Enquiry();
        $enquiry->setCheckInDate($start);
        $enquiry->setCheckOutDate($end);
        $enquiry->setNumberOfPersons($numberOfPersons);
        $EnquiryRepository = $this->createMock(RoomTypeRepository::class);
        $EnquiryRepository->expects($this->any())
            ->method('findOrderByPriority')
            ->willReturn($roomTypes);
        $objectManager = $this->createMock(EntityManagerInterface::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($EnquiryRepository);
        $availabilities = (new AvailabilityProvider($params["availabilities"]))->get();
        $breakdownRuleCalculator = new BreakdownRuleCalculator($objectManager);
        $resultFormater = new ResultFormater();
        $calculator = new BreakdownCalculator(
            $objectManager,
            $resultFormater,
            $breakdownRuleCalculator
        );
        $calculator->setEnquiry($enquiry);
        $calculator->setAvailabilities($availabilities);
        $calculator->calculate();
        $result = $calculator->result();
        foreach ($breakdownExpected as $label => $expectations) {
            if (!array_key_exists($label, $result)) {
                continue;
            }
            $exp = explode('.', $expectations);
            $nbPersons = ($result[$label]->getRoomType()->getCapacity() * $result[$label]->getRoomsPicked()) - $result[$label]->getEmptyBeds();
            $this->assertEquals((int)$exp[1], $nbPersons);
            $this->assertEquals((int)$exp[0], $result[$label]->getRoomsPicked());
        }
    }
}
