<?php

namespace App\Tests\Unit\Provider;

use App\Entity\RoomType;
use App\Model\BreakdownRoom;

class BreakdownRoomProvider
{
    private $breakdownRooms;

    public function __construct(array $breakdownRooms)
    {
        $this->breakdownRooms = $breakdownRooms;
    }

    /**
     * @param $param
     * @return BreakdownRoom[]
     */
    public function get(): array
    {
        $ret = [];
        foreach ($this->breakdownRooms as $breakDown) {
            $ret[$breakDown['roomtype']] = $this->createBreakDownRoom($breakDown);
        }

        return $ret;
    }

    private function createBreakDownRoom(array $breakDown): BreakdownRoom
    {
        $breakDownRoom = new BreakdownRoom();
        $breakDownRoom->setAvailability($breakDown['availability']);
        $breakDownRoom->setRoomType($this->createRoomType($breakDown['capacity']));

        return $breakDownRoom;
    }

    private function createRoomType(int $capacity): RoomType
    {
        $roomType = new RoomType();
        $roomType->setCapacity($capacity);

        return $roomType;
    }
}
