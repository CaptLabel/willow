<?php

namespace App\Tests\Unit\Provider;

use App\Entity\BreakdownRuleAdd;
use App\Entity\RoomType;

class BreakdownRuleAddProvider
{
    public function get(RoomType $room, $param)
    {
        $entity = new BreakdownRuleAdd();
        $entity->setQuantity($param['quantity']);
        $entity->setRatio($param['ratio']);
        $entity->setRoomToAdd($room);

        return $entity;
    }
}
