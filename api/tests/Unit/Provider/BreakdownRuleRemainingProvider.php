<?php

namespace App\Tests\Unit\Provider;

use App\Entity\BreakdownRuleRemaining;
use App\Entity\RoomType;

class BreakdownRuleRemainingProvider
{
    public function get(RoomType $room, $param): BreakdownRuleRemaining
    {
        $entity = new BreakdownRuleRemaining();
        $entity->setPercentageLeft($param['percentage']);
        $entity->setRoom($room);

        return $entity;
    }
}
