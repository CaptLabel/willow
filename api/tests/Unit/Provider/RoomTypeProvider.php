<?php

namespace App\Tests\Unit\Provider;

use App\Entity\RoomType;

class RoomTypeProvider
{
    /**
     * @param array $params
     * @return RoomType[]
     */
    public function get(array $params): array
    {
        foreach ($params as $param) {
            $roomTypes[] = $this->createRoomType(
                $param[0],
                $param[1],
                $param[2],
                $param[3],
                $param[4]
            );
        }

        return $roomTypes;
    }

    /**
     * @param string $label
     * @param int $capacity
     * @param int $quantity
     * @param bool $groupOffers
     * @param int $priority
     * @return RoomType
     */
    private function createRoomType(
        string $label,
        int $capacity,
        int $quantity,
        bool $groupOffers,
        ?int $priority
    ): RoomType {
        $roomType = new RoomType();
        $roomType->setLabel($label);
        $roomType->setCapacity($capacity);
        $roomType->setQuantity($quantity);
        $roomType->setGroupOffers($groupOffers);
        $roomType->setPriority($priority);

        return $roomType;
    }
}
