<?php

namespace App\Service;

use App\Entity\Status;
use Doctrine\Persistence\ManagerRegistry;

class FollowUpPrefOrder
{
    private $em;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->em = $doctrine->getManager();
    }

    public function assignClassification($followUpPreference, $propertyId): bool
    {
        $existingFollowUps = $this->em->getRepository('App:FollowUpPreference')->findOrderByProperty($propertyId);
        foreach ($existingFollowUps as $existingFollowUp) {
            if ($existingFollowUp->getDays() === $followUpPreference->getDays() &&
                $existingFollowUp->getId() !== $followUpPreference->getId()) {
                return false;
            }
            if ($existingFollowUp->getClassification() === $followUpPreference->getClassification() &&
                $existingFollowUp->getId() !== $followUpPreference->getId()) {
                return false;
            }
        }
        return true;
    }
}
