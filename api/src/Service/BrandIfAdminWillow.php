<?php

namespace App\Service;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Security;

class BrandIfAdminWillow
{
    /**
     * @var Security
     */
    private $security;

    private $em;

    public function __construct(Security $security, ManagerRegistry $doctrine)
    {
        $this->security = $security;
        $this->em = $doctrine->getManager();
    }

    public function getBrandDependingOnUser($session, $roleAdminWillow)
    {
        if ($roleAdminWillow) {
            $brand = $session->get('brand');
        } else {
            $user = $this->security->getUser();
            $brand = $user->getBrand();
        }

        return $brand;
    }

    public function getBrandIdDependingOnUser($session, $roleAdminWillow)
    {
        if ($roleAdminWillow) {
            $brand = $session->get('brand');
            $brandId = $brand->getId();
        } else {
            $user = $this->security->getUser();
            $brandId = $user->getBrand()->getId();
        }

        return $brandId;
    }

    public function getBrandViaRepoDependingOnUser($session, $roleAdminWillow)
    {
        if ($roleAdminWillow) {
            $brand = $session->get('brand');
            $brandId = $brand->getId();
        } else {
            $user = $this->security->getUser();
            $brandId = $user->getBrand()->getId();
        }

        return $this->em->getRepository('App:Brand')->find($brandId);
    }
}
