<?php

namespace App\Service;

use App\Entity\Status;
use Doctrine\Persistence\ManagerRegistry;

class DefaultStatus
{
    private $em;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->em = $doctrine->getManager();
    }

    public function defaultOptional($status, $brand)
    {
        $optionalTrue = $status->getOptionalStatus();
        $defCondEx = $this->em->getRepository(Status::class)->findOptionalByBrand($brand);
        if ($defCondEx) {
            if ($optionalTrue) {
                $defCondEx->setOptionalStatus(false);
            }
        }
    }

    public function defaultConfirmed($status, $brand)
    {
        $confirmedTrue = $status->getConfirmedStatus();
        $defCondEx = $this->em->getRepository(Status::class)->findConfirmedByBrand($brand);
        if ($defCondEx) {
            if ($confirmedTrue) {
                $defCondEx->setConfirmedStatus(false);
            }
        }
    }
}
