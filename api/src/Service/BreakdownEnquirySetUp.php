<?php

namespace App\Service;

use App\Entity\BreakdownEnquiry;
use App\Entity\Property;
use Doctrine\Persistence\ManagerRegistry;

class BreakdownEnquirySetUp
{
    private $em;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->em = $doctrine->getManager();
    }

    public function changeEnquiries(Property $property)
    {
        $breakdownTypes = $property->getBreakdownTypes();
        $breakdownIds = [];
        foreach ($breakdownTypes as $breakdown) {
            $id = $breakdown->getId();
            $breakdownIds[$id] = $id;
        }
        $propertyId = $property->getId();
        $futureEnquiries = $this->em->getRepository('App:Enquiry')->findAllFutureByProperty($propertyId);
        if (!empty($futureEnquiries)) {
            foreach ($futureEnquiries as $futureEnquiry) {
                $breakdownEnquiries = $futureEnquiry->getBreakdownEnquiries();
                $breakdownEnquiryIds = [];
                foreach ($breakdownEnquiries as $breakdownEnquiry) {
                    $breakdownEnquiryId = $breakdownEnquiry->getBreakdownPref()->getId();
                    if (!array_key_exists($breakdownEnquiryId, $breakdownIds)) {
                        $this->em->remove($breakdownEnquiry);
                    }
                    $breakdownEnquiryIds[$breakdownEnquiryId] = $breakdownEnquiryId;
                }
                foreach ($breakdownTypes as $breakdown) {
                    if (!array_key_exists($breakdown->getId(), $breakdownEnquiryIds)) {
                        $newBreakdown = new BreakdownEnquiry();
                        $newBreakdown->setEnquiry($futureEnquiry);
                        $newBreakdown->setBreakdownPref($breakdown);
                        $this->em->persist($newBreakdown);
                    }
                }
            }
        }
    }
}
