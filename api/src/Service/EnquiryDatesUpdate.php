<?php

namespace App\Service;

use App\Entity\EnquiryUpdates;
use App\Entity\History;
use App\Entity\Offer;
use App\Model\EnquiryChanges;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Security;

class EnquiryDatesUpdate
{
    private $entityManager;

    private $security;

    public function __construct(ManagerRegistry $doctrine, Security $security)
    {
        $this->entityManager = $doctrine->getManager();
        $this->security = $security;
    }

    public function datesSetter(Offer $offer)
    {
        $enquiryChanges = new EnquiryChanges();
        foreach ($offer->getRooms() as $room) {
            if (
                $enquiryChanges->getCheckInDate() === null
                || $enquiryChanges->getCheckInDate() > $room->getCheckInDate()
            ) {
                $enquiryChanges->setCheckInDate($room->getCheckInDate());
            }
            if (
                $enquiryChanges->getCheckOutDate() === null
                || $enquiryChanges->getCheckOutDate() < $room->getCheckOutDate()
            ) {
                $enquiryChanges->setCheckOutDate($room->getCheckOutDate());
            }
            $enquiryChanges->setNumberOfPersons($enquiryChanges->getNumberOfPersons() + $room->getPersonsInTheRoom());
        }
        $enquiry = $offer->getEnquiry();
        $enquiry->setCheckInDate($enquiryChanges->getCheckInDate());
        $enquiry->setCheckOutDate($enquiryChanges->getCheckOutDate());
        $enquiry->setNumberOfPersons($enquiryChanges->getNumberOfPersons());
        $enquiryId = $enquiry->getId();
        $user = $this->security->getUser();
        $lastUpdate = $this->entityManager->getRepository('App:EnquiryUpdates')->findLastOneCreated($enquiryId);
        $luCid = $lastUpdate->getCheckInDate();
        $luCod = $lastUpdate->getCheckOutDate();
        $luPax = $lastUpdate->getNumberOfPersons();
        $cid = $enquiry->getCheckInDate();
        $cod = $enquiry->getCheckOutDate();
        $status = $enquiry->getStatus();
        $pax = $enquiry->getNumberOfPersons();
        if ($luCid != $cid || $luCod != $cod || $luPax !== $pax) {
            $newEnquiryUpdate = new EnquiryUpdates();
            $newEnquiryUpdate->setEnquiry($enquiry);
            $newEnquiryUpdate->setCheckInDate($cid);
            $newEnquiryUpdate->setCheckOutDate($cod);
            $newEnquiryUpdate->setNumberOfPersons($pax);
            $newEnquiryUpdate->setStatus($status);
            $this->entityManager->persist($newEnquiryUpdate);
        }
        if ($luCid != $cid) {
            $newHistory = new History();
            $newHistory->setUser($user);
            $newHistory->setEnquiry($enquiry);
            $newHistory->setOldValue($luCid->format('d-m-Y'));
            $newHistory->setNewValue($cid->format('d-m-Y'));
            $newHistory->setFieldEdit('Check In date');
            $this->entityManager->persist($newHistory);
        }
        if ($luCod != $cod) {
            $newHistory = new History();
            $newHistory->setUser($user);
            $newHistory->setEnquiry($enquiry);
            $newHistory->setOldValue($luCod->format('d-m-Y'));
            $newHistory->setNewValue($cod->format('d-m-Y'));
            $newHistory->setFieldEdit('Check Out date');
            $this->entityManager->persist($newHistory);
        }
        if ($luPax !== $pax) {
            $newHistory = new History();
            $newHistory->setUser($user);
            $newHistory->setEnquiry($enquiry);
            $newHistory->setOldValue(strval($luPax));
            $newHistory->setNewValue(strval($pax));
            $newHistory->setFieldEdit('Number of Persons');
            $this->entityManager->persist($newHistory);
        }
        $this->entityManager->flush();
    }
}
