<?php

namespace App\Service;

use App\Entity\Enquiry;
use App\Entity\EnquiryUpdates;
use App\Entity\History;
use App\Entity\Offer;
use App\Entity\OfferExtraUpdates;
use App\Entity\OfferRoomUpdates;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Security;

class HistoryManager
{
    private $entityManager;

    private $security;

    public function __construct(ManagerRegistry $doctrine, Security $security)
    {
        $this->entityManager = $doctrine->getManager();
        $this->security = $security;
    }

    public function enquiryHistorySetter(Enquiry $enquiry)
    {
        $enquiryId = $enquiry->getId();
        $user = $this->security->getUser();
        $lastUpdate = $this->entityManager->getRepository('App:EnquiryUpdates')->findLastOneCreated($enquiryId);
        $luSta = $lastUpdate->getStatus();
        $luCid = $lastUpdate->getCheckInDate();
        $luCod = $lastUpdate->getCheckOutDate();
        $luPax = $lastUpdate->getNumberOfPersons();
        $sta = $enquiry->getStatus();
        $cid = $enquiry->getCheckInDate();
        $cod = $enquiry->getCheckOutDate();
        $pax = $enquiry->getNumberOfPersons();
        if ($luSta !== $sta || $luCid != $cid || $luCod != $cod || $luPax !== $pax) {
            $newEnquiryUpdate = new EnquiryUpdates();
            $newEnquiryUpdate->setEnquiry($enquiry);
            $newEnquiryUpdate->setStatus($sta);
            $newEnquiryUpdate->setCheckInDate($cid);
            $newEnquiryUpdate->setCheckOutDate($cod);
            $newEnquiryUpdate->setNumberOfPersons($pax);
            $this->entityManager->persist($newEnquiryUpdate);
        }
        if ($luSta !== $sta) {
            $newHistory = new History();
            $newHistory->setUser($user);
            $newHistory->setEnquiry($enquiry);
            $newHistory->setOldValue($luSta->getLabel());
            $newHistory->setNewValue($sta->getLabel());
            $newHistory->setFieldEdit('Status');
            $this->entityManager->persist($newHistory);
        }
        if ($luCid != $cid) {
            $newHistory = new History();
            $newHistory->setUser($user);
            $newHistory->setEnquiry($enquiry);
            $newHistory->setOldValue($luCid->format('d-m-Y'));
            $newHistory->setNewValue($cid->format('d-m-Y'));
            $newHistory->setFieldEdit('Check In date');
            $this->entityManager->persist($newHistory);
        }
        if ($luCod != $cod) {
            $newHistory = new History();
            $newHistory->setUser($user);
            $newHistory->setEnquiry($enquiry);
            $newHistory->setOldValue($luCod->format('d-m-Y'));
            $newHistory->setNewValue($cod->format('d-m-Y'));
            $newHistory->setFieldEdit('Check Out date');
            $this->entityManager->persist($newHistory);
        }
        if ($luPax !== $pax) {
            $newHistory = new History();
            $newHistory->setUser($user);
            $newHistory->setEnquiry($enquiry);
            $newHistory->setOldValue(strval($luPax));
            $newHistory->setNewValue(strval($pax));
            $newHistory->setFieldEdit('Number of Persons');
            $this->entityManager->persist($newHistory);
        }
    }

    public function offerHistorySetter(Offer $offer)
    {
        $user = $this->security->getUser();
        $enquiry = $offer->getEnquiry();
        foreach ($offer->getRooms() as $room) {
            $lastUpdate = $this->entityManager->getRepository('App:OfferRoomUpdates')->findLastOneCreated($room);
            if (null !== $lastUpdate) {
                $luRT = $lastUpdate->getRoomType();
                $luRQ = $lastUpdate->getRoomQuantity();
                $luCid = $lastUpdate->getCheckInDate();
                $luCod = $lastUpdate->getCheckOutDate();
                $luPax = $lastUpdate->getPersons();
                $luAv = $lastUpdate->getAverage();
                $rT = $room->getRoomType();
                $rQ = $room->getRoomQuantity();
                $cid = $room->getCheckInDate();
                $cod = $room->getCheckOutDate();
                $pax = $room->getPersonsInTheRoom();
                $av = $room->getAverageRate();
                if ($luRQ !== $rQ || $luCid != $cid || $luCod != $cod || $luPax !== $pax || $luAv !== $av || $luRT !== $rT) {
                    $newOfferRoomUpdates = new OfferRoomUpdates();
                    $newOfferRoomUpdates->setOfferRoom($room);
                    $newOfferRoomUpdates->setRoomType($room->getRoomType());
                    $newOfferRoomUpdates->setCheckInDate($cid);
                    $newOfferRoomUpdates->setCheckOutDate($cod);
                    $newOfferRoomUpdates->setRoomQuantity($rQ);
                    $newOfferRoomUpdates->setPersons($pax);
                    $newOfferRoomUpdates->setAverage($av);
                    $this->entityManager->persist($newOfferRoomUpdates);
                    $newHistory = new History();
                    $newHistory->setUser($user);
                    $newHistory->setEnquiry($enquiry);
                    $newHistory->setOldValue(
                        "Room Type: " . (strval($luRT->getLabel())) .
                        "<br/>Room Quantity: " . (strval($luRQ)) .
                        "<br/>Check In: " . $luCid->format('d-m-Y') .
                        "<br/>Check Out: " . $luCod->format('d-m-Y') .
                        "<br/>Persons: " . (strval($luPax)) .
                        "<br/>Average: " . (strval($luAv))
                    );
                    $newHistory->setNewValue(
                        "Room Type: " . (strval($rT->getLabel())) .
                        "<br/>Room Quantity: " . (strval($rQ)) .
                        "<br/>Check In: " . $cid->format('d-m-Y') .
                        "<br/>Check Out: " . $cod->format('d-m-Y') .
                        "<br/>Persons: " . (strval($pax)) .
                        "<br/>Average: " . (strval($av))
                    );
                    $newHistory->setFieldEdit($room->getRoomType()->getLabel());
                    $this->entityManager->persist($newHistory);
                }
            } else {
                $newOfferUpdate = new OfferRoomUpdates();
                $newOfferUpdate->setOfferRoom($room);
                $newOfferUpdate->setRoomType($room->getRoomType());
                $newOfferUpdate->setRoomQuantity($room->getRoomQuantity());
                $newOfferUpdate->setCheckInDate($room->getCheckInDate());
                $newOfferUpdate->setCheckOutDate($room->getCheckOutDate());
                $newOfferUpdate->setPersons($room->getPersonsInTheRoom());
                $newOfferUpdate->setAverage($room->getAverageRate());
                $this->entityManager->persist($newOfferUpdate);
            }
        }
        foreach ($offer->getOfferExtras() as $extra) {
            $extraId = $extra->getId();
            $lastExtra = $this->entityManager->getRepository('App:OfferExtraUpdates')->findLastOneCreated($extraId);
            if (null !== $lastExtra) {
                $luET = $lastExtra->getExtraType();
                $luQu = $lastExtra->getQuantity();
                $luSt = $lastExtra->getStartDate();
                $luEn = $lastExtra->getEndDate();
                $luPr = $lastExtra->getPrice();
                $eT = $extra->getExtra();
                $qu = $extra->getQuantity();
                $st = $extra->getStartDate();
                $en = $extra->getEndDate();
                $pr = $extra->getPrice();
                if ($luQu !== $qu || $luSt != $st || $luEn != $en || $luPr !== $pr || $luET !== $eT) {
                    $newOfferExtraUpdates = new OfferExtraUpdates();
                    $newOfferExtraUpdates->setOfferExtra($extra);
                    $newOfferExtraUpdates->setExtraType($extra->getExtra());
                    $newOfferExtraUpdates->setQuantity($qu);
                    $newOfferExtraUpdates->setStartDate($st);
                    $newOfferExtraUpdates->setEndDate($en);
                    $newOfferExtraUpdates->setPrice($pr);
                    $this->entityManager->persist($newOfferExtraUpdates);
                    $newHistory = new History();
                    $newHistory->setUser($user);
                    $newHistory->setEnquiry($enquiry);
                    $newHistory->setOldValue(
                        "Extra Type: " . (strval($luET->getLabel())) .
                        "<br/>Extra Quantity: " . (strval($luQu)) .
                        "<br/>Start Date: " . $luSt->format('d-m-Y') .
                        "<br/>End Date: " . $luEn->format('d-m-Y') .
                        "<br/>Price: " . (strval($luPr))
                    );
                    $newHistory->setNewValue(
                        "Extra Type: " . (strval($eT->getLabel())) .
                        "<br/>Extra Quantity: " . (strval($qu)) .
                        "<br/>Start Date: " . $st->format('d-m-Y') .
                        "<br/>End Date: " . $en->format('d-m-Y') .
                        "<br/>Price: " . (strval($pr))
                    );
                    $newHistory->setFieldEdit($extra->getExtra()->getLabel());
                    $this->entityManager->persist($newHistory);
                }
            } else {
                $newOfferExtraUpdates = new OfferExtraUpdates();
                $newOfferExtraUpdates->setOfferExtra($extra);
                $newOfferExtraUpdates->setExtraType($extra->getExtra());
                $newOfferExtraUpdates->setQuantity($extra->getQuantity());
                $newOfferExtraUpdates->setStartDate($extra->getStartDate());
                $newOfferExtraUpdates->setEndDate($extra->getEndDate());
                $newOfferExtraUpdates->setPrice($extra->getPrice());
                $this->entityManager->persist($newOfferExtraUpdates);
            }
        }
    }

    public function newOfferExtraUpdate($extra)
    {
        $newExtraUpdate = new OfferExtraUpdates();
        $newExtraUpdate->setOfferExtra($extra);
        $newExtraUpdate->setExtraType($extra->getExtra());
        $newExtraUpdate->setQuantity($extra->getQuantity());
        $newExtraUpdate->setStartDate($extra->getStartDate());
        $newExtraUpdate->setEndDate($extra->getEndDate());
        $newExtraUpdate->setPrice($extra->getPrice());
        $this->entityManager->persist($newExtraUpdate);

        return $newExtraUpdate;
    }

    public function newOfferRoomUpdate($offerRoom)
    {
        $newOfferUpdate = new OfferRoomUpdates();
        $newOfferUpdate->setOfferRoom($offerRoom);
        $newOfferUpdate->setRoomType($offerRoom->getRoomType());
        $newOfferUpdate->setRoomQuantity($offerRoom->getRoomQuantity());
        $newOfferUpdate->setCheckInDate($offerRoom->getCheckInDate());
        $newOfferUpdate->setCheckOutDate($offerRoom->getCheckOutDate());
        $newOfferUpdate->setPersons($offerRoom->getPersonsInTheRoom());
        $newOfferUpdate->setAverage($offerRoom->getAverageRate());
        $this->entityManager->persist($newOfferUpdate);

        return $newOfferUpdate;
    }

    public function generateNewOffer($offer, $user)
    {
        foreach ($offer->getRooms() as $room) {
            $newHistory = new History();
            $newHistory->setUser($user);
            $newHistory->setEnquiry($offer->getEnquiry());
            $newHistory->setOldValue(
                "Room quantity: " . (strval($room->getRoomQuantity())) .
                "<br/>Check In: " . $room->getCheckInDate()->format('d-m-Y') .
                "<br/>Check Out: " . $room->getCheckOutDate()->format('d-m-Y') .
                "<br/>Persons: " . (strval($room->getPersonsInTheRoom())) .
                "<br/>Average: " . (strval($room->getAverageRate()))
            );
            $newHistory->setNewValue("Removed");
            $newHistory->setFieldEdit($room->getRoomType()->getLabel());
            $this->entityManager->persist($newHistory);
        }
        foreach ($offer->getOfferExtras() as $extra) {
            $newHistory = new History();
            $newHistory->setUser($user);
            $newHistory->setEnquiry($offer->getEnquiry());
            $newHistory->setOldValue(
                "Extra quantity: " . (strval($extra->getQuantity())) .
                "<br/>Start Date: " . $extra->getStartDate()->format('d-m-Y') .
                "<br/>End Date: " . $extra->getEndDate()->format('d-m-Y') .
                "<br/>Price: " . (strval($extra->getPrice()))
            );
            $newHistory->setNewValue("Removed");
            $newHistory->setFieldEdit($extra->getExtra()->getLabel());
            $this->entityManager->persist($newHistory);
        }
        $this->entityManager->flush();
    }
}
