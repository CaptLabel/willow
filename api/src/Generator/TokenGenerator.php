<?php

namespace App\Generator;

use App\Entity\User;

class TokenGenerator
{
    public function create(User $user): string
    {
        $username = $user->getUsername();
        $timestamp = 1234567890;
        $date_time = new \DateTime();
        $key = $username . $date_time->getTimestamp();
        $hash = hash('sha256', $key);

        return $hash;
    }
}
