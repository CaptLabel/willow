<?php

namespace App\Generator;

use App\Entity\Brand;
use App\Entity\Property;
use Doctrine\ORM\EntityManagerInterface;

class PropertyListGenerator
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {

        $this->entityManager = $entityManager;
    }

    /**
     * @return Property[]
     */
    public function list(Brand $brand): array
    {
        return $this->entityManager->getRepository('App:Property')->findBy(['brand' => $brand]);
    }
}
