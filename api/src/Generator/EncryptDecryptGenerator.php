<?php

namespace App\Generator;

use App\Entity\Property;
use Nzo\UrlEncryptorBundle\Encryptor\Encryptor;

class EncryptDecryptGenerator
{
    private $encryptor;

    public function __construct(Encryptor $encryptor)
    {
        $this->encryptor = $encryptor;
    }

    public function encryptBrandTokens($brand)
    {
        if ($brand->getApiKeyGetData()) {
            $encryptedTokenGetData = $this->encryptor->encrypt($brand->getApiKeyGetData());
            $brand->setApiKeyGetData($encryptedTokenGetData);
        }
        if ($brand->getApiKeyPostData()) {
            $encryptedTokenPostData = $this->encryptor->encrypt($brand->getApiKeyPostData());
            $brand->setApiKeyPostData($encryptedTokenPostData);
        }
    }

    public function decryptBrandTokens($brand)
    {
        if ($brand->getApiKeyGetData()) {
            $encryptedTokenGetData = $this->encryptor->decrypt($brand->getApiKeyGetData());
            $brand->setApiKeyGetData($encryptedTokenGetData);
        }
        if ($brand->getApiKeyPostData()) {
            $encryptedTokenPostData = $this->encryptor->decrypt($brand->getApiKeyPostData());
            $brand->setApiKeyPostData($encryptedTokenPostData);
        }
    }

    public function encryptPropertyToken(Property $property)
    {
        if ($property->getApiKey()) {
            $encryptedToken = $this->encryptor->encrypt($property->getApiKey());
            $property->setApiKey($encryptedToken);
        }
    }

    public function decryptPropertyToken(Property $property)
    {
        if ($property->getApiKey()) {
            $encryptedToken = $this->encryptor->decrypt($property->getApiKey());
            $property->setApiKey($encryptedToken);
        }
    }
}
