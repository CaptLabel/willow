<?php

namespace App\Generator;

class EnquiryProposalKeyGenerator
{
    public function create()
    {
        return uniqid();
    }
}