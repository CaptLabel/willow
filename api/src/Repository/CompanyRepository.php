<?php

namespace App\Repository;

use App\Entity\Company;
use App\Entity\Status;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Company|null find($id, $lockMode = null, $lockVersion = null)
 * @method Company|null findOneBy(array $criteria, array $orderBy = null)
 * @method Company[]    findAll()
 * @method Company[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Company::class);
    }
    /**
     * @param string $brandId
     * @return Company[]
     */
    public function findByBrand(string $brandId): array
    {
        return $this->createQueryBuilder('c')
            ->select('c')
            ->where('c.brand = :brand')
            ->setParameter('brand', $brandId)
            ->OrderBy('c.defaultSelection', 'DESC')
            ->addOrderBy('c.label', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findByBrandWithUser($brand): array
    {
        return $this->createQueryBuilder('c')
            ->where('c.brand = :brand')
            ->setParameter('brand', $brand)
            ->andWhere('c.user is not null')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findDefaultByBrand($brand): ?Company
    {
        return $this->createQueryBuilder('c')
            ->where('c.brand = :brand')
            ->setParameter('brand', $brand)
            ->andWhere('c.defaultSelection = 1')
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }
}
