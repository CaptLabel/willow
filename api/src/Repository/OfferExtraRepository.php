<?php

namespace App\Repository;

use App\Entity\OfferExtra;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OfferExtra|null find($id, $lockMode = null, $lockVersion = null)
 * @method OfferExtra|null findOneBy(array $criteria, array $orderBy = null)
 * @method OfferExtra[]    findAll()
 * @method OfferExtra[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OfferExtraRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OfferExtra::class);
    }

    // /**
    //  * @return OfferExtra[] Returns an array of OfferExtra objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OfferExtra
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
