<?php

namespace App\Repository;

use App\Entity\Brand;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function getByToken($token): ?User
    {
        return $this->createQueryBuilder('u')
            ->where('u.token = :token')
            ->setParameter('token', $token)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findAllByRole(array $roles)
    {
        $qb = $this->createQueryBuilder('user');
        foreach ($roles as $role) {
            $qb
            ->orWhere("user.roles LIKE :$role")
            ->setParameter($role, '%"' . $role . '"%')
            ;
        }

        return $qb->getQuery()->getResult();
    }

    public function findByBrand(Brand $brand): array
    {
        return $this->createQueryBuilder('user')
            ->where('user.brand = :brand')
            ->setParameter('brand', $brand)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findProperties(UserInterface $user): array
    {
        return $this->createQueryBuilder('user')
            ->where('user.properties = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findByBrandAndRole(Brand $brand, array $roles): array
    {
        $qb = $this->createQueryBuilder('user');
        foreach ($roles as $role) {
            $qb
                ->orWhere("user.roles LIKE :$role")
                ->setParameter($role, '%"' . $role . '"%')
            ;
        }
        $qb
            ->AndWhere('user.brand = :brand')
            ->setParameter('brand', $brand)
        ;

        return $qb->getQuery()->getResult();
    }
}
