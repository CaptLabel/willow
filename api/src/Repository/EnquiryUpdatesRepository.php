<?php

namespace App\Repository;

use App\Entity\EnquiryUpdates;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<EnquiryUpdates>
 *
 * @method EnquiryUpdates|null find($id, $lockMode = null, $lockVersion = null)
 * @method EnquiryUpdates|null findOneBy(array $criteria, array $orderBy = null)
 * @method EnquiryUpdates[]    findAll()
 * @method EnquiryUpdates[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EnquiryUpdatesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EnquiryUpdates::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(EnquiryUpdates $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function findLastOneCreated($enquiryId): ?EnquiryUpdates
    {
        return $this->createQueryBuilder('e')
            ->Where('e.enquiry = :enquiry')
            ->setParameter('enquiry', $enquiryId)
            ->setMaxResults(1)
            ->orderBy('e.updateDateTime', 'DESC')
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }
}
