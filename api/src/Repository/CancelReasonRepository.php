<?php

namespace App\Repository;

use App\Entity\CancelReason;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CancelReason|null find($id, $lockMode = null, $lockVersion = null)
 * @method CancelReason|null findOneBy(array $criteria, array $orderBy = null)
 * @method CancelReason[]    findAll()
 * @method CancelReason[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CancelReasonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CancelReason::class);
    }
    /**
     * @param string $brandId
     * @return CancelReason[]
     */
    public function findByBrand(string $brandId): array
    {
        return $this->createQueryBuilder('s')
            ->select('s')
            ->where('s.brand = :brand')
            ->setParameter('brand', $brandId)
            ->getQuery()
            ->getResult()
            ;
    }
}