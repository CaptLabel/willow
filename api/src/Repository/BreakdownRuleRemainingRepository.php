<?php

namespace App\Repository;

use App\Entity\BreakdownRuleRemaining;
use App\Entity\Enquiry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BreakdownRuleRemaining|null find($id, $lockMode = null, $lockVersion = null)
 * @method BreakdownRuleRemaining|null findOneBy(array $criteria, array $orderBy = null)
 * @method BreakdownRuleRemaining[]    findAll()
 * @method BreakdownRuleRemaining[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BreakdownRuleRemainingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BreakdownRuleRemaining::class);
    }

    public function findAllByProByRoom($propertyId, $roomId): array
    {
        return $this->createQueryBuilder('b')
            ->where('b.property = :property')
            ->setParameter('property', $propertyId)
            ->andWhere('b.room = :room')
            ->setParameter('room', $roomId)
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @param Enquiry $enquiry
     * @return BreakdownRuleRemaining[] Returns an array of BreakdownRuleAdd objects
     */
    public function findRemainingRules(Enquiry $enquiry, $occupancy): array
    {
        $numberOfNights = $enquiry->getCheckInDate()->diff($enquiry->getCheckOutDate())->format("%a");
        $dayName = $enquiry->getCheckInDate()->format('l');
        if ($enquiry->getAge()) {
            $ageMin = $enquiry->getAge()->getMin();
            $ageMax = $enquiry->getAge()->getMax();
        } else {
            $ageMin = 0;
            $ageMax = 1000;
        }
        return $this->createQueryBuilder('brr')
            ->leftJoin('brr.age_min', 'age_min')
            ->leftJoin('brr.age_max', 'age_max')
            ->Where('
                (brr.pax_max >= :pax_number OR brr.pax_max is null)
                AND (brr.pax_min <= :pax_number OR brr.pax_min is null)
             ')
            ->setParameter('pax_number', $enquiry->getNumberOfPersons())
            ->andWhere('
                (brr.length_max >= :night_number OR brr.length_max is null)
                AND (brr.length_min <= :night_number OR brr.length_min is null)
            ')
            ->setParameter('night_number', $numberOfNights)
            ->andWhere('
                (brr.occupancy_max >= :occupancy OR brr.occupancy_max is null)
                AND (brr.occupancy_min <= :occupancy OR brr.occupancy_min is null)
            ')
            ->setParameter('occupancy', $occupancy)
            ->andWhere('
                (age_max.max >= :enq_age_max OR age_max is null OR age_max.max is null)
                AND (age_min.min <= :enq_age_min OR age_min is null OR age_min.min is null)
            ')
            ->setParameter('enq_age_min', $ageMin)
            ->setParameter('enq_age_max', $ageMax)
            ->andWhere('
                (brr.date_end >= :check_in_date OR brr.date_end is null)
                AND (brr.date_start <= :check_in_date OR brr.date_start is null)
            ')
            ->setParameter('check_in_date', $enquiry->getCheckInDate(), Types::DATETIME_MUTABLE)
            ->andWhere("brr.daySelection LIKE :day OR brr.daySelection LIKE '%a:0:{}%'")
            ->setParameter('day', '%' . $dayName . '%')
            ->andWhere('brr.active = 1')
            ->andWhere('brr.property = :property')
            ->setParameter('property', $enquiry->getProperty()->getId())
            ->getQuery()
            ->getResult();
    }

    public function findAllByProperty($property_id): array
    {
        return $this->createQueryBuilder('breakdown_rule_remaining')
            ->where('breakdown_rule_remaining.property = :property')
            ->setParameter('property', $property_id)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findInactiveRoom(int $property_id): ?array
    {
        return $this->createQueryBuilder('b')
            ->join('b.room', 'rt')
            ->where('b.property = :property')
            ->setParameter('property', $property_id)
            ->andWhere('rt.activated = 0')
            ->select('rt.label')
            ->groupBy('rt.label')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findAgeByBrand($brand, $age): ?array
    {
        return $this->createQueryBuilder('b')
            ->join('b.property', 'p')
            ->where('p.brand = :brand')
            ->setParameter('brand', $brand)
            ->andWhere('b.age_min = :age OR b.age_max = :age')
            ->setParameter('age', $age)
            ->getQuery()
            ->getResult();
    }
}
