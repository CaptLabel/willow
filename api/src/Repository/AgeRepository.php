<?php

namespace App\Repository;

use App\Entity\Age;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Age|null find($id, $lockMode = null, $lockVersion = null)
 * @method Age|null findOneBy(array $criteria, array $orderBy = null)
 * @method Age[]    findAll()
 * @method Age[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AgeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Age::class);
    }
    /**
     * @param string $brandId
     * @return Age[]
     */
    public function findByBrand(string $brandId): array
    {
        return $this->createQueryBuilder('a')
            ->select('a')
            ->where('a.brand = :brand')
            ->setParameter('brand', $brandId)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findByCode($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.formCode = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }
}
