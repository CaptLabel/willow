<?php

namespace App\Repository;

use App\Entity\Brand;
use App\Entity\Enquiry;
use App\Entity\Status;
use DateInterval;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Enquiry|null find($id, $lockMode = null, $lockVersion = null)
 * @method Enquiry|null findOneBy(array $criteria, array $orderBy = null)
 * @method Enquiry[]    findAll()
 * @method Enquiry[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EnquiryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Enquiry::class);
    }

    /**
     * @param string $propertyId
     * @return Enquiry[] Returns an array of Enquiry objects
     */
    public function findAllByProperty(string $propertyId): array
    {
        return $this->createQueryBuilder('e')
            ->join('e.property', 'property')
            ->andWhere('e.property = :propertyId')
            ->setParameter('propertyId', $propertyId)
            ->orderBy('e.enquiry_date', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @param string $propertyId
     * @return Enquiry[] Returns an array of Enquiry objects
     */
    public function findAllFutureByProperty(string $propertyId): array
    {
        $date = new \DateTime();
        return $this->createQueryBuilder('e')
            ->where('e.property = :propertyId')
            ->setParameter('propertyId', $propertyId)
            ->andWhere('e.check_in_date > :date')
            ->setParameter('date', $date)
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @param string $propertyId
     * @return Enquiry[] Returns an array of Enquiry objects
     */
    public function findAllByProperties(array $properties): array
    {
        return $this->createQueryBuilder('e')
            ->join('e.property', 'property')
            ->andWhere('e.property IN (:properties)')
            ->setParameter('properties', $properties)
            ->orderBy('e.enquiry_date', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @param Brand $brand
     * @return Enquiry[] Returns an array of Enquiry objects
     */
    public function findAllByBrand(Brand $brand): array
    {
        return $this->createQueryBuilder('e')
            ->join('e.property', 'p')
            ->andWhere('p.brand = :brand')
            ->setParameter('brand', $brand)
            ->orderBy('e.enquiry_date', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findLastOneCreated($propertyId): ?Enquiry
    {
        return $this->createQueryBuilder('e')
            ->where('e.property = :property')
            ->setParameter('property', $propertyId)
            ->setMaxResults(1)
            ->orderBy('e.enquiry_date', 'DESC')
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    /**
     * @return Enquiry[] Returns an array of Enquiry objects
     */
    public function findLastByProperty(string $propertyId): array
    {
        return $this->createQueryBuilder('e')
            ->where('e.property = :property')
            ->setParameter('property', $propertyId)
            ->andWhere('e.active = 1')
            ->setMaxResults(5)
            ->orderBy('e.enquiry_date', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return Enquiry[] Returns an array of Enquiry objects
     */
    public function findLastTenByBrand($properties): array
    {
        return $this->createQueryBuilder('e')
            ->join('e.property', 'property')
            ->andWhere('property IN (:propertyList)')
            ->setParameter('propertyList', $properties)
            ->andWhere('e.active = 1')
            ->setMaxResults(10)
            ->orderBy('e.enquiry_date', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @param string $propertyId
     * @return Enquiry[] Returns an array of Enquiry objects
     */
    public function findAllPendingByProperty(string $propertyId): array
    {
        return $this->createQueryBuilder('e')
            ->join('e.property', 'p')
            ->join('e.status', 's')
            ->andWhere('p = :propertyId')
            ->andWhere('e.offer is null')
            ->andWhere('e.active = 1')
            ->andWhere('s.default_selection = 1')
            ->setParameter('propertyId', $propertyId)
            ->orderBy('e.enquiry_date', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @param string $propertyId
     * @return Enquiry[] Returns an array of Enquiry objects
     */
    public function findPendingOneDayByProperty(string $propertyId, \DateTimeImmutable $yesterday): int
    {
        return $this->createQueryBuilder('e')
            ->join('e.property', 'p')
            ->join('e.status', 's')
            ->andWhere('p = :propertyId')
            ->andWhere('e.offer is null')
            ->andWhere('e.active = 1')
            ->andWhere('s.default_selection = 1')
            ->andWhere('e.enquiry_date > :yesterday')
            ->setParameter('propertyId', $propertyId)
            ->setParameter('yesterday', $yesterday)
            ->select('count(e.id)')
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }

    /**
     * @param string $propertyId
     * @return Enquiry[] Returns an array of Enquiry objects
     */
    public function findPendingThreeDayByProperty(string $propertyId, \DateTimeImmutable $threeDaysAgo, \DateTimeImmutable $yesterday): int
    {
        return $this->createQueryBuilder('e')
            ->join('e.property', 'p')
            ->join('e.status', 's')
            ->andWhere('p = :propertyId')
            ->andWhere('e.offer is null')
            ->andWhere('e.active = 1')
            ->andWhere('s.default_selection = 1')
            ->andWhere('e.enquiry_date < :yesterday')
            ->andWhere('e.enquiry_date > :threeDay')
            ->setParameter('propertyId', $propertyId)
            ->setParameter('threeDay', $threeDaysAgo)
            ->setParameter('yesterday', $yesterday)
            ->select('count(e.id)')
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }

    /**
     * @param string $propertyId
     * @return Enquiry[] Returns an array of Enquiry objects
     */
    public function findPendingMaxByProperty(string $propertyId, \DateTimeImmutable $max): int
    {
        return $this->createQueryBuilder('e')
            ->join('e.property', 'p')
            ->join('e.status', 's')
            ->andWhere('p = :propertyId')
            ->andWhere('e.offer is null')
            ->andWhere('e.active = 1')
            ->andWhere('s.default_selection = 1')
            ->andWhere('e.enquiry_date < :max')
            ->setParameter('propertyId', $propertyId)
            ->setParameter('max', $max)
            ->select('count(e.id)')
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }

    /**
     * @param string $propertyId
     * @return Enquiry[] Returns an array of Enquiry objects
     */
    public function findLastTenDays(string $propertyId, \DateTimeImmutable $tenDays, \DateTimeImmutable $lastNight): ?array
    {
        return $this->createQueryBuilder('e')
            ->select(
                "DATE_FORMAT(e.enquiry_date, '%d-%m') AS date",
                'count(e.id) as count',
            )
            ->join('e.property', 'p')
            ->where('p = :propertyId')
            ->andWhere('e.enquiry_date IS NOT NULL')
            ->andWhere('e.active = 1')
            ->andWhere('e.enquiry_date > :tenDays')
            ->andWhere('e.enquiry_date < :yesterdayNight')
            ->setParameter('propertyId', $propertyId)
            ->setParameter('tenDays', $tenDays)
            ->setParameter('yesterdayNight', $lastNight)
            ->groupBy('date')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return Enquiry[] Returns an array of Enquiry objects
     */
    public function findLastTenDaysBrand($brand, \DateTimeImmutable $tenDays, \DateTimeImmutable $lastNight): ?array
    {
        return $this->createQueryBuilder('e')
            ->select(
                "DATE_FORMAT(e.enquiry_date, '%d-%m') AS date",
                'count(e.id) as count',
            )
            ->join('e.property', 'p')
            ->where('p.brand = :brand')
            ->andWhere('e.enquiry_date IS NOT NULL')
            ->andWhere('e.active = 1')
            ->andWhere('e.enquiry_date > :tenDays')
            ->andWhere('e.enquiry_date < :yesterdayNight')
            ->setParameter('brand', $brand)
            ->setParameter('tenDays', $tenDays)
            ->setParameter('yesterdayNight', $lastNight)
            ->groupBy('date')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @param Brand $brand
     * @return Enquiry[]
     */
    public function findAllPendingByBrand(Brand $brand): array
    {
        return $this->createQueryBuilder('e')
            ->join('e.property', 'p')
            ->andWhere('p.brand = :brand')
            ->andWhere('e.active = 1')
            ->andWhere('e.offer is null')
            ->setParameter('brand', $brand)
            ->orderBy('e.enquiry_date', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @param string $propertyId
     * @return Enquiry[] Returns an array of Enquiry objects
     */
    public function findAllReceivedByProperty(string $propertyId): array
    {
        return $this->createQueryBuilder('e')
            ->join('e.property', 'property')
            ->andWhere('e.property = :propertyId')
            ->andWhere('e.offer is null')
            ->andWhere('e.active = 1')
            ->setParameter('propertyId', $propertyId)
            ->setMaxResults(5)
            ->orderBy('e.enquiry_date', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return Enquiry[] Returns an array of Enquiry objects
     */
    public function search(array $properties, string $item): array
    {
        return $this->createQueryBuilder('e')
            ->join('e.property', 'property')
            ->andWhere('e.group_name LIKE :item OR e.reservationNumber LIKE :item')
            ->andWhere('property IN (:propertyList)')
            ->setParameter('propertyList', $properties)
            ->setParameter('item', '%' . $item . '%')
            ->setMaxResults(10)
            ->orderBy('e.enquiry_date', 'DESC')
            ->getQuery()
            ->getArrayResult()
            ;
    }

    /**
     * @return array
     */
    public function findCreatedByPLastMonth(int $propertyId): ?array
    {
        $today = new \DateTimeImmutable();
        $oneMonth = $today->sub(new DateInterval('P1M'));
        return $this->createQueryBuilder('e')
            ->select('count(e.id) as count')
            ->where('e.property = :property')
            ->setParameter('property', $propertyId)
            ->andWhere('e.enquiry_date > :oneMonth')
            ->setParameter('oneMonth', $oneMonth)
            ->andWhere('e.active = 1')
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    /**
     * @return array
     */
    public function findCreatedByPLastYear(int $propertyId): ?array
    {
        $today = new \DateTimeImmutable();
        $oneYear = $today->sub(new DateInterval('P1Y'));
        return $this->createQueryBuilder('e')
            ->select('count(e.id) as count')
            ->where('e.property = :property')
            ->setParameter('property', $propertyId)
            ->andWhere('e.enquiry_date > :oneMonth')
            ->setParameter('oneMonth', $oneYear)
            ->andWhere('e.active = 1')
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    /**
     * @return array
     */
    public function findConfirmedByPLastMonth(int $propertyId): ?array
    {
        $today = new \DateTimeImmutable();
        $oneMonth = $today->sub(new DateInterval('P1M'));
        return $this->createQueryBuilder('e')
            ->select('count(e.id) as count')
            ->leftJoin('e.status', 's')
            ->where('e.property = :property')
            ->setParameter('property', $propertyId)
            ->andWhere('e.enquiry_date > :oneMonth')
            ->setParameter('oneMonth', $oneMonth)
            ->andWhere('e.active = 1')
            ->andWhere('s.confirmedStatus = 1')
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    /**
     * @return array
     */
    public function findConfirmedByPLastYear(int $propertyId): ?array
    {
        $today = new \DateTimeImmutable();
        $oneYear = $today->sub(new DateInterval('P1Y'));
        return $this->createQueryBuilder('e')
            ->select('count(e.id) as count')
            ->leftJoin('e.status', 's')
            ->where('e.property = :property')
            ->setParameter('property', $propertyId)
            ->andWhere('e.enquiry_date > :oneMonth')
            ->setParameter('oneMonth', $oneYear)
            ->andWhere('e.active = 1')
            ->andWhere('s.confirmedStatus = 1')
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    /**
     * @return array
     */
    public function findCancelledByPLastMonth(int $propertyId): ?array
    {
        $today = new \DateTimeImmutable();
        $oneMonth = $today->sub(new DateInterval('P1M'));
        return $this->createQueryBuilder('e')
            ->select('count(e.id) as count')
            ->leftJoin('e.status', 's')
            ->where('e.property = :property')
            ->setParameter('property', $propertyId)
            ->andWhere('e.enquiry_date > :oneMonth')
            ->setParameter('oneMonth', $oneMonth)
            ->andWhere('e.active = 1')
            ->andWhere('s.cancel_type = 1')
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    /**
     * @return array
     */
    public function findCancelledByPLastYear(int $propertyId): ?array
    {
        $today = new \DateTimeImmutable();
        $oneYear = $today->sub(new DateInterval('P1Y'));
        return $this->createQueryBuilder('e')
            ->select('count(e.id) as count')
            ->leftJoin('e.status', 's')
            ->where('e.property = :property')
            ->setParameter('property', $propertyId)
            ->andWhere('e.enquiry_date > :oneMonth')
            ->setParameter('oneMonth', $oneYear)
            ->andWhere('e.active = 1')
            ->andWhere('s.cancel_type = 1')
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    /**
     * @return Enquiry[] Returns an array of Enquiry objects
     */
    public function searchEnquiryReportByProp($criteria): ?array
    {
        return $this->createQueryBuilder('e')
            ->where('e.property = :propertyName')
            ->setParameter('propertyName', $criteria['property']->getId())
            ->andWhere('e.check_in_date >= :from')
            ->setParameter('from', $criteria['from'])
            ->andWhere('e.check_in_date <= :until')
            ->setParameter('until', $criteria['until'])
            ->andWhere('e.enquiry_date >= :created_from')
            ->setParameter('created_from', $criteria['created_from'])
            ->andWhere('e.enquiry_date <= :created_until')
            ->setParameter('created_until', $criteria['created_until'])
            ->orderBy('e.enquiry_date', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return Enquiry[] Returns an array of Enquiry objects
     */
    public function searchEnquiryReport($criteria, $brand): ?array
    {
        return $this->createQueryBuilder('e')
            ->leftJoin('e.property', 'p')
            ->where('p.brand = :brandName')
            ->setParameter('brandName', $brand)
            ->andWhere('e.check_in_date >= :from')
            ->setParameter('from', $criteria['from'])
            ->andWhere('e.check_in_date <= :until')
            ->setParameter('until', $criteria['until'])
            ->andWhere('e.enquiry_date >= :created_from')
            ->setParameter('created_from', $criteria['created_from'])
            ->andWhere('e.enquiry_date <= :created_until')
            ->setParameter('created_until', $criteria['created_until'])
            ->orderBy('e.enquiry_date', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return Enquiry[] Returns an array of Enquiry objects
     */
    public function searchSalesByUserReport($criteria): ?array
    {
        return $this->createQueryBuilder('e')
            ->leftJoin('e.company', 'c')
            ->where('c is not null')
            ->andWhere('e.offer is not null')
            ->andWhere('c.user = :user')
            ->setParameter('user', $criteria['account_manager']->getId())
            ->andWhere('e.property = :propertyName')
            ->setParameter('propertyName', $criteria['property']->getId())
            ->andWhere('e.check_in_date >= :from')
            ->setParameter('from', $criteria['from'])
            ->andWhere('e.check_in_date <= :until')
            ->setParameter('until', $criteria['until'])
            ->andWhere('e.enquiry_date >= :created_from')
            ->setParameter('created_from', $criteria['created_from'])
            ->andWhere('e.enquiry_date <= :created_until')
            ->setParameter('created_until', $criteria['created_until'])
            ->andWhere('e.active = 1')
            ->getQuery()
            ->getResult()
            ;
    }

    public function searchSalesCompanyReport($criteria): ?array
    {
        return $this->createQueryBuilder('e')
            ->leftJoin('e.company', 'c')
            ->where('c is not null')
            ->andWhere('e.offer is not null')
            ->andWhere('c.user is not null')
            ->andWhere('e.property = :propertyName')
            ->setParameter('propertyName', $criteria['property']->getId())
            ->andWhere('e.check_in_date >= :from')
            ->setParameter('from', $criteria['from'])
            ->andWhere('e.check_in_date <= :until')
            ->setParameter('until', $criteria['until'])
            ->andWhere('e.enquiry_date >= :created_from')
            ->setParameter('created_from', $criteria['created_from'])
            ->andWhere('e.enquiry_date <= :created_until')
            ->setParameter('created_until', $criteria['created_until'])
            ->andWhere('e.active = 1')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByProposalKey(string $proposalKey): ?Enquiry
    {
        return $this->createQueryBuilder('e')
            ->where('e.proposalKey = :proposal_key')
            ->setParameter('proposal_key', $proposalKey)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findStatusByBrand($brand, $status): ?array
    {
        return $this->createQueryBuilder('e')
            ->join('e.property', 'p')
            ->where('p.brand = :brand')
            ->setParameter('brand', $brand)
            ->andWhere('e.status = :status')
            ->setParameter('status', $status)
            ->getQuery()
            ->getResult();
    }

    public function findBreakdownSetUpByBrand($brand, $breakdownPreference): ?array
    {
        return $this->createQueryBuilder('e')
            ->join('e.property', 'p')
            ->leftJoin('e.breakdownEnquiries', 'br')
            ->where('p.brand = :brand')
            ->setParameter('brand', $brand)
            ->andWhere('br.breakdownPref = :breakdownPreference')
            ->setParameter('breakdownPreference', $breakdownPreference)
            ->getQuery()
            ->getResult();
    }

    public function findAgeByBrand($brand, $age): ?array
    {
        return $this->createQueryBuilder('e')
            ->join('e.property', 'p')
            ->where('p.brand = :brand')
            ->setParameter('brand', $brand)
            ->andWhere('e.age = :age')
            ->setParameter('age', $age)
            ->getQuery()
            ->getResult();
    }

    public function findCxlReasonByBrand($brand, $cancelReason): ?array
    {
        return $this->createQueryBuilder('e')
            ->join('e.property', 'p')
            ->where('p.brand = :brand')
            ->setParameter('brand', $brand)
            ->andWhere('e.cancelReason = :cxl')
            ->setParameter('cxl', $cancelReason)
            ->getQuery()
            ->getResult();
    }

    public function findExtraByProperty($property, $extra): ?array
    {
        return $this->createQueryBuilder('e')
            ->join('e.offer', 'o')
            ->leftJoin('o.offerExtras', 'o_ex')
            ->where('e.property = :property')
            ->setParameter('property', $property)
            ->andWhere('o_ex.extra = :extra')
            ->setParameter('extra', $extra)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string $propertyId
     * @return Enquiry[] Returns an array of Enquiry objects
     */
    public function findHighestValueLastMonth(string $propertyId, \DateTimeImmutable $oneMonthAgo): ?array
    {
        return $this->createQueryBuilder('e')
            ->join('e.property', 'p')
            ->join('p.currency', 'c')
            ->join('e.offer', 'o')
            ->join('o.rooms', 'o_room')
            ->join('o_room.rates', 'o_room_rate')
            ->where('e.property = :propertyId')
            ->setParameter('propertyId', $propertyId)
            ->andWhere('e.active = 1')
            ->andWhere('e.enquiry_date > :oneMonthAgo')
            ->setParameter('oneMonthAgo', $oneMonthAgo)
            ->select(
                'SUM(o_room.room_quantity * o_room_rate.price) as room_total',
                'e.group_name',
                'e.check_in_date',
                'e.check_out_date',
                'e.numberOfPersons',
                'e.id',
                'c.symbol as symbol',
            )
            ->groupBy('e')
            ->setMaxResults(5)
            ->orderBy('room_total', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }
}
