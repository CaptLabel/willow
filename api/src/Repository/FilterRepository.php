<?php

namespace App\Repository;

use App\Entity\Enquiry;
use App\Entity\Filter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Filter|null find($id, $lockMode = null, $lockVersion = null)
 * @method Filter|null findOneBy(array $criteria, array $orderBy = null)
 * @method Filter[]    findAll()
 * @method Filter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FilterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Filter::class);
    }

    public function findFilters(Enquiry $enquiry, $occupancy)
    {
        $numberOfNights = $enquiry->getCheckInDate()->diff($enquiry->getCheckOutDate())->format("%a");
        $dayName = $enquiry->getCheckInDate()->format('l');
        if ($enquiry->getAge()) {
            $ageMin = $enquiry->getAge()->getMin();
            $ageMax = $enquiry->getAge()->getMax();
        } else {
            $ageMin = 0;
            $ageMax = 1000;
        }

        return $this->createQueryBuilder('f')
            ->leftJoin('f.age_min', 'age_min')
            ->leftJoin('f.age_max', 'age_max')
            ->Where('
                (f.pax_max >= :pax_number OR f.pax_max is null) 
                AND (f.pax_min <= :pax_number OR f.pax_min is null)
             ')
            ->setParameter('pax_number', $enquiry->getNumberOfPersons())
            ->andWhere('
                (f.night_max >= :night_number OR f.night_max is null)
                AND (f.night_min <= :night_number OR f.night_min is null)
            ')
            ->setParameter('night_number', $numberOfNights)
            ->andWhere('
                (f.occupancy_max >= :occupancy OR f.occupancy_max is null)
                AND (f.occupancy_min <= :occupancy OR f.occupancy_min is null)
            ')
            ->setParameter('occupancy', $occupancy)
            ->andWhere('
                (age_max.max >= :enq_age_max OR age_max is null OR age_max.max is null)
                AND (age_min.min <= :enq_age_min OR age_min is null OR age_min.min is null)
            ')
            ->setParameter('enq_age_min', $ageMin)
            ->setParameter('enq_age_max', $ageMax)
            ->andWhere('
                (f.date_max >= :check_in_date OR f.date_max is null)
                AND (f.date_min <= :check_in_date OR f.date_min is null)
            ')
            ->setParameter('check_in_date', $enquiry->getCheckInDate(), Types::DATETIME_MUTABLE)
            ->andWhere("f.daySelection LIKE :day OR f.daySelection LIKE '%a:0:{}%'")
            ->setParameter('day', '%' . $dayName . '%')
            ->andWhere('f.active = 1')
            ->andWhere('f.property = :property')
            ->setParameter('property', $enquiry->getProperty())
            ->orderBy('f.id', 'ASC')
            ->getQuery()
            ->getResult();
    }


    /**
     * @param string $propertyId
     * @return Filter[] Returns an array of Enquiry objects
     */
    public function findAllByProperty(string $propertyId): ?array
    {
        return $this->createQueryBuilder('f')
            ->join('f.property', 'property')
            ->andWhere('f.property = :propertyId')
            ->setParameter('propertyId', $propertyId)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findAgeByBrand($brand, $age): ?array
    {
        return $this->createQueryBuilder('f')
            ->join('f.property', 'p')
            ->where('p.brand = :brand')
            ->setParameter('brand', $brand)
            ->andWhere('f.age_min = :age OR f.age_max = :age')
            ->setParameter('age', $age)
            ->getQuery()
            ->getResult();
    }
}
