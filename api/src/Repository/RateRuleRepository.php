<?php

namespace App\Repository;

use App\Entity\Enquiry;
use App\Entity\RateRule;
use DateTimeInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RateRule|null find($id, $lockMode = null, $lockVersion = null)
 * @method RateRule|null findOneBy(array $criteria, array $orderBy = null)
 * @method RateRule[]    findAll()
 * @method RateRule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RateRuleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RateRule::class);
    }

    /**
     * @param $property_id
     * @return RateRule[]
     */
    public function findRateRulesByProperty(string $property_id): array
    {
        $qb = $this->createQueryBuilder('rate_rule');
        $qb
            ->select('rate_rule')
            ->where('rate_rule.property = :property_id')
            ->setParameter('property_id', $property_id);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $property_id
     * @return ?RateRule
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findDefaultRateRuleByProperty(int $property_id): ?RateRule
    {
        $qb = $this->createQueryBuilder('rate_rule');
        $qb
            ->select('rate_rule')
            ->where('rate_rule.property = :property_id')
            ->andWhere('rate_rule.default_selection = 1')
            ->setParameter('property_id', $property_id);

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function findRates(Enquiry $enquiry, DateTimeInterface $date, int $occupancy)
    {
        $numberOfNights = $enquiry->getCheckInDate()->diff($enquiry->getCheckOutDate())->format("%a");
        $dayName = $date->format('l');
        if ($enquiry->getAge()) {
            $ageMin = $enquiry->getAge()->getMin();
            $ageMax = $enquiry->getAge()->getMax();
        } else {
            $ageMin = 0;
            $ageMax = 1000;
        }

        return $this->createQueryBuilder('rate_rule')
            ->leftJoin('rate_rule.age_min', 'age_min')
            ->leftJoin('rate_rule.age_max', 'age_max')
            ->Where('
                (rate_rule.pax_max >= :pax_number OR rate_rule.pax_max is null)
                AND (rate_rule.pax_min <= :pax_number OR rate_rule.pax_min is null)
             ')
            ->setParameter('pax_number', $enquiry->getNumberOfPersons())
            ->andWhere('
                (rate_rule.length_max >= :night_number OR rate_rule.length_max is null)
                AND (rate_rule.length_min <= :night_number OR rate_rule.length_min is null)
            ')
            ->setParameter('night_number', $numberOfNights)
            ->andWhere('
                (rate_rule.date_end >= :check_in_date OR rate_rule.date_end is null)
                AND (rate_rule.date_start <= :check_in_date OR rate_rule.date_start is null)
            ')
            ->setParameter('check_in_date', $date, Types::DATETIME_MUTABLE)
            ->andWhere("rate_rule.day_selection LIKE :day OR rate_rule.day_selection LIKE '%a:0:{}%'")
            ->setParameter('day', '%' . $dayName . '%')
            ->andWhere('
                (rate_rule.occupancy_max >= :occupancy OR rate_rule.occupancy_max is null)
                AND (rate_rule.occupancy_min <= :occupancy OR rate_rule.occupancy_min is null)
            ')
            ->setParameter('occupancy', $occupancy)
            ->andWhere('
                (age_max.max >= :enq_age_max OR age_max is null OR age_max.max is null)
                AND (age_min.min <= :enq_age_min OR age_min is null OR age_min.min is null)
            ')
            ->setParameter('enq_age_min', $ageMin)
            ->setParameter('enq_age_max', $ageMax)
            ->andWhere('rate_rule.active = 1')
            ->andWhere('rate_rule.property = :property')
            ->setParameter('property', $enquiry->getProperty()->getId())
            ->orderBy('rate_rule.priority', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findSimilar(int $property_id, RateRule $rateRule): ?RateRule
    {
        return $this->createQueryBuilder('rate_rule')
            ->where('
                (rate_rule.date_start <= :date_start AND rate_rule.date_end >= :date_start)   
                OR 
                (rate_rule.date_start <= :date_end AND rate_rule.date_end >= :date_end)
            ')
            ->setParameter('date_start', $rateRule->getDateStart(), Types::DATETIME_MUTABLE)
            ->setParameter('date_end', $rateRule->getDateEnd(), Types::DATETIME_MUTABLE)
            ->andWhere('rate_rule.priority = :priority')
            ->setParameter('priority', $rateRule->getPriority())
            ->andWhere('rate_rule.property = :property')
            ->setParameter('property', $property_id)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    public function findRateRuleInactiveRate(int $property_id): ?array
    {
        return $this->createQueryBuilder('r')
            ->join('r.rate_type', 'rt')
            ->where('r.property = :property')
            ->setParameter('property', $property_id)
            ->andWhere('rt.activated = 0')
            ->select('rt.label')
            ->groupBy('rt.label')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findAgeByBrand($brand, $age): ?array
    {
        return $this->createQueryBuilder('r')
            ->join('r.property', 'p')
            ->where('p.brand = :brand')
            ->setParameter('brand', $brand)
            ->andWhere('r.age_min = :age OR r.age_max = :age')
            ->setParameter('age', $age)
            ->getQuery()
            ->getResult();
    }
}
