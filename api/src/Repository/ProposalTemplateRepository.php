<?php

namespace App\Repository;

use App\Entity\TemplateProposal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TemplateProposal|null find($id, $lockMode = null, $lockVersion = null)
 * @method TemplateProposal|null findOneBy(array $criteria, array $orderBy = null)
 * @method TemplateProposal[]    findAll()
 * @method TemplateProposal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProposalTemplateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TemplateProposal::class);
    }

    /**
     * @param string $propertyId
     * @return TemplateProposal[] Returns an array of Enquiry objects
     */
    public function findAllByProperty(string $propertyId): array
    {
        return $this->createQueryBuilder('mt')
            ->join('mt.property', 'property')
            ->andWhere('mt.property = :propertyId')
            ->setParameter('propertyId', $propertyId)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findDefByPByL($propertyId, $languageId): ?TemplateProposal
    {
        return $this->createQueryBuilder('p')
            ->where('p.property = :property')
            ->setParameter('property', $propertyId)
            ->andWhere('p.language = :language')
            ->setParameter('language', $languageId)
            ->andWhere('p.defaultSelection = 1')
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    public function findLanguageWithDefault($propertyId): ?array
    {
        return $this->createQueryBuilder('p')
            ->join('p.language', 'l')
            ->where('p.property = :property')
            ->setParameter('property', $propertyId)
            ->andWhere('p.defaultSelection = 1')
            ->select('l.id')
            ->getQuery()
            ->getResult()
            ;
    }
}
