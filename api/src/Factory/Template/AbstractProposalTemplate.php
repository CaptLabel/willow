<?php

namespace App\Factory\Template;

use Twig\Environment;

abstract class AbstractProposalTemplate
{
    protected string $templateName = 'changeIt!';

    public string $html;

    protected Environment $environment;

    public function __construct(
        Environment $environment
    )
    {
        $this->environment = $environment;
    }

    public function renderTemplate(string $templateName, array $context = []): string
    {
        return $this->environment->render($templateName, $context);
    }

    abstract public function getHtml($data): string;
}
