<?php

namespace App\Factory\Template;

use App\Entity\OfferRoom;

class TableRoom extends AbstractProposalTemplate
{
    protected string $templateName = 'proposal/proposal_offer.html.twig';

    /**
     * @param OfferRoom[] $data
     */
    public function getHtml($data): string
    {
        if ($data) {
            $property = $data[0]->getOffer()->getEnquiry()->getProperty();
            $context['property'] = $property;
        }
        $context['rooms'] = $data;
        return $this->renderTemplate($this->templateName, $context);
    }
}