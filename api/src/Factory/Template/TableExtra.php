<?php

namespace App\Factory\Template;

use App\Entity\OfferExtra;

class TableExtra extends AbstractProposalTemplate
{
    protected string $templateName = 'proposal/proposal_extra.html.twig';

    /**
     * @param OfferExtra[] $data
     */
    public function getHtml($data): string
    {
        if ($data) {
            $property = $data[0]->getOffer()->getEnquiry()->getProperty();
            $context['property'] = $property;
        }
        $context['extras'] = $data;
        return $this->renderTemplate($this->templateName, $context);
    }
}