<?php

namespace App\Factory\Template;

use App\Entity\Offer;

class TotalProposal extends AbstractProposalTemplate
{
    protected string $templateName = 'proposal/total_proposal.html.twig';

    /**
     * @param Offer[] $data
     */
    public function getHtml($data): string
    {
        $rooms = $data->getRooms();
        $extras = $data->getOfferExtras();
        $property = $data->getEnquiry()->getProperty();
        $context['rooms'] = $rooms;
        $context['extras'] = $extras;
        $context['property'] = $property;
        return $this->renderTemplate($this->templateName, $context);
    }
}