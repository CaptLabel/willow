<?php

namespace App\Checker;

use App\Entity\Enquiry;
use App\Entity\EnquiryWish;
use App\Entity\RoomType;
use App\Exception\AvailabilityCheckerException;
use App\Model\Availability;

class AvailabilityChecker
{
    /**
     * @var Enquiry
     */
    private $enquiry;

    /**
     * @var Availability[]
     */
    private $bookings;

    /**
     * @var array
     */
    private $errors;

    /**
     * @var RoomType[]
     */
    private $roomTypes;

    public function __construct(
        Enquiry $enquiry,
        array $bookings,
        array $roomTypes
    ) {
        $this->enquiry = $enquiry;
        $this->bookings = $bookings;
        $this->roomTypes = $roomTypes;
    }

    public function isAvailable(EnquiryWish $enquiryWish)
    {
        $available = true;
        $period = new \DatePeriod(
            $enquiryWish->getCheckInDate(),
            new \DateInterval('P1D'),
            $enquiryWish->getCheckOutDate()
        );

        foreach ($period as $date) {
            if (!array_key_exists($date->format('d/m/Y'), $this->bookings)) {
                $this->errors[] = "{$enquiryWish->getLabel()} : Date not found {$date->format('d/m/Y')}";
                $available = false;
                continue;
            }
            $booking = $this->bookings[$date->format('d/m/Y')];
            if (!array_key_exists($enquiryWish->getLabel(), $booking->getRooms())) {
                $this->errors[] = "{$enquiryWish->getLabel()} : type not found in BookingRooms";
                $available = false;
                continue;
            }
            $bookingRoom = $booking->getRooms()[$enquiryWish->getLabel()];
            if (!array_key_exists($enquiryWish->getLabel(), $this->roomTypes)) {
                $this->errors[] = "{$enquiryWish->getLabel()} : type not found in RoomType";
                $available = false;
                continue;
            }
            $roomType = $this->roomTypes[$enquiryWish->getLabel()];
            if ($bookingRoom->getAvailability() * $roomType->getCapacity() < $enquiryWish->getNumberOfPersons()) {
                $label = $enquiryWish->getLabel();
                $this->errors[] = "{$label} : No availibility found for the date {$date->format('d/m/Y')}";
                $available = false;
                continue;
            }
        }

        return $available;
    }

    public function errors()
    {
        return $this->errors;
    }
}
