<?php

namespace App\DataFixtures;

use App\Entity\Filter;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class FilterFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= PropertyFixtures::PROPERTY_NUMBER; $i++) {
            $filter = new Filter();
            $filter->setProperty($this->getReference('property_' . $i));
            $filter->setLabel('between 15 and 100 pax');
            $filter->setPaxMin(15);
            $filter->setPaxMax(100);
            $filter->setActive(1);
            $filter->setDescription('desc');

            $manager->persist($filter);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            PropertyFixtures::class
        ];
    }
}
