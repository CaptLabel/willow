<?php

namespace App\DataFixtures;

use App\Entity\Address;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AddressFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    public const ADDRESS_COUNT = 10;

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        $address_reference = [];
        for ($i = 1; $i <= self::ADDRESS_COUNT; $i++) {
            $address = new Address();
            $address->setAddressLine1($faker->address);
            $address->setCity($faker->city);
            $address->setPhoneNumber($faker->phoneNumber);
            $address->setPostalCode($faker->postcode);
            $address->setCountry($this->getReference('country_' . rand(1, count(CountryFixtures::COUNTRY_LIST))));
            $address_reference['address_' . $i] = $address;
            $manager->persist($address);
        }

        $manager->flush();

        foreach ($address_reference as $key => $item) {
            $this->addReference($key, $item);
        }
    }

    public function getDependencies()
    {
        return array(
            CountryFixtures::class,
        );
    }

    public static function getGroups(): array
    {
        return ['testGroup'];
    }
}
