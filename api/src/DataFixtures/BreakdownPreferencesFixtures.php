<?php

namespace App\DataFixtures;

use App\Entity\BreakdownPreferences;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class BreakdownPreferencesFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= BrandFixtures::BRAND_NUMBER; $i++) {
            $breakdown1 = new BreakdownPreferences();
            $breakdown1->setBrand($this->getReference('brand_' . $i));
            $breakdown1->setLabel('Single');

            $manager->persist($breakdown1);
        }
        for ($i = 1; $i <= BrandFixtures::BRAND_NUMBER; $i++) {
            $breakdown2 = new BreakdownPreferences();
            $breakdown2->setBrand($this->getReference('brand_' . $i));
            $breakdown2->setLabel('Twin');

            $manager->persist($breakdown2);
        }
        for ($i = 1; $i <= BrandFixtures::BRAND_NUMBER; $i++) {
            $breakdown3 = new BreakdownPreferences();
            $breakdown3->setBrand($this->getReference('brand_' . $i));
            $breakdown3->setLabel('Dorms');

            $manager->persist($breakdown3);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            BrandFixtures::class
        ];
    }

    public static function getGroups(): array
    {
        return ['testGroup'];
    }
}
