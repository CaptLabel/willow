<?php

namespace App\DataFixtures;

use App\Entity\EnquiryUpdates;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class EnquiryUpdatesFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    public function getDependencies()
    {
        return array(
            EnquiryFixtures::class,
            StatusFixtures::class,
        );
    }

    public function load(ObjectManager $manager)
    {

        for ($i = 1; $i <= EnquiryFixtures::ENQUIRY_NUMBER; $i++) {
            $enquiryUpdate = new EnquiryUpdates();
            $enquiryUpdate->setEnquiry($this->getReference('enquiry_' . $i));
            $enquiryUpdate->setUpdateDateTime(new \DateTime());
            $status = $enquiryUpdate->getEnquiry()->getStatus();
            $enquiryUpdate->setStatus($status);
            $checkInDate = $enquiryUpdate->getEnquiry()->getCheckInDate();
            $enquiryUpdate->setCheckInDate($checkInDate);
            $checkOutDate = $enquiryUpdate->getEnquiry()->getCheckOutDate();
            $enquiryUpdate->setCheckOutDate($checkOutDate);
            $numberOfPersons = $enquiryUpdate->getEnquiry()->getNumberOfPersons();
            $enquiryUpdate->setNumberOfPersons($numberOfPersons);

            $manager->persist($enquiryUpdate);
        }

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['testGroup'];
    }
}
