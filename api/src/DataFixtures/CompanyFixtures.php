<?php

namespace App\DataFixtures;

use App\Entity\Company;
use App\Entity\Property;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class CompanyFixtures extends Fixture implements DependentFixtureInterface
{
    public const COMPANY_NUMBER = 100;

    protected $faker;

    public function load(ObjectManager $manager)
    {
        $this->faker = Factory::create();
        $company_reference = [];
        for ($i = 1; $i <= self::COMPANY_NUMBER; $i++) {
            $company = new Company();
            $company->setLabel($this->faker->company);
            $company->setBrand($this->getReference('brand_' . rand(1, 2)));
            $company->setAddress($this->getReference('address_' . rand(1, AddressFixtures::ADDRESS_COUNT)));
            $company_reference['company_' . $i] = $company;
            $manager->persist($company);
        }

        $manager->flush();

        foreach ($company_reference as $key => $item) {
            $this->addReference($key, $item);
        }
    }

    public function getDependencies()
    {
        return array(
            BrandFixtures::class,
            AddressFixtures::class,
        );
    }
}
