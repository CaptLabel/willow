<?php

namespace App\DataFixtures;

use App\Entity\TemplateProposal;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TemplateProposalFixtures extends Fixture implements DependentFixtureInterface
{
    public function getDependencies()
    {
        return [
            PropertyFixtures::class,
            LanguageFixtures::class
        ];
    }

    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= PropertyFixtures::PROPERTY_NUMBER; $i++) {
            $proposalTemplate = new TemplateProposal();
            $proposalTemplate->setLabel('2022 EN');
            $proposalTemplate->setProperty($this->getReference('property_' . $i));
            $proposalTemplate->setLanguage($this->getReference('language_1'));
            $proposalTemplate->setDefaultSelection(1);
            $html = '
            <!doctype html>
                <html lang="fr">
                <head>
                  <meta charset="utf-8">
                  <title>Titre de la page</title>
                  <style>
                    h1{
                        color: blue;
                    }
                    .condition{
                        color: grey;
                    }
                  </style>
                </head>
                <body>
                    <h1>Awesome Proposal</h1>
                    <p>{presentation_content}</p>
                    <hr>
                    {table_room}
                    <hr>
                    {table_extra}
                    <hr>
                    {total_proposal}
                    <hr>
                    <p class="condition">{condition_content}</p>
                    <hr>
                    <p>{presentation_feature}</p>
                </body>
                </html>
            ';
            $proposalTemplate->setContent($html);
            $proposalTemplate->setTemplatePresentation($this->getReference('property_' . $i . '_hotel_presentation'));
            $manager->persist($proposalTemplate);
            $manager->flush();
        }
    }
}
