<?php

namespace App\DataFixtures;

use App\Entity\RateType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class RateTypeFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    public const RATES_TYPE = [
        "GRA1" => [10, 100],
        "GRA2" => [110, 200],
        "GRA3" => [220, 310],
        "NRF" => [10,20]
    ];

    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 8; $i++) {
            $count = 1;
            foreach (self::RATES_TYPE as $label => $params) {
                $rateType = new RateType();
                $rateType->setLabel($label);
                $rateType->setProperty($this->getReference((string)'property_' . $i));
                $references['property_' . $i . '_ratetype_' . $count] = $rateType;
                $manager->persist($rateType);
                $count++;
            }
        }

        $manager->flush();

        foreach ($references as $key => $item) {
            $this->addReference($key, $item);
        }
    }

    public function getDependencies()
    {
        return array(
            PropertyFixtures::class,
        );
    }

    public static function getGroups(): array
    {
        return ['testGroup'];
    }
}
