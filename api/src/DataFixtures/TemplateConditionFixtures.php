<?php

namespace App\DataFixtures;

use App\Entity\TemplateCondition;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TemplateConditionFixtures extends Fixture implements DependentFixtureInterface
{
    public function getDependencies()
    {
        return [
            PropertyFixtures::class,
            LanguageFixtures::class
        ];
    }

    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= PropertyFixtures::PROPERTY_NUMBER; $i++) {
            $conditions = new TemplateCondition();
            $conditions->setLabel('2022 EN');
            $conditions->setProperty($this->getReference('property_' . $i));
            $conditions->setDefaultSelection(1);
            $html = "The terms and conditions for 2022 are: <b>Non Refundable</b>";
            $conditions->setContent($html);
            $conditions->setLanguage($this->getReference('language_1'));

            $manager->persist($conditions);
            $manager->flush();
        }
    }
}
