<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AvailabilityFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $this->createResourcesAll();
        $this->createServicesAvailability();
    }

    private function createResourcesAll()
    {
        $resources = [];
        $resources['Resources'] = null;
        $resources['ResourceCategoryAssignments'] = null;
        $resources['ResourceCategoryImageAssignments'] = null;
        $resources['ResourceFeatures'] = null;
        $resources['ResourceFeatureAssignments'] = null;

        $resourceCategories = [];
        $id = 1;
        foreach (RoomTypeFixtures::ROOMTYPE as $label => $room) {
            $categorie = [
                "Id" => "$id",
                "ServiceId" => 'bd26d8db-86da-4f96-9efc-e5a4654a4a94',
                "IsActive" => true,
                "Type" => "Dorm",
                "Names" => [
                    "en-US" => $label,
                ],
                "ShortNames" => [
                    "en-US" => $label,
                ],
                "Descriptions" => [
                    "en-US" => $label,
                ],
                "Ordering" => 0,
                "Capacity" => $room['capacity'],
                "ExtraCapacity" => 0,
            ];
            $resourceCategories[] = $categorie;
            $id++;
        }
        $resources['ResourceCategories'] = $resourceCategories;
        $fileDir = __DIR__ . '/../Requestor/mock/resources';
        if (!is_dir($fileDir)) {
            mkdir($fileDir, 0777, true);
        }
        $fp = fopen("{$fileDir}/getAll.json", 'w');
        fwrite($fp, json_encode($resources));
        fclose($fp);
    }

    private function createServicesAvailability()
    {
        $date_start = (new \DateTime())->modify('-1 month');
        $date_end = (new \DateTime())->modify('+15 days');

        $period = new \DatePeriod(
            $date_start,
            new \DateInterval('P1D'),
            $date_end
        );
        $nbDays = 0;
        $dateUTC = [];
        foreach ($period as $dateTime) {
            $dateUTC[] = $dateTime->format('c');
            $nbDays++;
        }
        $availability = [];
        $availabilities = [];
        $id = 1;
        foreach (RoomTypeFixtures::ROOMTYPE as $label => $room) {
            $dispo = [];
            for ($i = 1; $i <= $nbDays; $i++) {
                $dispo[] = rand(2, $room['quantity']);
            }

            $adjustment = [];
            for ($i = 1; $i <= $nbDays; $i++) {
                $adjustment[] = rand(1, 3);
            }

            $categorie = [
                "CategoryId" => "$id",
                "Availabilities" => $dispo,
                "Adjustments" => $adjustment
            ];
            $availabilities[] = $categorie;
            $id++;
        }
        $availability['DatesUtc'] = $dateUTC;
        $availability['CategoryAvailabilities'] = $availabilities;
        $fileDir = __DIR__ . '/../Requestor/mock/services';
        if (!is_dir($fileDir)) {
            mkdir($fileDir, 0777, true);
        }
        $fp = fopen("{$fileDir}/getAvailability.json", 'w');
        fwrite($fp, json_encode($availability));
        fclose($fp);
    }
}
