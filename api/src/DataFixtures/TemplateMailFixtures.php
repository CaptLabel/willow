<?php

namespace App\DataFixtures;

use App\Entity\TemplateMail;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TemplateMailFixtures extends Fixture implements DependentFixtureInterface
{
    public function getDependencies()
    {
        return [
            PropertyFixtures::class,
            LanguageFixtures::class,
            MailTypeFixtures::class
        ];
    }

    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= PropertyFixtures::PROPERTY_NUMBER; $i++) {
            $mailTemplate = new TemplateMail();
            $mailTemplate->setLabel('2022 EN proposal');
            $mailTemplate->setProperty($this->getReference('property_' . $i));
            $mailTemplate->setDefaultSelection(1);
            $mailTemplate->setSubject('New offer');
            $mailTemplate->setType($this->getReference('mailType_1'));
            $mailTemplate->setBody('You can find in the link below the best offer for your request:');
            $mailTemplate->setBodySecond('We wait for your feedback on this offer!');
            $mailTemplate->setLanguage($this->getReference('language_1'));

            $manager->persist($mailTemplate);
            $manager->flush();
        }

        for ($i = 1; $i <= PropertyFixtures::PROPERTY_NUMBER; $i++) {
            $mailTemplate = new TemplateMail();
            $mailTemplate->setLabel('2022 EN pending');
            $mailTemplate->setProperty($this->getReference('property_' . $i));
            $mailTemplate->setDefaultSelection(1);
            $mailTemplate->setSubject('New request received');
            $mailTemplate->setType($this->getReference('mailType_2'));
            $mailTemplate->setBody('We received your request with the details below:');
            $mailTemplate->setBodySecond('We will get back to you as soon as possible!');
            $mailTemplate->setLanguage($this->getReference('language_1'));

            $manager->persist($mailTemplate);
            $manager->flush();
        }
    }
}
