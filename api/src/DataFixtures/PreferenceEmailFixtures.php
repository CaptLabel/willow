<?php

namespace App\DataFixtures;

use App\Entity\PreferenceEmail;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class PreferenceEmailFixtures extends Fixture implements FixtureGroupInterface
{
    public const PREFERENCE_EMAIL_COUNT = 10;

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        $preference_email_reference = [];
        for ($i = 1; $i <= self::PREFERENCE_EMAIL_COUNT; $i++) {
            $preferenceEmail = new PreferenceEmail();
            $preferenceEmail->setReceiverEnquiryFiltered($faker->email);
            $preferenceEmail->setSenderEnquiry($faker->email);
            $preference_email_reference['preference_email_' . $i] = $preferenceEmail;
            $manager->persist($preferenceEmail);
        }

        $manager->flush();

        foreach ($preference_email_reference as $key => $item) {
            $this->addReference($key, $item);
        }
    }

    public static function getGroups(): array
    {
        return ['testGroup'];
    }
}
