<?php

namespace App\DataFixtures;

use App\Entity\MailType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class MailTypeFixtures extends Fixture
{
    public const MAIL_TYPE_LIST = array(
        'Proposal',
        'Pending to quote',
    );

    public function load(ObjectManager $manager)
    {
        $mailType_reference = [];
        $i = 1;
        foreach (self::MAIL_TYPE_LIST as $mailTypeValue) {
            $mailType = new MailType();
            $mailType->setLabel($mailTypeValue);
            $mailType_reference['mailType_' . $i] = $mailType;
            $manager->persist($mailType);
            $i++;
        }

        $manager->flush();

        foreach ($mailType_reference as $key => $item) {
            $this->addReference($key, $item);
        }
    }
}
