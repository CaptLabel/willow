<?php

namespace App\DataFixtures;

use App\Entity\Status;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class StatusFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $status_reference = [];
        for ($i = 1; $i <= BrandFixtures::BRAND_NUMBER; $i++) {
            $status1 = new Status();
            $status1->setBrand($this->getReference('brand_' . $i));
            $brandId = $status1->getBrand()->getId();
            $status1->setLabel('Enquiry');
            $status1->setCancelType(0);
            $status_reference['status_' . $brandId] = $status1;
            $status1->setDefaultSelection(1);
            if ($i == 1) {
                $status1->setNewBrand(1);
            }

            $manager->persist($status1);
        }
        for ($i = 1; $i <= BrandFixtures::BRAND_NUMBER; $i++) {
            $status2 = new Status();
            $status2->setBrand($this->getReference('brand_' . $i));
            $status2->setLabel('Optional');
            $status2->setCancelType(0);
            $status2->setDefaultSelection(0);
            if ($i == 1) {
                $status2->setNewBrand(1);
            }
            $manager->persist($status2);
        }
        for ($i = 1; $i <= BrandFixtures::BRAND_NUMBER; $i++) {
            $status3 = new Status();
            $status3->setBrand($this->getReference('brand_' . $i));
            $status3->setLabel('Confirmed');
            $status3->setCancelType(0);
            $status3->setDefaultSelection(0);
            if ($i == 1) {
                $status3->setNewBrand(1);
            }

            $manager->persist($status3);
        }
        for ($i = 1; $i <= BrandFixtures::BRAND_NUMBER; $i++) {
            $status4 = new Status();
            $status4->setBrand($this->getReference('brand_' . $i));
            $status4->setLabel('Denied');
            $status4->setCancelType(1);
            $status4->setDefaultSelection(0);
            if ($i == 1) {
                $status4->setNewBrand(1);
            }

            $manager->persist($status4);
        }
        for ($i = 1; $i <= BrandFixtures::BRAND_NUMBER; $i++) {
            $status5 = new Status();
            $status5->setBrand($this->getReference('brand_' . $i));
            $status5->setLabel('Lost');
            $status5->setCancelType(1);
            $status5->setDefaultSelection(0);
            if ($i == 1) {
                $status5->setNewBrand(1);
            }

            $manager->persist($status5);
        }

        $manager->flush();

        foreach ($status_reference as $key => $item) {
            $this->addReference($key, $item);
        }
    }

    public function getDependencies()
    {
        return [
            BrandFixtures::class
        ];
    }

    public static function getGroups(): array
    {
        return ['testGroup'];
    }
}
