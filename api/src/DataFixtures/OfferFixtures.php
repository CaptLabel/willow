<?php

namespace App\DataFixtures;

use App\Entity\Offer;
use App\Entity\OfferRoom;
use App\Entity\OfferRoomRate;
use DateInterval;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use DoctrineExtensions\Query\Mysql\DateDiff;
use Faker\Factory;

class OfferFixtures extends Fixture implements DependentFixtureInterface
{
    public const OFFER_NUMBER = EnquiryFixtures::ENQUIRY_NUMBER;

    protected $faker;

    public function load(ObjectManager $manager)
    {
        $this->faker = Factory::create();

        $enquiries_reference = [];
        for ($i = 1; $i <= self::OFFER_NUMBER; $i++) {
            $offer = new Offer();
            $offer->setEnquiry($this->getReference('enquiry_' . $i));
            $propertyId = $offer->getEnquiry()->getProperty()->getId();
            for ($j = 1; $j <= rand(2, 4); $j++) {
                $offerRoom = new OfferRoom();
                $offerRoom->setCheckInDate($offer->getEnquiry()->getCheckInDate());
                $offerRoom->setCheckOutDate($offer->getEnquiry()->getCheckOutDate());
                $offerRoom->setRoomQuantity(rand(1, 6));
                $roomRef = 'property_' . $propertyId . '_roomtype_' . rand(1, count(RoomTypeFixtures::ROOMTYPE));
                $offerRoom->setRoomType($this->getReference($roomRef));
                $offerRoom->setPersonsInTheRoom($offerRoom->getRoomType()->getCapacity());
                $offerRoom->setAverageRate(rand(50, 300));
                $manager->persist($offerRoom);
                $offer->addRoom($offerRoom);
            }
            $manager->persist($offer);
        }

        $manager->flush();

        foreach ($enquiries_reference as $key => $item) {
            $this->addReference($key, $item);
        }
    }

    public function getDependencies()
    {
        return array(
            EnquiryFixtures::class,
            RoomTypeFixtures::class,
        );
    }
}
