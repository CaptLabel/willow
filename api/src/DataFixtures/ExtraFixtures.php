<?php

namespace App\DataFixtures;

use App\Entity\Extra;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ExtraFixtures extends Fixture implements DependentFixtureInterface
{
    public function getDependencies()
    {
        return [
            PropertyFixtures::class
        ];
    }

    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 8; $i++) {
            //first night per pax
            $extra = new Extra();
            $extra->setProperty($this->getReference('property_' . $i));
            $extra->setLabel('towels');
            $extra->setPrice(4);
            $extra->setCategory('test');
            $references['property_' . $i . '_extra_towels'] = $extra;

            $manager->persist($extra);

            //all night per pax
            $extra = new Extra();
            $extra->setProperty($this->getReference('property_' . $i));
            $extra->setLabel('breakfast');
            $extra->setPrice(10);
            $extra->setCategory('test');
            $references['property_' . $i . '_extra_breakfast'] = $extra;

            $manager->persist($extra);

            //first night per group
            $extra = new Extra();
            $extra->setProperty($this->getReference('property_' . $i));
            $extra->setLabel('event room');
            $extra->setPrice(500);
            $extra->setCategory('test');
            $references['property_' . $i . '_extra_room'] = $extra;

            $manager->persist($extra);

            //first night per room
            $extra = new Extra();
            $extra->setProperty($this->getReference('property_' . $i));
            $extra->setLabel('flowers');
            $extra->setPrice(25);
            $extra->setCategory('test');
            $references['property_' . $i . '_extra_flowers'] = $extra;

            $manager->persist($extra);

            //all night per room
            $extra = new Extra();
            $extra->setProperty($this->getReference('property_' . $i));
            $extra->setLabel('apero');
            $extra->setPrice(25);
            $extra->setCategory('test');
            $references['property_' . $i . '_extra_apero'] = $extra;

            $manager->persist($extra);
        }

        $manager->flush();

        foreach ($references as $key => $item) {
            $this->addReference($key, $item);
        }
    }
}
