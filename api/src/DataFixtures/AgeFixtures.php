<?php

namespace App\DataFixtures;

use App\Entity\Age;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class AgeFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $age_reference = [];
        for ($i = 1; $i <= BrandFixtures::BRAND_NUMBER; $i++) {
            $age1 = new Age();
            $age1->setBrand($this->getReference('brand_' . $i));
            $age1->setLabel('less than 16');
            $age1->setMax(16);
            $age_reference['age_' . $i] = $age1;

            $manager->persist($age1);
        }
        for ($i = 1; $i <= BrandFixtures::BRAND_NUMBER; $i++) {
            $age2 = new Age();
            $age2->setBrand($this->getReference('brand_' . $i));
            $age2->setMin(16);
            $age2->setMax(18);
            $age2->setLabel('16-18');

            $manager->persist($age2);
        }
        for ($i = 1; $i <= BrandFixtures::BRAND_NUMBER; $i++) {
            $age3 = new Age();
            $age3->setBrand($this->getReference('brand_' . $i));
            $age3->setMin(18);
            $age3->setLabel('more than 18');

            $manager->persist($age3);
        }

        $manager->flush();

        foreach ($age_reference as $key => $item) {
            $this->addReference($key, $item);
        }
    }

    public function getDependencies()
    {
        return [
            PropertyFixtures::class
        ];
    }

    public static function getGroups(): array
    {
        return ['testGroup'];
    }
}
