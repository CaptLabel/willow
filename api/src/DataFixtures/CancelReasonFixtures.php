<?php

namespace App\DataFixtures;

use App\Entity\CancelReason;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CancelReasonFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $cancel_reason_reference = [];
        for ($i = 1; $i <= BrandFixtures::BRAND_NUMBER; $i++) {
            $cancel_reason1 = new CancelReason();
            $cancel_reason1->setBrand($this->getReference('brand_' . $i));
            $cancel_reason1->setLabel('No longer travelling');
            $cancel_reason_reference['cancel_reason_' . $i] = $cancel_reason1;

            $manager->persist($cancel_reason1);
        }
        for ($i = 1; $i <= BrandFixtures::BRAND_NUMBER; $i++) {
            $cancel_reason2 = new CancelReason();
            $cancel_reason2->setBrand($this->getReference('brand_' . $i));
            $cancel_reason2->setLabel('Too expensive');
            $cancel_reason_reference['cancel_reason_' . $i] = $cancel_reason2;

            $manager->persist($cancel_reason2);
        }

        $manager->flush();

        foreach ($cancel_reason_reference as $key => $item) {
            $this->addReference($key, $item);
        }
    }

    public function getDependencies()
    {
        return [
            BrandFixtures::class
        ];
    }

    public static function getGroups(): array
    {
        return ['testGroup'];
    }
}
