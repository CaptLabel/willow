<?php

namespace App\Form;

use App\Entity\FollowUpPreference;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FollowUpPreferenceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('comment', TextType::class, [
                'label' => 'Comment',
                'required' => false,
            ])
            ->add('classification', NumberType::class, [
                'html5' => true,
                'label' => 'Follow up number*',
            ])
            ->add('days', NumberType::class, [
                'html5' => true,
                'label' => 'Days after enquiry creation *',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => FollowUpPreference::class,
        ]);
    }
}
