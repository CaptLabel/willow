<?php

namespace App\Form;

use App\Entity\Language;
use App\Entity\TemplateMail;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TemplateMailType extends AbstractType
{
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        SessionInterface $session,
        EntityManagerInterface $entityManager
    ) {
        $this->session = $session;
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $propertyId = $this->session->get('property_id');
        $property = $this->entityManager->getRepository('App:Property')->find($propertyId);

        $builder
            ->add('label', TextType::class, [
                'label' => 'Label *',
            ])
            ->add('subject', TextareaType::class, [
                'label' => 'Subject',
                'required' => false,
            ])
            ->add('body', TextareaType::class, [
                'label' => 'Mail body text first part',
                'required' => false,
            ])
            ->add('bodySecond', TextareaType::class, [
                'label' => 'Mail body text second part',
                'required' => false,
            ])
            ->add('defaultSelection')
            ->add('language', EntityType::class, [
                'class' => Language::class,
                'choices' => $property->getLanguage(),
                'choice_label' => 'label',
                'label' => 'Language *',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TemplateMail::class,
        ]);
    }
}
