<?php

namespace App\Form;

use App\Entity\Property;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class UserType extends AbstractType
{
    /**
     * @var Security
     */
    private $security;

    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(
        SessionInterface $session,
        Security $security
    ) {
        $this->session = $session;
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $this->security->getUser();

        $brandId = null;
        if (null !== $user->getBrand()) {
            $brandId = $user->getBrand()->getId();
        }
        if (null !== $this->session->get('brand')) {
            $brandId = $this->session->get('brand')->getId();
        }

        $builder
            ->add('email', TextType::class, [
                'label' => 'Email *',
            ])
            ->add('firstname', TextType::class, [
                'label' => 'Firstname *',
            ])
            ->add('lastname', TextType::class, [
                'label' => 'Lastname *',
            ])
            ->add('roles', ChoiceType::class, [
                'choices'  => [
                    'Revenue manager' => 'ROLE_REVENUE_MANAGER',
                    'Reservation agent' => 'ROLE_RESERVATION_AGENT',
                    'Admin' => 'ROLE_ADMIN_BRAND'
                ],
                'required' => true,
                'multiple' => false,
                'expanded' => false,
                'label' => 'Role *'
            ])
            ->add('properties', EntityType::class, [
                'class' => Property::class,
                'choice_label' => 'label',
                'query_builder' => function (EntityRepository $er) use ($user, $brandId) {
                    return $er->createQueryBuilder('p')
                        ->where('p.brand = :brand')
                        ->setParameter('brand', $brandId)
                        ->orderBy('p.label', 'ASC');
                },
                'multiple' => true,
                'expanded' => false,
            ])
        ;
        $builder->get('roles')
            ->addModelTransformer(new CallbackTransformer(
                function ($rolesArray) {
                    // transform the array to a string
                    return count($rolesArray) ? $rolesArray[0] : null;
                },
                function ($rolesString) {
                    // transform the string back to an array
                    return [$rolesString];
                }
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
