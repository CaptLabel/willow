<?php

namespace App\Form;

use App\Entity\Extra;
use App\Entity\OfferExtra;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OfferExtraType extends AbstractType
{
    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(
        SessionInterface $session
    ) {
        $this->session = $session;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $propertyId = $this->session->get('property_id');
        $builder
            ->add('extra', EntityType::class, [
                'class' => Extra::class,
                'choice_label' => 'label',
                'query_builder' => function (EntityRepository $er) use ($propertyId) {
                    return $er->createQueryBuilder('e')
                        ->where('e.property = :property')
                        ->setParameter('property', $propertyId)
                        ->orderBy('e.label', 'ASC');
                },
                'label' => 'Extra *',
            ])
            ->add('quantity', NumberType::class, [
                'html5' => false,
                'label' => 'Quantity *',
            ])
            ->add('startDate', DateType::class, [
                'widget' => 'single_text',
                'attr' => ['class' => 'start-date-field'],
                'label' => 'Start Date *',
            ])
            ->add('endDate', DateType::class, [
                'widget' => 'single_text',
                'attr' => ['class' => 'end-date-field'],
                'label' => 'End Date *',
            ])
            ->add('price', NumberType::class, [
                'html5' => false,
                'label' => 'Price per item *',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OfferExtra::class,
        ]);
    }
}
