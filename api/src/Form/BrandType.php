<?php

namespace App\Form;

use App\Entity\Brand;
use App\Entity\PropertyManagementSystem;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BrandType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('label')
            ->add('pmsGetData', EntityType::class, [
                'class' => PropertyManagementSystem::class,
                'choice_label' => 'label',
                'label' => 'PMS to get availability and rates from',
                'required' => false,
            ])
            ->add('apiKeyGetData', TextType::class, [
                'label' => 'API key of the PMS to get availability and rates from',
                'required' => false,
            ])
            ->add('pmsPostData', EntityType::class, [
                'class' => PropertyManagementSystem::class,
                'choice_label' => 'label',
                'label' => 'PMS where you send enquiry',
                'required' => false,
            ])
            ->add('apiKeyPostData', TextType::class, [
                'label' => 'API key of the PMS where you send enquiry',
                'required' => false,
            ])
            ->add('comment', TextareaType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Brand::class,
        ]);
    }
}
