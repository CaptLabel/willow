<?php

namespace App\Form;

use App\Entity\Age;
use App\Entity\RateRule;
use App\Entity\RateType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class RateRuleType extends AbstractType
{
    /**
     * @var Security
     */
    private $security;

    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(
        SessionInterface $session,
        Security $security
    ) {
        $this->security = $security;
        $this->session = $session;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $this->security->getUser();
        $propertyId = $this->session->get('property_id');
        $builder
            ->add('defaultSelection', HiddenType::class, [
                'data' => 0
            ])
            ->add('active')
            ->add('rate_type', EntityType::class, [
                'class' => RateType::class,
                'label' => 'Rate code *',
                'choice_label' => 'label',
                'query_builder' => function (EntityRepository $er) use ($propertyId) {
                    return $er->createQueryBuilder('rt')
                        ->where('rt.property = :property')
                        ->setParameter('property', $propertyId)
                        ->andWhere('rt.activated = 1')
                        ->orderBy('rt.label', 'ASC');
                },
                'placeholder' => 'Choose a rate type'
            ])
            ->add('date_start', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Date from *',
                'required' => true,
            ])
            ->add('date_end', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Date until *',
                'required' => true,
            ])
            ->add('day_selection', ChoiceType::class, [
                'choices'  => [
                    'Monday' => 'Monday',
                    'Tuesday' => 'Tuesday',
                    'Wednesday' => 'Wednesday',
                    'Thursday' => 'Thursday',
                    'Friday' => 'Friday',
                    'Saturday' => 'Saturday',
                    'Sunday' => 'Sunday',
                ],
                'multiple' => true,
                'required' => false
            ])
            ->add('base_rate_variable', NumberType::class, [
                'html5' => true,
                'label' => 'Percentage of the rate code applicable',
                'required' => false
            ])
            ->add('base_rate_fix', NumberType::class, [
                'html5' => true,
                'label' => 'Fix amount added or discounted to the rate code',
                'required' => false
            ])
            ->add('length_min', NumberType::class, [
                'html5' => true,
                'label' => 'Minimum length of stay',
                'required' => false
            ])
            ->add('length_max', NumberType::class, [
                'html5' => true,
                'label' => 'Maximum length of stay',
                'required' => false
            ])
            ->add('occupancyMin', NumberType::class, [
                'label' => 'Minimum occupancy to apply the rule',
                'html5' => true,
                'required' => false
            ])
            ->add('occupancyMax', NumberType::class, [
                'label' => 'Maximum occupancy to apply the rule',
                'html5' => true,
                'required' => false
            ])
            ->add('ageMin', EntityType::class, [
                'class' => Age::class,
                'query_builder' => function (EntityRepository $er) use ($user) {
                    return $er->createQueryBuilder('a')
                        ->where('a.brand = :brand')
                        ->setParameter('brand', $user->getBrand()->getId())
                        ->orderBy('a.label', 'ASC');
                },
                'choice_label' => 'label',
                'label' => 'Minimum age range to apply the rule',
                'required' => false,
                'choice_attr' => function ($choices, $key, $value) {
                    return ['data-min' => $choices->getMin()];
                },
            ])
            ->add('ageMax', EntityType::class, [
                'class' => Age::class,
                'query_builder' => function (EntityRepository $er) use ($user) {
                    return $er->createQueryBuilder('a')
                        ->where('a.brand = :brand')
                        ->setParameter('brand', $user->getBrand()->getId())
                        ->orderBy('a.label', 'ASC');
                },
                'choice_label' => 'label',
                'label' => 'Maximum age range to apply the rule',
                'required' => false,
                'choice_attr' => function ($choices, $key, $value) {
                    return ['data-min' => $choices->getMin()];
                },
            ])
            ->add('pax_min', NumberType::class, [
                'html5' => true,
                'label' => 'Minimum number of persons',
                'required' => false
            ])
            ->add('pax_max', NumberType::class, [
                'html5' => true,
                'label' => 'Maximum number of persons',
                'required' => false
            ])
            ->add('priority', ChoiceType::class, [
                'choices'  => [
                    'high' => 1,
                    'medium' => 2,
                    'low' => 3,
                ],
                'label' => 'Priority *'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => RateRule::class,
        ]);
    }
}
