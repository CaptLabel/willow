<?php

namespace App\Form;

use App\Entity\TemplatePresentation;
use App\Entity\Language;
use App\Entity\TemplateProposal;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TemplateProposalType extends AbstractType
{
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        SessionInterface $session,
        EntityManagerInterface $entityManager
    ) {
        $this->session = $session;
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $propertyId = $this->session->get('property_id');
        $property = $this->entityManager->getRepository('App:Property')->find($propertyId);

        $builder
            ->add('defaultSelection')
            ->add('label', TextType::class, [
                'label' => 'Label *',
            ])
            ->add('language', EntityType::class, [
                'class' => Language::class,
                'choices' => $property->getLanguage(),
                'choice_label' => 'label',
                'label' => 'Language *',
            ])
            ->add('templatePresentation', EntityType::class, [
                'class' => TemplatePresentation::class,
                'query_builder' => function (EntityRepository $er) use ($propertyId) {
                    return $er->createQueryBuilder('p')
                        ->where('p.property = :property')
                        ->setParameter('property', $propertyId)
                        ->orderBy('p.label', 'ASC');
                },
                'choice_label' => 'label',
                'label' => 'Template Presentation and Features',
                'placeholder' => '',
                'required' => false,
            ])
            ->add('content', TextareaType::class, [
                'label' => 'Proposal Model HTML content',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TemplateProposal::class,
        ]);
    }
}
