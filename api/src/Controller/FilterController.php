<?php

namespace App\Controller;

use App\Entity\Filter;
use App\Form\FilterType;
use App\Repository\FilterRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/filter")
 */
class FilterController extends AbstractController
{
    /**
     * @Route("/", name="filter_index", methods={"GET"})
     */
    public function index(FilterRepository $filterRepository): Response
    {
        if (!$this->container->get('session')->has('property_id')) {
            $this->addFlash('warning', 'No property selected');

            return $this->redirectToRoute('dashboard');
        }
        $propertyId = $this->container->get('session')->get('property_id');

        return $this->render('filter/index.html.twig', [
            'filters' => $filterRepository->findAllByProperty($propertyId),
        ]);
    }

    /**
     * @Route("/new", name="filter_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $filter = new Filter();
        $form = $this->createForm(FilterType::class, $filter);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($filter);
            $entityManager->flush();
            $this->container->get('session')->set('active-tab', 'general_filter');

            return $this->redirectToRoute('revenue_configuration');
        }

        return $this->render('filter/new.html.twig', [
            'filter' => $filter,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="filter_show", methods={"GET"})
     */
    public function show(Filter $filter): Response
    {
        return $this->render('filter/show.html.twig', [
            'filter' => $filter,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="filter_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Filter $filter): Response
    {
        $this->denyAccessUnlessGranted('edit', $filter);
        $form = $this->createForm(FilterType::class, $filter);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->container->get('session')->set('active-tab', 'general_filter');

            return $this->redirectToRoute('revenue_configuration');
        }

        return $this->render('filter/edit.html.twig', [
            'filter' => $filter,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="filter_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Filter $filter): Response
    {
        $this->denyAccessUnlessGranted('delete', $filter);
        if ($this->isCsrfTokenValid('delete' . $filter->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($filter);
            $entityManager->flush();
            $this->container->get('session')->set('active-tab', 'general_filter');
        }

        return $this->redirectToRoute('revenue_configuration');
    }
}
