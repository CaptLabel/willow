<?php

namespace App\Controller;

use App\Entity\BreakdownRuleAdd;
use App\Form\BreakdownRuleAddType;
use App\Repository\BreakdownRuleAddRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/breakdown/rule/add")
 */
class BreakdownRuleAddController extends AbstractController
{
    /**
     * @Route("/new", name="breakdown_rule_add_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $breakdownRuleAdd = new BreakdownRuleAdd();
        $form = $this->createForm(BreakdownRuleAddType::class, $breakdownRuleAdd);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($breakdownRuleAdd);
            $entityManager->flush();
            $this->container->get('session')->set('active-tab', 'breakdown_rules_add');

            return $this->redirectToRoute('revenue_configuration');
        }

        return $this->render('breakdown_rule_add/new.html.twig', [
            'breakdown_rule_add' => $breakdownRuleAdd,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="breakdown_rule_add_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, BreakdownRuleAdd $breakdownRuleAdd): Response
    {
        $this->denyAccessUnlessGranted('edit', $breakdownRuleAdd);
        $form = $this->createForm(BreakdownRuleAddType::class, $breakdownRuleAdd);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->container->get('session')->set('active-tab', 'breakdown_rules_add');

            return $this->redirectToRoute('revenue_configuration');
        }

        return $this->render('breakdown_rule_add/edit.html.twig', [
            'breakdown_rule_add' => $breakdownRuleAdd,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="breakdown_rule_add_delete", methods={"DELETE"})
     */
    public function delete(Request $request, BreakdownRuleAdd $breakdownRuleAdd): Response
    {
        $this->denyAccessUnlessGranted('delete', $breakdownRuleAdd);
        if ($this->isCsrfTokenValid('delete' . $breakdownRuleAdd->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($breakdownRuleAdd);
            $entityManager->flush();
            $this->container->get('session')->set('active-tab', 'breakdown_rules_add');
        }

        return $this->redirectToRoute('revenue_configuration');
    }
}
