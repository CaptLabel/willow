<?php

namespace App\Controller;

use App\Entity\Enquiry;
use App\Entity\FollowUp;
use App\Form\FollowUpType;
use App\Repository\FollowUpRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("/follow/up")
 */
class FollowUpController extends AbstractController
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(
        Security $security
    ) {
        $this->security = $security;
    }

    /**
     * @Route("/", name="follow_up_index", methods={"GET"})
     */
    public function index(FollowUpRepository $followUpRepository)
    {
        $propertyId = $this->container->get('session')->get('property_id');
        if (!$propertyId) {
            $this->addFlash('warning', 'No property selected');

            return $this->redirectToRoute('dashboard');
        }

        return $this->render('follow_up/index.html.twig', [
            'followups' => $followUpRepository->findPassed($propertyId),
        ]);
    }

    /**
     * @Route("/new/{id}", name="follow_up_new", methods={"GET","POST"})
     */
    public function new(Request $request, Enquiry $enquiry): Response
    {
        $followUp = new FollowUp();
        $form = $this->createForm(FollowUpType::class, $followUp);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $followUp->setEnquiry($enquiry);
            if ($enquiry->getFollowUps()->isEmpty()) {
                $followUp->setNumber(1);
            } else {
                $enquiryId = $enquiry->getId();
                $oldNumber = $this->getDoctrine()->getRepository('App:FollowUp')
                    ->findNumberLastCreated($enquiryId);
                $followUp->setNumber($oldNumber + 1);
            }
            $entityManager->persist($followUp);
            $entityManager->flush();
            $this->container->get('session')->set('active-tab-enquiry', 'enquiry_follow_up');

            return $this->redirectToRoute('enquiry_show', ['id' => $enquiry->getId()]);
        }

        return $this->render('follow_up/new.html.twig', [
            'follow_up' => $followUp,
            'enquiry' => $enquiry,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/validate/{id}", name="follow_up_validate", methods={"GET", "POST"})
     */
    public function validate(FollowUp $followUp): Response
    {
        $followUp->setExecutedDate(new \DateTime());
        $followUp->setUser($this->security->getUser());
        $this->getDoctrine()->getManager()->flush();
        $this->container->get('session')->set('active-tab-enquiry', 'enquiry_follow_up');
        return $this->redirectToRoute('enquiry_show', ['id' => $followUp->getEnquiry()->getId()]);
    }

    /**
     * @Route("/validate/list/{id}", name="follow_up_validate_list", methods={"GET", "POST"})
     */
    public function validateList(FollowUp $followUp): Response
    {
        $followUp->setUser($this->security->getUser());
        $followUp->setExecutedDate(new \DateTime());
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('follow_up_index');
    }

    /**
     * @Route("/show/enquiry/{id}", name="show_follow_up_in_enquiry", methods={"GET", "POST"})
     */
    public function showInEnquiry(FollowUp $followUp): Response
    {
        $this->container->get('session')->set('active-tab-enquiry', 'enquiry_follow_up');
        return $this->redirectToRoute('enquiry_show', ['id' => $followUp->getEnquiry()->getId()]);
    }

    /**
     * @Route("/{id}/edit", name="follow_up_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, FollowUp $followUp): Response
    {
        $form = $this->createForm(FollowUpType::class, $followUp);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->container->get('session')->set('active-tab-enquiry', 'enquiry_follow_up');

            return $this->redirectToRoute('enquiry_show', ['id' => $followUp->getEnquiry()->getId()]);
        }

        return $this->render('follow_up/edit.html.twig', [
            'follow_up' => $followUp,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="follow_up_delete", methods={"DELETE"})
     */
    public function delete(Request $request, FollowUp $followUp): Response
    {
        $enquiryId = $followUp->getEnquiry()->getId();

        if ($this->isCsrfTokenValid('delete' . $followUp->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($followUp);
            $entityManager->flush();
            $this->container->get('session')->set('active-tab-enquiry', 'enquiry_follow_up');
        }

        return $this->redirectToRoute('enquiry_show', ['id' => $enquiryId]);
    }
}
