<?php

namespace App\Controller;

use App\Entity\BreakdownRuleAdd;
use App\Entity\BreakdownRuleRemaining;
use App\Entity\BreakdownRuleTotal;
use App\Entity\RoomType;
use App\Form\RoomTypeType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/room_type")
 */
class RoomTypeController extends AbstractController
{
    /**
     * @Route("/{id}/edit", name="room_type_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, RoomType $roomType): Response
    {
        $this->denyAccessUnlessGranted('edit', $roomType);
        $form = $this->createForm(RoomTypeType::class, $roomType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $eManager = $this->getDoctrine()->getManager();
            $propertyId = $roomType->getProperty();
            $priorityId = $roomType->getPriority();
            $roomTypeId = $roomType->getId();
            $samePrio = $eManager->getRepository(RoomType::class)->findSamePriorityByP($propertyId, $priorityId);
            $lowestPrio = $eManager->getRepository(RoomType::class)->findLowestPriorityByP($propertyId);
            $newLowestPrio = $lowestPrio['priority'] + 1;
            if ($samePrio) {
                if ($roomTypeId != $samePrio->getId()) {
                    $samePrio->setPriority($newLowestPrio);
                }
            }
            if ($roomType->getPriority() === null && $roomType->getGroupOffers() === true) {
                $roomType->setPriority($newLowestPrio);
            }
            if ($roomType->getGroupOffers() === false) {
                $roomType->setPriority(null);
            }
            $eManager->flush();
            $this->container->get('session')->remove('active-tab');

            if (!$roomType->getGroupOffers()) {
                $this->addFlash('warning', 'The updated room is not used for group offers, check your breakdown rules');
            }
            return $this->redirectToRoute('property_configuration');
        }

        return $this->render('room_type/edit.html.twig', [
            'room_type' => $roomType,
            'form' => $form->createView(),
        ]);
    }
}
