<?php

namespace App\Controller;

use App\Entity\BreakdownPreferences;
use App\Form\BreakdownPreferencesType;
use App\Repository\BreakdownPreferencesRepository;
use App\Repository\EnquiryRepository;
use App\Service\BrandIfAdminWillow;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/breakdown_preferences")
 */
class BreakdownPreferencesController extends AbstractController
{
    /**
     * @Route("/new", name="breakdown_preferences_new", methods={"GET", "POST"})
     */
    public function new(
        Request $request,
        BreakdownPreferencesRepository $breakdownPreferencesRepository,
        BrandIfAdminWillow $ifAdminWillow
    ): Response {
        $breakdownPreference = new BreakdownPreferences();
        $session = $this->container->get('session');
        $roleAdminWillow = $this->isGranted('ROLE_ADMIN_WILLOW');
        $brand = $ifAdminWillow->getBrandViaRepoDependingOnUser($session, $roleAdminWillow);
        $form = $this->createForm(BreakdownPreferencesType::class, $breakdownPreference);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $breakdownPreference->setBrand($brand);
            $breakdownPreferencesRepository->add($breakdownPreference);
            return $this->redirectToRoute('brand_preferences');
        }

        return $this->render('breakdown_preferences/new.html.twig', [
            'breakdown_preference' => $breakdownPreference,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="breakdown_preferences_edit", methods={"GET", "POST"})
     */
    public function edit(
        Request $request,
        BreakdownPreferences $breakdownPreference,
        BreakdownPreferencesRepository $breakdownPreferencesRepository
    ): Response {
        $form = $this->createForm(BreakdownPreferencesType::class, $breakdownPreference);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $breakdownPreferencesRepository->add($breakdownPreference);
            return $this->redirectToRoute('brand_preferences');
        }

        return $this->render('breakdown_preferences/edit.html.twig', [
            'breakdown_preference' => $breakdownPreference,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="breakdown_preferences_delete", methods={"POST"})
     */
    public function delete(
        Request $request,
        BreakdownPreferences $breakdownPreference,
        EnquiryRepository $enquiryRepository
    ): Response {
        $brand = $breakdownPreference->getBrand();
        if ($this->isCsrfTokenValid('delete' . $breakdownPreference->getId(), $request->request->get('_token'))) {
            $breakdownIsUsed = $enquiryRepository->findBreakdownSetUpByBrand($brand, $breakdownPreference);
            if ($breakdownIsUsed) {
                $this->addFlash('error', 'This breakdown setup is used in an enquiry, you cannot delete it.');
            } else {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($breakdownPreference);
                $entityManager->flush();
            }
        }

        return $this->redirectToRoute('brand_preferences');
    }
}
