<?php

namespace App\Controller;

use App\Builder\OccupancyBuilder;
use App\Builder\OccupancyOfferBuilder;
use App\Builder\OfferBuilder;
use App\Entity\Company;
use App\Entity\Enquiry;
use App\Entity\Offer;
use App\Form\CompanyNewType;
use App\Form\EnquiryType;
use App\Form\GenerateOfferType;
use App\Form\OfferType;
use App\Form\OptionDateType;
use App\Form\SimulateType;
use App\Repository\EnquiryRepository;
use App\Service\HistoryManager;
use App\Service\NewEnquirySetUp;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("/enquiry")
 */
class EnquiryController extends AbstractController
{
    /**
     * @var OfferBuilder
     */
    private $offerBuilder;

    /**
     * @var Security
     */
    private $security;

    public function __construct(
        OfferBuilder $offerBuilder,
        Security $security
    ) {
        $this->offerBuilder = $offerBuilder;
        $this->security = $security;
    }

    /**
     * @Route("/new", name="enquiry_new", methods={"GET","POST"})
     */
    public function new(Request $request, NewEnquirySetUp $setUp): Response
    {
        if ($this->container->get('session')->get('property_id') == null) {
            $this->addFlash('warning', 'No property selected');

            return $this->redirectToRoute('dashboard');
        }
        $enquiry = new Enquiry();
        $user = $this->security->getUser();
        if ($this->isGranted('ROLE_ADMIN_WILLOW')) {
            $brand = $this->container->get('session')->get('brand');
            $brandId = $brand->getId();
        } else {
            $brandId = $user->getBrand()->getId();
        }
        $entityManager = $this->getDoctrine()->getManager();
        $propertyId = $this->container->get('session')->get('property_id');
        $property = $entityManager->getRepository('App:Property')->find($propertyId);
        $brand = $property->getBrand();
        $defaultStatus = $entityManager->getRepository('App:Status')->findDefaultStatusByBrand($brandId);
        $setUp->createManualBeforeForm($enquiry, $property, $defaultStatus);
        $enquiryForm = $this->createForm(EnquiryType::class, $enquiry);
        $enquiryForm->handleRequest($request);
        $newCompanyForm = $this->createForm(CompanyNewType::class);

        if ($enquiryForm->isSubmitted() && $enquiryForm->isValid()) {
            $setUp->createManualAfterForm($enquiry, $property, $defaultStatus, $user);

            return $this->redirectToRoute('enquiry_show', ['id' => $enquiry->getId()]);
        }

        return $this->render('enquiry/show.html.twig', [
            'enquiry' => $enquiry,
            'enquiryForm' => $enquiryForm->createView(),
            'newCompanyForm' => $newCompanyForm->createView(),
            'brand' => $brand,
        ]);
    }

    /**
     * @Route("/{id}", name="enquiry_show", methods={"GET","POST"}, requirements={"id"="\d+"})
     */
    public function show(Enquiry $enquiry, OccupancyOfferBuilder $occBuilder, OccupancyBuilder $occGeneral): Response
    {
        $property = $enquiry->getProperty();
        $brand = $property->getBrand();
        $propertyId = $property->getId();
        $enquiryId = $enquiry->getId();
        $entityManager = $this->getDoctrine()->getManager();
        if ($this->isGranted('ROLE_ADMIN_BRAND')) {
            $brandId = $brand->getId();
            $properties = $entityManager->getRepository('App:Property')->findAllByBrand($brandId);
            $propertyList = array_map(function ($property) {
                return $property->getId();
            }, $properties);
        } else {
            $propertyList = $this->getUser()->getPropertyListid();
        }
        if (!in_array($propertyId, $propertyList)) {
            return $this->redirectToRoute('dashboard');
        }

        $this->container->get('session')->set('property_id', $enquiry->getProperty()->getId());
        $this->container->get('session')->set('property_label', $enquiry->getProperty()->getLabel());

        $occupancy = $occBuilder->getOccupancy($enquiry);
        $roomsOverbooked = $occBuilder->getRoomsOverbooked($enquiry);
        $occupancyGeneral = $occGeneral->findOccupancy($enquiry);
        $enquiryForm = $this->createForm(EnquiryType::class, $enquiry);
        $newCompany = new Company();
        $newCompanyForm = $this->createForm(CompanyNewType::class, $newCompany);
        $modifyCompanyForm = $this->createForm(CompanyNewType::class, $enquiry->getCompany());
        $optionDateForm = $this->createForm(OptionDateType::class);
        $generateForm = $this->createForm(GenerateOfferType::class);
        if (!$offer = $enquiry->getOffer()) {
            $offer = new Offer();
        }
        $offerForm = $this->createForm(OfferType::class, $offer);
        $histories = $entityManager->getRepository('App:History')->findByEnquiry($enquiryId);

        return $this->render('enquiry/show.html.twig', [
            'enquiry' => $enquiry,
            'enquiryForm' => $enquiryForm->createView(),
            'offerForm' => $offerForm->createView(),
            'newCompanyForm' => $newCompanyForm->createView(),
            'optionDateForm' => $optionDateForm->createView(),
            'generateForm' => $generateForm->createView(),
            'histories' => $histories,
            'modifyCompanyForm' => $modifyCompanyForm->createView(),
            'brand' => $brand,
            'occupancy' => $occupancy,
            'occupancy_general' => $occupancyGeneral,
            'rooms_overbooked' => $roomsOverbooked,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="enquiry_edit", methods={"GET","POST"}, requirements={"id"="\d+"})
     */
    public function edit(Request $request, Enquiry $enquiry, HistoryManager $historyManager): Response
    {
        $form = $this->createForm(EnquiryType::class, $enquiry);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $statusType = $enquiry->getStatus()->getCancelType();
            if ($statusType == null) {
                $enquiry->setCancelReason(null);
            }
            $historyManager->enquiryHistorySetter($enquiry);
            $this->getDoctrine()->getManager()->flush();
            $this->container->get('session')->set('active-tab-enquiry', 'enquiry_info');
        }

        return $this->redirectToRoute('enquiry_show', ['id' => $enquiry->getId()]);
    }

    /**
     * @Route("/pending", name="enquiry_pending", methods={"GET"})
     */
    public function pending(EnquiryRepository $enquiryRepository)
    {
        $propertyId = $this->container->get('session')->get('property_id');
        if (!$propertyId) {
            $this->addFlash('warning', 'No property selected');

            return $this->redirectToRoute('dashboard');
        }

        return $this->render('enquiry/pending.html.twig', [
            'enquiries' => $enquiryRepository->findAllPendingByProperty($propertyId),
        ]);
    }

    /**
     * @Route("/{id}/generate", name="enquiry_generate", methods={"GET","POST"}, requirements={"id"="\d+"})
     */
    public function generate(Enquiry $enquiry, Request $request, HistoryManager $historyManager)
    {
        $generateForm = $this->createForm(GenerateOfferType::class);
        $generateForm->handleRequest($request);
        $user = $this->security->getUser();

        if ($generateForm->isSubmitted() && $generateForm->isValid()) {
            if ($offer = $enquiry->getOffer()) {
                $historyManager->generateNewOffer($offer, $user);
            }
            if ($generateForm['breakdown']->getData() === true) {
                $this->offerBuilder->buildWithBreakdown($enquiry);
            } else {
                $this->offerBuilder->build($enquiry);
            }
            $this->offerBuilder->save();
            $this->container->get('session')->set('active-tab-enquiry', 'offer_rooms');
        }

        return $this->redirectToRoute('enquiry_show', ['id' => $enquiry->getId()]);
    }

    /**
     * @Route("/simulate", name="enquiry_simulate", methods={"GET","POST"})
     */
    public function simulate(Request $request): Response
    {
        $enquiry = new Enquiry();
        $form = $this->createForm(SimulateType::class, $enquiry);
        $form->handleRequest($request);
        $offer = null;

        if ($form->isSubmitted() && $form->isValid()) {
            $this->offerBuilder->build($enquiry);
            $offer = $this->offerBuilder->get();
        }

        return $this->render('enquiry/simulate.html.twig', [
            'enquiry' => $enquiry,
            'form' => $form->createView(),
            'offer' => $offer,
        ]);
    }

    /**
     * @Route("/switch/{id}", name="enquiry_switch", methods={"GET","POST"}, requirements={"id"="\d+"})
     */
    public function switchEnquiry(Enquiry $enquiry, Request $request): Response
    {
        $property = $enquiry->getProperty();
        $brand = $property->getBrand();
        $propertyId = $property->getId();
        $entityManager = $this->getDoctrine()->getManager();
        if ($this->isGranted('ROLE_ADMIN_BRAND')) {
            $brandId = $brand->getId();
            $properties = $entityManager->getRepository('App:Property')->findAllByBrand($brandId);
            $propertyList = array_map(function ($property) {
                return $property->getId();
            }, $properties);
        } else {
            $propertyList = $this->getUser()->getPropertyListid();
        }
        if (!in_array($propertyId, $propertyList)) {
            return $this->redirectToRoute('dashboard');
        }
        if ($enquiry->getActive()) {
            $enquiry->setActive(0);
        } else {
            $enquiry->setActive(1);
        }
        $entityManager->persist($enquiry);
        $entityManager->flush();

        return $this->redirect($request->headers->get('referer'));
    }
}
