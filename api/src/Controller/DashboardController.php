<?php

namespace App\Controller;

use App\Form\PreferenceType;
use App\Generator\EncryptDecryptGenerator;
use App\Service\BrandIfAdminWillow;
use App\Service\BreakdownEnquirySetUp;
use App\Service\DefaultLanguage;
use DateInterval;
use DatePeriod;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class DashboardController extends AbstractController
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function index(BrandIfAdminWillow $ifAdminWillow): Response
    {
        $this->container->get('session')->set('active-tab', null);
        $this->container->get('session')->set('active-tab-enquiry', null);
        $this->container->get('session')->set('active-tab-preference', null);
        $propertyId = $this->container->get('session')->get('property_id');
        if ($this->isGranted('ROLE_ADMIN_WILLOW')) {
            $brand = $this->container->get('session')->get('brand');
        } else {
            $brand = $this->security->getUser()->getBrand();
        }
        $today = new \DateTimeImmutable();
        $yesterday = $today->sub(new DateInterval('P1D'));
        $lastNight = $yesterday->setTime(23, 59);
        $threeDaysAgo = $today->sub(new DateInterval('P3D'));
        $tenDaysAgoWithoutTime = $today->sub(new DateInterval('P10D'));
        $tenDaysAgo = $tenDaysAgoWithoutTime->setTime(23, 59);
        $oneMonthAgo = $today->sub(new DateInterval('P30D'));
        $intervalOneDay = new DateInterval('P1D');
        $lastEnquiries = [];
        $lastEnquiriesBrand = [];
        $periodTenDays = [];
        $period = new DatePeriod($tenDaysAgo, $intervalOneDay, $today);
        foreach ($period as $date) {
            $periodTenDays[] = $date->format('d-m');
        }
        $groupName = [];
        $roomTotal = [];
        $followUpFirst = [];
        $followUpSecond = [];
        $followUpThird = [];
        $enquiryPendingMax = [];
        $enquiryPendingOneDay = [];
        $enquiryPendingThreeDay = [];
        $lastTenDaysNumberEnq = [];
        $lastTenDaysNumberEnqBrand = [];
        if (null === $propertyId && null !== $brand) {
            $lastTenRepoBrand = $this->getDoctrine()->getRepository('App:Enquiry')
                ->findLastTenDaysBrand($brand, $tenDaysAgo, $lastNight);
            $lastTenDataBrand = array_column($lastTenRepoBrand, 'count', 'date');
            $periodFlipBrand = array_flip($periodTenDays);
            $periodAtZeroBrand = array_fill_keys(array_keys($periodFlipBrand), 0);
            $lastTenDaysNumberEnqBrand = array_merge($periodAtZeroBrand, $lastTenDataBrand);
            if ($this->isGranted('ROLE_ADMIN_BRAND')) {
                $brand = $this->security->getUser()->getBrand();
                if (!$brand) {
                    $brandId = $this->container->get('session')->get('brand')->getId();
                } else {
                    $brandId = $brand->getId();
                }
                $properties = $this->getDoctrine()->getRepository('App:Property')->findAllByBrand($brandId);
                $propertyList = array_map(function ($property) {
                    return $property->getId();
                }, $properties);
            } else {
                $propertyList = $this->getUser()->getPropertyListid();
            }
            $lastEnquiriesBrand = $this->getDoctrine()->getRepository('App:Enquiry')
                ->findLastTenByBrand($propertyList);
        }
        if (null !== $propertyId) {
            $highestValue = $this->getDoctrine()->getRepository('App:Enquiry')
                ->findHighestValueLastMonth($propertyId, $oneMonthAgo);
            $lastEnquiries = $this->getDoctrine()->getRepository('App:Enquiry')
                ->findLastByProperty($propertyId);
            $followUpFirst = $this->getDoctrine()->getRepository('App:FollowUp')
                ->findPassedFirst($propertyId);
            $followUpSecond = $this->getDoctrine()->getRepository('App:FollowUp')
                ->findPassedSecond($propertyId);
            $followUpThird = $this->getDoctrine()->getRepository('App:FollowUp')
                ->findPassedThird($propertyId);
            $enquiryPendingOneDay = $this->getDoctrine()->getRepository('App:Enquiry')
                ->findPendingOneDayByProperty($propertyId, $yesterday);
            $enquiryPendingThreeDay = $this->getDoctrine()->getRepository('App:Enquiry')
                ->findPendingThreeDayByProperty($propertyId, $threeDaysAgo, $yesterday);
            $enquiryPendingMax = $this->getDoctrine()->getRepository('App:Enquiry')
                ->findPendingMaxByProperty($propertyId, $threeDaysAgo);
            $lastTenRepo = $this->getDoctrine()->getRepository('App:Enquiry')
                ->findLastTenDays($propertyId, $tenDaysAgo, $lastNight);
            $lastTenData = array_column($lastTenRepo, 'count', 'date');
            $periodFlip = array_flip($periodTenDays);
            $periodAtZero = array_fill_keys(array_keys($periodFlip), 0);
            $lastTenDaysNumberEnq = array_merge($periodAtZero, $lastTenData);
            $groupName = array_column($highestValue, 'group_name');
            $roomTotal = array_column($highestValue, 'room_total');
        }

        return $this->render('dashboard/index.html.twig', [
            'group_name' => $groupName,
            'room_total' => $roomTotal,
            'last_enquiry' => $lastEnquiries,
            'last_enquiry_brand' => $lastEnquiriesBrand,
            'follow_up_first' => $followUpFirst,
            'follow_up_sec' => $followUpSecond,
            'follow_up_third' => $followUpThird,
            'pending_max' => $enquiryPendingMax,
            'pending_one_day' => $enquiryPendingOneDay,
            'pending_three_day' => $enquiryPendingThreeDay,
            'last_ten_days' => $lastTenDaysNumberEnq,
            'last_ten_days_brand' => $lastTenDaysNumberEnqBrand,
            'period_ten_days' => $periodTenDays,
        ]);
    }

    /**
     * @Route("/revenue-configuration", name="revenue_configuration")
     */
    public function revenueConfiguration(): Response
    {
        $propertyId = $this->container->get('session')->get('property_id');
        $defaultRate = $this->getDoctrine()->getRepository('App:RateRule')
            ->findDefaultRateRuleByProperty($propertyId);

        return $this->render('revenue_configuration/index.html.twig', [
            'default_rate' => $defaultRate,
            'filters' => $this->getDoctrine()->getRepository('App:Filter')
                ->findAllByProperty($propertyId),
            'breakdown_rule_adds' => $this->getDoctrine()->getRepository('App:BreakdownRuleAdd')
                ->findAllByProperty($propertyId),
            'breakdown_rule_remainings' => $this->getDoctrine()->getRepository('App:BreakdownRuleRemaining')
                ->findAllByProperty($propertyId),
            'breakdown_rule_totals' => $this->getDoctrine()->getRepository('App:BreakdownRuleTotal')
                ->findAllByProperty($propertyId),
            'rate_rules' => $this->getDoctrine()->getRepository('App:RateRule')
                ->findRateRulesByProperty($propertyId),
            'extra_rules' => $this->getDoctrine()->getRepository('App:ExtraRule')
                ->findAllByProperty($propertyId),
        ]);
    }

    /**
     * @Route("/property-configuration", name="property_configuration")
     */
    public function propertyConfiguration(): Response
    {
        $propertyId = $this->container->get('session')->get('property_id');
        $property = $this->getDoctrine()->getRepository('App:Property')->find($propertyId);
        $brand = $property->getBrand();
        $rateTypes = $this->getDoctrine()->getRepository('App:RateType')
            ->findByProperty($propertyId);
        $inactiveRateType = $this->getDoctrine()->getRepository('App:RateRule')
            ->findRateRuleInactiveRate($propertyId);
        $inactiveAddRoomType = $this->getDoctrine()->getRepository('App:BreakdownRuleAdd')
            ->findInactiveRoom($propertyId);
        $inactiveRemainingRoomType = $this->getDoctrine()->getRepository('App:BreakdownRuleRemaining')
            ->findInactiveRoom($propertyId);
        $inactiveTotalRoomType = $this->getDoctrine()->getRepository('App:BreakdownRuleTotal')
            ->findInactiveRoom($propertyId);
        $array1 = array_column($inactiveAddRoomType, 'label', 'label');
        $array2 = array_column($inactiveRemainingRoomType, 'label', 'label');
        $array3 = array_column($inactiveTotalRoomType, 'label', 'label');
        $inactiveRoomType = array_merge($array1, $array2, $array3);

        if (!$propertyId) {
            return $this->redirectToRoute('dashboard');
        }

        return $this->render('property_configuration/index.html.twig', [
            'rate_types' => $rateTypes,
            'inactive_rate_types' => $inactiveRateType,
            'room_types' => $this->getDoctrine()->getRepository('App:RoomType')
                ->findAllByProperty($propertyId),
            'room_types_offers' => $this->getDoctrine()->getRepository('App:RoomType')
                ->findGroupOffersByProperty($propertyId),
            'inactive_room_types' => $inactiveRoomType,
            'extras' => $this->getDoctrine()->getRepository('App:Extra')
                ->findByProperty($propertyId),
            'brand' => $brand,
        ]);
    }

    /**
     * @Route("/preferences", name="preferences")
     */
    public function preferences(
        Request $request,
        DefaultLanguage $defaultLanguage,
        EncryptDecryptGenerator $generator,
        BreakdownEnquirySetUp $breakdownEnquirySetUp
    ): Response {
        $propertyId = $this->container->get('session')->get('property_id');
        if (!$propertyId) {
            return $this->redirectToRoute('dashboard');
        }
        $property = $this->getDoctrine()->getRepository('App:Property')->find($propertyId);
        $mailProposalDefault = $defaultLanguage->mailProposalWithoutDefault($property);
        $mailPendingDefault = $defaultLanguage->mailPendingWithoutDefault($property);
        $conditionDefault = $defaultLanguage->conditionWithoutDefault($property);
        $proposalDefault = $defaultLanguage->proposalWithoutDefault($property);

        $generator->decryptPropertyToken($property);
        $preferenceForm = $this->createForm(PreferenceType::class, $property);
        $preferenceForm->handleRequest($request);
        if ($preferenceForm->isSubmitted() && $preferenceForm->isValid()) {
            $breakdownEnquirySetUp->changeEnquiries($property);
            $generator->encryptPropertyToken($property);
            $this->getDoctrine()->getManager()->flush();
            $this->container->get('session')->set('property_label', $property->getLabel());

            return $this->redirectToRoute('preferences', [
                'id' => $property->getId(),
            ]);
        }

        return $this->render('preference/index.html.twig', [
            'termsconditions' => $this->getDoctrine()->getRepository('App:TemplateCondition')
                ->findAllByProperty($propertyId),
            'condition_default' => $conditionDefault,
            'preferenceForm' => $preferenceForm->createView(),
            'presentations' => $this->getDoctrine()->getRepository('App:TemplatePresentation')
                ->findAllByProperty($propertyId),
            'mail_templates' => $this->getDoctrine()->getRepository('App:TemplateMail')
                ->findAllByProperty($propertyId),
            'mail_proposal_default' => $mailProposalDefault,
            'mail_pending_default' => $mailPendingDefault,
            'follow_up_preferences' => $this->getDoctrine()->getRepository('App:FollowUpPreference')
                ->findOrderByProperty($propertyId),
            'proposal_templates' => $this->getDoctrine()->getRepository('App:TemplateProposal')
                ->findAllByProperty($propertyId),
            'proposal_default' => $proposalDefault,
        ]);
    }

    /**
     * @Route("/brand-preferences", name="brand_preferences", methods={"GET"})
     */
    public function brandPreferences(BrandIfAdminWillow $ifAdminWillow): Response
    {
        $session = $this->container->get('session');
        $roleAdminWillow = $this->isGranted('ROLE_ADMIN_WILLOW');
        $brandId = $ifAdminWillow->getBrandIdDependingOnUser($session, $roleAdminWillow);

        return $this->render('brand_preferences/index.html.twig', [
            'breakdowns' => $this->getDoctrine()->getRepository('App:BreakdownPreferences')->findByBrand($brandId),
            'ages' => $this->getDoctrine()->getRepository('App:Age')->findByBrand($brandId),
            'statuses' => $this->getDoctrine()->getRepository('App:Status')->findByBrand($brandId),
            'cancels' => $this->getDoctrine()->getRepository('App:CancelReason')->findByBrand($brandId),
        ]);
    }
}
