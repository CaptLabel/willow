<?php

namespace App\Controller;

use App\Entity\Status;
use App\Form\StatusType;
use App\Repository\EnquiryRepository;
use App\Service\BrandIfAdminWillow;
use App\Service\DefaultStatus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("/status")
 */
class StatusController extends AbstractController
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/new", name="status_new", methods={"GET","POST"})
     */
    public function new(Request $request, DefaultStatus $defaultStatus, BrandIfAdminWillow $ifAdminWillow): Response
    {
        $status = new Status();
        $form = $this->createForm(StatusType::class, $status);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $session = $this->container->get('session');
            $roleAdminWillow = $this->isGranted('ROLE_ADMIN_WILLOW');
            $brand = $ifAdminWillow->getBrandViaRepoDependingOnUser($session, $roleAdminWillow);
            $status->setBrand($brand);
            $brandId = $brand->getId();
            $defaultStatus->defaultOptional($status, $brandId);
            $defaultStatus->defaultConfirmed($status, $brandId);
            $entityManager->persist($status);
            $entityManager->flush();

            return $this->redirectToRoute('brand_preferences');
        }

        return $this->render('status/new.html.twig', [
            'status' => $status,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="status_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Status $status, DefaultStatus $defaultStatus): Response
    {
        $form = $this->createForm(StatusType::class, $status);
        $form->handleRequest($request);
        $brand = $status->getBrand()->getId();

        if ($form->isSubmitted() && $form->isValid()) {
            $defaultStatus->defaultOptional($status, $brand);
            $defaultStatus->defaultConfirmed($status, $brand);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('brand_preferences');
        }

        return $this->render('status/edit.html.twig', [
            'status' => $status,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("{id}", name="status_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Status $status, EnquiryRepository $enquiryRepository): Response
    {
        $brand = $status->getBrand();
        if ($this->isCsrfTokenValid('delete' . $status->getId(), $request->request->get('_token'))) {
            $statusIsUsed = $enquiryRepository->findStatusByBrand($brand, $status);
            if ($statusIsUsed) {
                $this->addFlash('error', 'This status is used in an enquiry, you cannot delete it.');
            } else {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($status);
                $entityManager->flush();
            }
        }

        return $this->redirectToRoute('brand_preferences');
    }
}
