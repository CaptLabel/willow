<?php

namespace App\Controller;

use App\Entity\TemplateMail;
use App\Form\TemplateMailType;
use App\Repository\MailTypeRepository;
use App\Service\DefaultLanguage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/mail_template")
 */
class TemplateMailController extends AbstractController
{
    private $mailType;

    public function __construct(MailTypeRepository $mailType)
    {
        $this->mailType = $mailType;
    }

    /**
     * @Route("/new/proposal", name="mail_template_new_proposal", methods={"GET","POST"})
     */
    public function newProposal(Request $request, DefaultLanguage $defaultLanguage): Response
    {
        $propertyId = $this->container->get('session')->get('property_id');
        $mailTemplate = new TemplateMail();
        $form = $this->createForm(TemplateMailType::class, $mailTemplate);
        $form->handleRequest($request);
        $proposalType = $this->mailType->findOneBy(['label' => 'Proposal']);

        if ($form->isSubmitted() && $form->isValid()) {
            $mailTemplate->setType($proposalType);
            $defaultLanguage->defaultMailProposal($mailTemplate, $propertyId);
            $this->getDoctrine()->getManager()->persist($mailTemplate);
            $this->getDoctrine()->getManager()->flush();
            $this->container->get('session')->set('active-tab-preference', 'mail_template');

            return $this->redirectToRoute('preferences');
        }

        return $this->render('mail_template/new_proposal.html.twig', [
            'mail_templates' => $mailTemplate,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new/pending", name="mail_template_new_pending", methods={"GET","POST"})
     */
    public function newPending(Request $request, DefaultLanguage $defaultLanguage): Response
    {
        $propertyId = $this->container->get('session')->get('property_id');
        $mailTemplate = new TemplateMail();
        $form = $this->createForm(TemplateMailType::class, $mailTemplate);
        $form->handleRequest($request);
        $pendingType = $this->mailType->findOneBy(['label' => 'Pending to quote']);

        if ($form->isSubmitted() && $form->isValid()) {
            $mailTemplate->setType($pendingType);
            $defaultLanguage->defaultMailPending($mailTemplate, $propertyId);
            $this->getDoctrine()->getManager()->persist($mailTemplate);
            $this->getDoctrine()->getManager()->flush();
            $this->container->get('session')->set('active-tab-preference', 'mail_template');

            return $this->redirectToRoute('preferences');
        }

        return $this->render('mail_template/new_pending.html.twig', [
            'mail_templates' => $mailTemplate,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit/proposal", name="mail_template_edit_proposal", methods={"GET","POST"})
     */
    public function editProposal(Request $request, TemplateMail $mail, DefaultLanguage $defaultLanguage): Response
    {
        $this->denyAccessUnlessGranted('edit', $mail);
        $propertyId = $this->container->get('session')->get('property_id');
        $form = $this->createForm(TemplateMailType::class, $mail);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $defaultLanguage->defaultMailProposal($mail, $propertyId);
            $this->getDoctrine()->getManager()->flush();
            $this->container->get('session')->set('active-tab-preference', 'mail_template');

            return $this->redirectToRoute('preferences');
        }

        return $this->render('mail_template/edit_proposal.html.twig', [
            'mail_templates' => $mail,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit/pending", name="mail_template_edit_pending", methods={"GET","POST"})
     */
    public function editPending(Request $request, TemplateMail $mail, DefaultLanguage $defaultLanguage): Response
    {
        $this->denyAccessUnlessGranted('edit', $mail);
        $propertyId = $this->container->get('session')->get('property_id');
        $form = $this->createForm(TemplateMailType::class, $mail);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $defaultLanguage->defaultMailPending($mail, $propertyId);
            $this->getDoctrine()->getManager()->flush();
            $this->container->get('session')->set('active-tab-preference', 'mail_template');

            return $this->redirectToRoute('preferences');
        }

        return $this->render('mail_template/edit_pending.html.twig', [
            'mail_templates' => $mail,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="mail_template_delete", methods={"DELETE"})
     */
    public function delete(Request $request, TemplateMail $mailTemplate): Response
    {
        $this->denyAccessUnlessGranted('delete', $mailTemplate);
        if ($this->isCsrfTokenValid('delete' . $mailTemplate->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($mailTemplate);
            $entityManager->flush();
            $this->container->get('session')->set('active-tab-preference', 'mail_template');
        }

        return $this->redirectToRoute('preferences');
    }
}
