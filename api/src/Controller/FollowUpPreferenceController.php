<?php

namespace App\Controller;

use App\Entity\FollowUpPreference;
use App\Form\FollowUpPreferenceType;
use App\Repository\FollowUpPreferenceRepository;
use App\Service\FollowUpPrefOrder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/preferences/follow-up")
 */
class FollowUpPreferenceController extends AbstractController
{
    private $followUpPrefOrder;

    public function __construct(FollowUpPrefOrder $followUpPrefOrder)
    {
        $this->followUpPrefOrder = $followUpPrefOrder;
    }

    /**
     * @Route("/new", name="app_follow_up_preference_new", methods={"GET", "POST"})
     */
    public function new(Request $request, FollowUpPreferenceRepository $followUpPreferenceRepository): Response
    {
        $followUpPreference = new FollowUpPreference();
        $form = $this->createForm(FollowUpPreferenceType::class, $followUpPreference);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $propertyId = $this->container->get('session')->get('property_id');
            if (!$propertyId) {
                return $this->redirectToRoute('dashboard');
            }
            $property = $this->getDoctrine()->getRepository('App:Property')->find($propertyId);
            $followUpPreference->setProperty($property);
            $order = $this->followUpPrefOrder->assignClassification($followUpPreference, $propertyId);
            if ($order === false) {
                $this->addFlash('error', 'You cannot configure 2 follow ups with the same days or with the same number.');
                return $this->redirectToRoute('app_follow_up_preference_new');
            }
            $followUpPreferenceRepository->add($followUpPreference);
            return $this->redirectToRoute('preferences');
        }

        return $this->render('follow_up_preference/new.html.twig', [
            'follow_up_preference' => $followUpPreference,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_follow_up_preference_edit", methods={"GET", "POST"})
     */
    public function edit(
        Request $request,
        FollowUpPreference $followUpPreference,
        FollowUpPreferenceRepository $followUpPreferenceRepository
    ): Response {
        $form = $this->createForm(FollowUpPreferenceType::class, $followUpPreference);
        $form->handleRequest($request);
        $propertyId = $followUpPreference->getProperty()->getId();

        if ($form->isSubmitted() && $form->isValid()) {
            $order = $this->followUpPrefOrder->assignClassification($followUpPreference, $propertyId);
            if ($order === false) {
                $this->addFlash('error', 'You cannot configure 2 follow ups with the same days or with the same number.');
                return $this->redirectToRoute('app_follow_up_preference_edit', [
                    'id' => $followUpPreference->getId(),
                ]);
            }
            $followUpPreferenceRepository->add($followUpPreference);
            return $this->redirectToRoute('preferences');
        }

        return $this->render('follow_up_preference/edit.html.twig', [
            'follow_up_preference' => $followUpPreference,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_follow_up_preference_delete", methods={"POST"})
     */
    public function delete(
        Request $request,
        FollowUpPreference $followUpPreference,
        FollowUpPreferenceRepository $followUpPreferenceRepository
    ): Response {
        if ($this->isCsrfTokenValid('delete'.$followUpPreference->getId(), $request->request->get('_token'))) {
            $followUpPreferenceRepository->remove($followUpPreference);
        }

        return $this->redirectToRoute('preferences');
    }
}
