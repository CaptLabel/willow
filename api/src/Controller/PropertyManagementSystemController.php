<?php

namespace App\Controller;

use App\Entity\PropertyManagementSystem;
use App\Form\PropertyManagementSystemType;
use App\Repository\PropertyManagementSystemRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/godboard/pms")
 */
class PropertyManagementSystemController extends AbstractController
{
    /**
     * @Route("/", name="property_management_system_index", methods={"GET"})
     */
    public function index(PropertyManagementSystemRepository $propertyManagementSystemRepository): Response
    {
        return $this->render('property_management_system/index.html.twig', [
            'property_management_systems' => $propertyManagementSystemRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="property_management_system_new", methods={"GET","POST"})
     */
    public function new(
        Request $request,
        PropertyManagementSystemRepository $propertyManagementSystemRepository
    ): Response {
        $propertyManagementSystem = new PropertyManagementSystem();
        $form = $this->createForm(PropertyManagementSystemType::class, $propertyManagementSystem);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $propertyManagementSystemRepository->add($propertyManagementSystem);
            return $this->redirectToRoute('property_management_system_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('property_management_system/new.html.twig', [
            'property_management_system' => $propertyManagementSystem,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="property_management_system_edit", methods={"GET","POST"})
     */
    public function edit(
        Request $request,
        PropertyManagementSystem $propertyManagementSystem,
        PropertyManagementSystemRepository $propertyManagementSystemRepository
    ): Response {
        $form = $this->createForm(PropertyManagementSystemType::class, $propertyManagementSystem);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $propertyManagementSystemRepository->add($propertyManagementSystem);
            return $this->redirectToRoute('property_management_system_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('property_management_system/edit.html.twig', [
            'property_management_system' => $propertyManagementSystem,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="property_management_system_delete", methods={"POST"})
     */
    public function delete(
        Request $request,
        PropertyManagementSystem $propertyManagementSystem,
        PropertyManagementSystemRepository $propertyManagementSystemRepository
    ): Response {
        if ($this->isCsrfTokenValid('delete'.$propertyManagementSystem->getId(), $request->request->get('_token'))) {
            $propertyManagementSystemRepository->remove($propertyManagementSystem);
        }

        return $this->redirectToRoute('property_management_system_index', [], Response::HTTP_SEE_OTHER);
    }
}
