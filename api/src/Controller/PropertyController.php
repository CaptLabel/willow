<?php

namespace App\Controller;

use App\Entity\Property;
use App\Form\PropertyType;
use App\Repository\PropertyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("/property")
 */
class PropertyController extends AbstractController
{
    /**
     * @Route("/switch/{id}", name="property_switch", methods={"GET"}, requirements={"id"="\d+"})
     */
    public function switch(Request $request, Property $property): Response
    {
        $this->container->get('session')->set('property_id', $property->getId());
        $this->container->get('session')->set('property_label', $property->getLabel());

        return $this->redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * @Route("/switch/reset", name="property_switch_reset", methods={"GET"})
     */
    public function reset(Request $request): Response
    {
        $this->container->get('session')->set('property_id', null);
        $this->container->get('session')->set('property_label', null);

        return $this->redirect($_SERVER['HTTP_REFERER']);
    }
}
