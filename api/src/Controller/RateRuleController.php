<?php

namespace App\Controller;

use App\Entity\RateRule;
use App\Form\DefaultRateRuleType;
use App\Form\RateRuleType;
use App\Repository\RateRuleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/rate_rule")
 */
class RateRuleController extends AbstractController
{
    /**
     * @Route("/new", name="rate_rule_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $propertyId = $this->container->get('session')->get('property_id');
        $rateRule = new RateRule();
        $formType = RateRuleType::class;
        $rateRuleReposotory = $this->getDoctrine()->getRepository('App:RateRule');
        if (!$rateRuleReposotory->findDefaultRateRuleByProperty($propertyId)) {
            $formType = DefaultRateRuleType::class;
        }
        $form = $this->createForm($formType, $rateRule);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $similarRule = $rateRuleReposotory->findSimilar($propertyId, $rateRule);
            if ($similarRule) {
                return $this->render('rate_rule/new.html.twig', [
                    'rate_rule' => $rateRule,
                    'similar_rule' => $similarRule,
                    'form' => $form->createView(),
                ]);
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($rateRule);
            $entityManager->flush();
            $this->container->get('session')->set('active-tab', 'rate_rules');

            return $this->redirectToRoute('revenue_configuration');
        }

        return $this->render('rate_rule/new.html.twig', [
            'rate_rule' => $rateRule,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/default", name="rate_rule_default", methods={"GET","POST"})
     */
    public function default(Request $request): Response
    {
        $rateRule = new RateRule();
        $form = $this->createForm(DefaultRateRuleType::class, $rateRule);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($rateRule);
            $entityManager->flush();
            $this->container->get('session')->set('active-tab', 'rate_rules');

            return $this->redirectToRoute('revenue_configuration');
        }

        return $this->render('rate_rule/new.html.twig', [
            'rate_rule' => $rateRule,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="rate_rule_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, RateRule $rateRule): Response
    {
        $this->denyAccessUnlessGranted('edit', $rateRule);
        $formType = RateRuleType::class;
        if ($rateRule->getDefaultSelection()) {
            $formType = DefaultRateRuleType::class;
        }
        $form = $this->createForm($formType, $rateRule);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->container->get('session')->set('active-tab', 'rate_rules');

            return $this->redirectToRoute('revenue_configuration');
        }

        return $this->render('rate_rule/edit.html.twig', [
            'rate_rule' => $rateRule,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="rate_rule_delete", methods={"DELETE"})
     */
    public function delete(Request $request, RateRule $rateRule): Response
    {
        $this->denyAccessUnlessGranted('delete', $rateRule);
        if ($this->isCsrfTokenValid('delete' . $rateRule->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($rateRule);
            $entityManager->flush();
            $this->container->get('session')->set('active-tab', 'rate_rules');
        }

        return $this->redirectToRoute('revenue_configuration');
    }
}
