<?php

namespace App\Controller;

use App\Entity\Extra;
use App\Form\ExtraType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/extra_type")
 */
class ExtraController extends AbstractController
{
    /**
     * @Route("/new", name="extra_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $extra = new Extra();
        $form = $this->createForm(ExtraType::class, $extra);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($extra);
            $entityManager->flush();
            $this->container->get('session')->set('active-tab', 'extra_type');

            return $this->redirectToRoute('property_configuration');
        }

        return $this->render('extra/new.html.twig', [
            'extra' => $extra,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="extra_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Extra $extra): Response
    {
        $this->denyAccessUnlessGranted('edit', $extra);
        $form = $this->createForm(ExtraType::class, $extra);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->container->get('session')->set('active-tab', 'extra_type');

            return $this->redirectToRoute('property_configuration');
        }

        return $this->render('extra/edit.html.twig', [
            'extra' => $extra,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="extra_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Extra $extra): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $property = $extra->getProperty();
        $this->denyAccessUnlessGranted('delete', $extra);
        if ($this->isCsrfTokenValid('delete' . $extra->getId(), $request->request->get('_token'))) {
            $inProperty = $entityManager->getRepository('App:Enquiry')->findExtraByProperty($property, $extra);
            $inRule = $entityManager->getRepository('App:ExtraRule')->findExtraByProperty($property, $extra);
            if ($inProperty || $inRule) {
                $this->addFlash('error', 'This extra type is used in an enquiry or a rule, you cannot delete it.');
            } else {
                $entityManager->remove($extra);
                $entityManager->flush();
            }
            $this->container->get('session')->set('active-tab', 'extra_type');
        }

        return $this->redirectToRoute('property_configuration');
    }
}
