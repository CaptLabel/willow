<?php

namespace App\Controller;

use App\Entity\Property;
use App\Entity\TemplateProposal;
use App\Form\TemplateProposalType;
use App\Service\DefaultLanguage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/template_proposal")
 */
class TemplateProposalController extends AbstractController
{
    /**
     * @Route("/new", name="template_proposal_new", methods={"GET","POST"})
     */
    public function new(Request $request, DefaultLanguage $defaultLanguage): Response
    {
        $propertyId = $this->container->get('session')->get('property_id');
        $proposalTemplate = new TemplateProposal();
        $form = $this->createForm(TemplateProposalType::class, $proposalTemplate);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $defaultLanguage->defaultProposal($proposalTemplate, $propertyId);
            $this->getDoctrine()->getManager()->persist($proposalTemplate);
            $this->getDoctrine()->getManager()->flush();
            $this->container->get('session')->set('active-tab-preference', 'template_proposal');

            return $this->redirectToRoute('preferences');
        }

        return $this->render('template_proposal/new.html.twig', [
            'template_proposal' => $proposalTemplate,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="template_proposal_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TemplateProposal $proposalTemplate, DefaultLanguage $defLanguage): Response
    {
        $this->denyAccessUnlessGranted('edit', $proposalTemplate);
        $propertyId = $this->container->get('session')->get('property_id');
        $form = $this->createForm(TemplateProposalType::class, $proposalTemplate);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $defLanguage->defaultProposal($proposalTemplate, $propertyId);
            $this->getDoctrine()->getManager()->flush();
            $this->container->get('session')->set('active-tab-preference', 'template_proposal');

            return $this->redirectToRoute('preferences');
        }

        return $this->render('template_proposal/edit.html.twig', [
            'template_proposal' => $proposalTemplate,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="template_proposal_delete", methods={"DELETE"})
     */
    public function delete(Request $request, TemplateProposal $proposalTemplate): Response
    {
        $this->denyAccessUnlessGranted('delete', $proposalTemplate);
        if ($this->isCsrfTokenValid('delete' . $proposalTemplate->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($proposalTemplate);
            $entityManager->flush();
            $this->container->get('session')->set('active-tab-preference', 'template_proposal');
        }

        return $this->redirectToRoute('preferences');
    }
}
