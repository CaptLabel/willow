<?php

namespace App\Controller;

use App\Entity\Enquiry;
use App\Form\OnlineFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OnlineFormController extends AbstractController
{
    /**
     * @Route("/new", name="online_form", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $enquiry = new Enquiry();
        $enquiryForm = $this->createForm(OnlineFormType::class, $enquiry);

        $enquiryForm->handleRequest($request);

        if ($enquiryForm->isSubmitted() && $enquiryForm->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($enquiry);
            $entityManager->flush();

            return $this->render('online_form/new.html.twig');
        }

        return $this->render('online_form/index.html.twig', [
            'enquiry' => $enquiry,
            'enquiryForm' => $enquiryForm->createView(),
        ]);
    }
}
