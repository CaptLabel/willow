<?php

namespace App\Controller;

use App\Entity\TemplateCondition;
use App\Entity\Property;
use App\Form\TemplateConditionType;
use App\Repository\ConditionsRepository;
use App\Service\DefaultLanguage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/conditions")
 */
class TemplateConditionController extends AbstractController
{
    /**
     * @Route("/new", name="conditions_new", methods={"GET","POST"})
     */
    public function new(Request $request, DefaultLanguage $defaultLanguage): Response
    {
        $propertyId = $this->container->get('session')->get('property_id');
        $conditions = new TemplateCondition();
        $form = $this->createForm(TemplateConditionType::class, $conditions);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $defaultLanguage->defaultCondition($conditions, $propertyId);
            $this->getDoctrine()->getManager()->persist($conditions);
            $this->getDoctrine()->getManager()->flush();
            $this->container->get('session')->set('active-tab-preference', 'conditions');

            return $this->redirectToRoute('preferences');
        }

        return $this->render('conditions/new.html.twig', [
            'conditions' => $conditions,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="conditions_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TemplateCondition $conditions, DefaultLanguage $defaultLanguage): Response
    {
        $this->denyAccessUnlessGranted('edit', $conditions);
        $propertyId = $this->container->get('session')->get('property_id');
        $form = $this->createForm(TemplateConditionType::class, $conditions);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $defaultLanguage->defaultCondition($conditions, $propertyId);
            $this->getDoctrine()->getManager()->flush();
            $this->container->get('session')->set('active-tab-preference', 'conditions');

            return $this->redirectToRoute('preferences');
        }

        return $this->render('conditions/edit.html.twig', [
            'conditions' => $conditions,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="conditions_delete", methods={"DELETE"})
     */
    public function delete(Request $request, TemplateCondition $conditions): Response
    {
        $this->denyAccessUnlessGranted('delete', $conditions);
        if ($this->isCsrfTokenValid('delete' . $conditions->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($conditions);
            $entityManager->flush();
            $this->container->get('session')->set('active-tab-preference', 'conditions');
        }

        return $this->redirectToRoute('preferences');
    }
}
