<?php

namespace App\Model\OnlineForm;

class NewRequestGenerator
{
    /**
     * @var int
     */
    private $numberOfPerson;

    /**
     * @var string
     */
    private $propertyCode;

    /**
     * @var string
     */
    private $checkInDate;

    /**
     * @var int
     */
    private $nights;

    /**
     * @var ?string
     */
    private $checkInFlexibility;

    /**
     * @var ?string
     */
    private $nightsFlexibility;

    /**
     * @var ?string
     */
    private $groupType;

    /**
     * @var string
     */
    private $groupName;

    /**
     * @var ?string
     */
    private $contactFirstName;

    /**
     * @var ?string
     */
    private $contactLastName;

    /**
     * @var ?string
     */
    private $phoneNumber;

    /**
     * @var string
     */
    private $emailAddress;

    /**
     * @var ?string
     */
    private $address;

    /**
     * @var ?string
     */
    private $city;

    /**
     * @var ?string
     */
    private $postalCode;

    /**
     * @var ?string
     */
    private $country;

    /**
     * @var ?string
     */
    private $age;

    /**
     * @var ?string
     */
    private $clientComment;

    /**
     * @return int
     */
    public function getNumberOfPerson(): int
    {
        return $this->numberOfPerson;
    }

    /**
     * @param int $numberOfPerson
     */
    public function setNumberOfPerson(int $numberOfPerson): void
    {
        $this->numberOfPerson = $numberOfPerson;
    }

    /**
     * @return string
     */
    public function getPropertyCode(): string
    {
        return $this->propertyCode;
    }

    /**
     * @param string $propertyCode
     */
    public function setPropertyCode(string $propertyCode): void
    {
        $this->propertyCode = $propertyCode;
    }

    /**
     * @return string
     */
    public function getCheckInDate(): string
    {
        return $this->checkInDate;
    }

    /**
     * @param string $checkInDate
     */
    public function setCheckInDate(string $checkInDate): void
    {
        $this->checkInDate = $checkInDate;
    }

    /**
     * @return int
     */
    public function getNights(): int
    {
        return $this->nights;
    }

    /**
     * @param int $nights
     */
    public function setNights(int $nights): void
    {
        $this->nights = $nights;
    }

    /**
     * @return string|null
     */
    public function getCheckInFlexibility(): ?string
    {
        return $this->checkInFlexibility;
    }

    /**
     * @param string|null $checkInFlexibility
     */
    public function setCheckInFlexibility(?string $checkInFlexibility): void
    {
        $this->checkInFlexibility = $checkInFlexibility;
    }

    /**
     * @return string|null
     */
    public function getNightsFlexibility(): ?string
    {
        return $this->nightsFlexibility;
    }

    /**
     * @param string|null $nightsFlexibility
     */
    public function setNightsFlexibility(?string $nightsFlexibility): void
    {
        $this->nightsFlexibility = $nightsFlexibility;
    }

    /**
     * @return string|null
     */
    public function getGroupType(): ?string
    {
        return $this->groupType;
    }

    /**
     * @param string|null $groupType
     */
    public function setGroupType(?string $groupType): void
    {
        $this->groupType = $groupType;
    }

    /**
     * @return string
     */
    public function getGroupName(): string
    {
        return $this->groupName;
    }

    /**
     * @param string $groupName
     */
    public function setGroupName(string $groupName): void
    {
        $this->groupName = $groupName;
    }

    /**
     * @return string|null
     */
    public function getContactFirstName(): ?string
    {
        return $this->contactFirstName;
    }

    /**
     * @param string|null $contactFirstName
     */
    public function setContactFirstName(?string $contactFirstName): void
    {
        $this->contactFirstName = $contactFirstName;
    }

    /**
     * @return string|null
     */
    public function getContactLastName(): ?string
    {
        return $this->contactLastName;
    }

    /**
     * @param string|null $contactLastName
     */
    public function setContactLastName(?string $contactLastName): void
    {
        $this->contactLastName = $contactLastName;
    }

    /**
     * @return string|null
     */
    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    /**
     * @param string|null $phoneNumber
     */
    public function setPhoneNumber(?string $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return string
     */
    public function getEmailAddress(): string
    {
        return $this->emailAddress;
    }

    /**
     * @param string $emailAddress
     */
    public function setEmailAddress(string $emailAddress): void
    {
        $this->emailAddress = $emailAddress;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     */
    public function setAddress(?string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     */
    public function setCity(?string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return string|null
     */
    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    /**
     * @param string|null $postalCode
     */
    public function setPostalCode(?string $postalCode): void
    {
        $this->postalCode = $postalCode;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     */
    public function setCountry(?string $country): void
    {
        $this->country = $country;
    }

    /**
     * @return string|null
     */
    public function getAge(): ?string
    {
        return $this->age;
    }

    /**
     * @param string|null $age
     */
    public function setAge(?string $age): void
    {
        $this->age = $age;
    }

    /**
     * @return string|null
     */
    public function getClientComment(): ?string
    {
        return $this->clientComment;
    }

    /**
     * @param string|null $clientComment
     */
    public function setClientComment(?string $clientComment): void
    {
        $this->clientComment = $clientComment;
    }
}
