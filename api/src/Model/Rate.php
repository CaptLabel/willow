<?php

namespace App\Model;

class Rate
{
    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var RateRoom[]
     */
    private $rooms;

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return RateRoom[]
     */
    public function getRooms(): array
    {
        return $this->rooms;
    }

    /**
     * @param RateRoom[] $rooms
     */
    public function setRooms(array $rooms): void
    {
        $this->rooms = $rooms;
    }
}
