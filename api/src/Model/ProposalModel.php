<?php

namespace App\Model;

class ProposalModel
{
    private string $presentationContent = '';
    private string $presentationFeature = '';
    private string $conditionContent = '';
    private string $tableRoom = '';
    private string $tableExtra = '';
    private string $totalProposal = '';
    private string $body = '';
    private string $proposalDate = '';
    private string $propertyLabel = '';
    private string $brandLabel = '';
    private ProposalAddressModel $proposalAddressModel;

    /**
     * @return string
     */
    public function getPresentationContent(): string
    {
        return $this->presentationContent;
    }

    /**
     * @param string $presentationContent
     */
    public function setPresentationContent(string $presentationContent): void
    {
        $this->presentationContent = $presentationContent;
    }

    /**
     * @return string
     */
    public function getPresentationFeature(): string
    {
        return $this->presentationFeature;
    }

    /**
     * @param string $presentationFeature
     */
    public function setPresentationFeature(string $presentationFeature): void
    {
        $this->presentationFeature = $presentationFeature;
    }

    /**
     * @return string
     */
    public function getConditionContent(): string
    {
        return $this->conditionContent;
    }

    /**
     * @param string $conditionContent
     */
    public function setConditionContent(string $conditionContent): void
    {
        $this->conditionContent = $conditionContent;
    }

    /**
     * @return string
     */
    public function getTableRoom(): string
    {
        return $this->tableRoom;
    }

    /**
     * @param string $tableRoom
     */
    public function setTableRoom(string $tableRoom): void
    {
        $this->tableRoom = $tableRoom;
    }

    /**
     * @return string
     */
    public function getTableExtra(): string
    {
        return $this->tableExtra;
    }

    /**
     * @param string $tableExtra
     */
    public function setTableExtra(string $tableExtra): void
    {
        $this->tableExtra = $tableExtra;
    }

    /**
     * @return string
     */
    public function getTotalProposal(): string
    {
        return $this->totalProposal;
    }

    /**
     * @param string $totalProposal
     */
    public function setTotalProposal(string $totalProposal): void
    {
        $this->totalProposal = $totalProposal;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody(string $body): void
    {
        $this->body = $body;
    }

    /**
     * @return string
     */
    public function getProposalDate(): string
    {
        return $this->proposalDate;
    }

    /**
     * @param string $proposalDate
     */
    public function setProposalDate(string $proposalDate): void
    {
        $this->proposalDate = $proposalDate;
    }

    /**
     * @return string
     */
    public function getPropertyLabel(): string
    {
        return $this->propertyLabel;
    }

    /**
     * @param string $propertyLabel
     */
    public function setPropertyLabel(string $propertyLabel): void
    {
        $this->propertyLabel = $propertyLabel;
    }

    /**
     * @return string
     */
    public function getBrandLabel(): string
    {
        return $this->brandLabel;
    }

    /**
     * @param string $brandLabel
     */
    public function setBrandLabel(string $brandLabel): void
    {
        $this->brandLabel = $brandLabel;
    }

    /**
     * @return ProposalAddressModel|null
     */
    public function getProposalAddressModel(): ?ProposalAddressModel
    {
        return $this->proposalAddressModel;
    }

    /**
     * @param ProposalAddressModel|null $proposalAddressModel
     */
    public function setProposalAddressModel(?ProposalAddressModel $proposalAddressModel): void
    {
        $this->proposalAddressModel = $proposalAddressModel;
    }
}