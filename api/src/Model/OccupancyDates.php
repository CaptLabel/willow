<?php

namespace App\Model;

class OccupancyDates
{
    /**
     * @var int
     */
    private $availability;

    /**
     * @var \DateTimeInterface
     */
    private $date;

    /**
     * @return ?int
     */
    public function getAvailability(): ?int
    {
        return $this->availability;
    }

    /**
     * @param int|null $availability
     */
    public function setAvailability(int $availability): void
    {
        $this->availability = $availability;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDate(): \DateTimeInterface
    {
        return $this->date;
    }

    /**
     * @param \DateTimeInterface $date
     */
    public function setDate(\DateTimeInterface $date): void
    {
        $this->date = $date;
    }
}