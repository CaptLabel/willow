<?php

namespace App\Model\Reporting;

class EnquiryReportModel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $property;

    /**
     * @var string
     */
    private $enquiryDate;

    /**
     * @var ?string
     */
    private $companyLabel;

    /**
     * @var string
     */
    private $groupName;

    /**
     * @var string
     */
    private $refNumber;

    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $checkInDate;

    /**
     * @var string
     */
    private $checkOutDate;

    /**
     * @var int
     */
    private $nights;

    /**
     * @var int
     */
    private $persons;

    /**
     * @var ?string
     */
    private $ageRange;

    /**
     * @var ?string
     */
    private $nationality;

    /**
     * @var ?float
     */
    private $totalAmount;

    /**
     * @var ?string
     */
    private $comments;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getProperty(): string
    {
        return $this->property;
    }

    /**
     * @param string $property
     */
    public function setProperty(string $property): void
    {
        $this->property = $property;
    }

    /**
     * @return string
     */
    public function getEnquiryDate(): string
    {
        return $this->enquiryDate;
    }

    /**
     * @param string $enquiryDate
     */
    public function setEnquiryDate(string $enquiryDate): void
    {
        $this->enquiryDate = $enquiryDate;
    }

    /**
     * @return string|null
     */
    public function getCompanyLabel(): ?string
    {
        return $this->companyLabel;
    }

    /**
     * @param string|null $companyLabel
     */
    public function setCompanyLabel(?string $companyLabel): void
    {
        $this->companyLabel = $companyLabel;
    }

    /**
     * @return string
     */
    public function getGroupName(): string
    {
        return $this->groupName;
    }

    /**
     * @param string $groupName
     */
    public function setGroupName(string $groupName): void
    {
        $this->groupName = $groupName;
    }

    /**
     * @return string
     */
    public function getRefNumber(): string
    {
        return $this->refNumber;
    }

    /**
     * @param string $refNumber
     */
    public function setRefNumber(string $refNumber): void
    {
        $this->refNumber = $refNumber;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getCheckInDate(): string
    {
        return $this->checkInDate;
    }

    /**
     * @param string $checkInDate
     */
    public function setCheckInDate(string $checkInDate): void
    {
        $this->checkInDate = $checkInDate;
    }

    /**
     * @return string
     */
    public function getCheckOutDate(): string
    {
        return $this->checkOutDate;
    }

    /**
     * @param string $checkOutDate
     */
    public function setCheckOutDate(string $checkOutDate): void
    {
        $this->checkOutDate = $checkOutDate;
    }

    /**
     * @return int
     */
    public function getNights(): int
    {
        return $this->nights;
    }

    /**
     * @param int $nights
     */
    public function setNights(int $nights): void
    {
        $this->nights = $nights;
    }

    /**
     * @return int
     */
    public function getPersons(): int
    {
        return $this->persons;
    }

    /**
     * @param int $persons
     */
    public function setPersons(int $persons): void
    {
        $this->persons = $persons;
    }

    /**
     * @return string|null
     */
    public function getAgeRange(): ?string
    {
        return $this->ageRange;
    }

    /**
     * @param string|null $ageRange
     */
    public function setAgeRange(?string $ageRange): void
    {
        $this->ageRange = $ageRange;
    }

    /**
     * @return string|null
     */
    public function getNationality(): ?string
    {
        return $this->nationality;
    }

    /**
     * @param string|null $nationality
     */
    public function setNationality(?string $nationality): void
    {
        $this->nationality = $nationality;
    }

    /**
     * @return float|null
     */
    public function getTotalAmount(): ?float
    {
        return $this->totalAmount;
    }

    /**
     * @param float|null $totalAmount
     */
    public function setTotalAmount(?float $totalAmount): void
    {
        $this->totalAmount = $totalAmount;
    }

    /**
     * @return string|null
     */
    public function getComments(): ?string
    {
        return $this->comments;
    }

    /**
     * @param string|null $comments
     */
    public function setComments(?string $comments): void
    {
        $this->comments = $comments;
    }
}
