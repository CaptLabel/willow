<?php

namespace App\Model\Reporting;

use App\Entity\Company;

class SalesByCompanyModel
{
    /**
     * @var Company
     */
    private $company;

    /**
     * @var string
     */
    private $companyLabel;

    /**
     * @var string
     */
    private $property;

    /**
     * @var string
     */
    private $user;

    /**
     * @var int
     */
    private $enquiryNumber;

    /**
     * @var float
     */
    private $totalAmount;

    /**
     * @return Company
     */
    public function getCompany(): Company
    {
        return $this->company;
    }

    /**
     * @param Company $company
     */
    public function setCompany(Company $company): void
    {
        $this->company = $company;
    }

    /**
     * @return string
     */
    public function getCompanyLabel(): string
    {
        return $this->companyLabel;
    }

    /**
     * @param string $companyLabel
     */
    public function setCompanyLabel(string $companyLabel): void
    {
        $this->companyLabel = $companyLabel;
    }

    /**
     * @return string
     */
    public function getProperty(): string
    {
        return $this->property;
    }

    /**
     * @param string $property
     */
    public function setProperty(string $property): void
    {
        $this->property = $property;
    }

    /**
     * @return string
     */
    public function getUser(): string
    {
        return $this->user;
    }

    /**
     * @param string $user
     */
    public function setUser(string $user): void
    {
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getEnquiryNumber(): int
    {
        return $this->enquiryNumber;
    }

    /**
     * @param int $enquiryNumber
     */
    public function setEnquiryNumber(int $enquiryNumber): void
    {
        $this->enquiryNumber = $enquiryNumber;
    }

    /**
     * @return float
     */
    public function getTotalAmount(): float
    {
        return $this->totalAmount;
    }

    /**
     * @param float $totalAmount
     */
    public function setTotalAmount(float $totalAmount): void
    {
        $this->totalAmount = $totalAmount;
    }
}
