<?php

namespace App\Model\Reporting;

class RateEnquiriesModel
{
    /**
     * @var array
     */
    private $property;

    /**
     * @var array
     */
    private $lastMonthRate;

    /**
     * @var array
     */
    private $lastYearRate;

    /**
     * @return array
     */
    public function getProperties(): array
    {
        return $this->property;
    }

    /**
     * @param array $property
     */
    public function setProperties(array $property): void
    {
        $this->property = $property;
    }

    /**
     * @return array
     */
    public function getLastMonthRate(): array
    {
        return $this->lastMonthRate;
    }

    /**
     * @param array $lastMonthRate
     */
    public function setLastMonthRate(array $lastMonthRate): void
    {
        $this->lastMonthRate = $lastMonthRate;
    }

    /**
     * @return array
     */
    public function getLastYearRate(): array
    {
        return $this->lastYearRate;
    }

    /**
     * @param array $lastYearRate
     */
    public function setLastYearRate(array $lastYearRate): void
    {
        $this->lastYearRate = $lastYearRate;
    }
}
