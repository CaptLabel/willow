<?php

namespace App\Model;

class Availability
{
    /**
     * @var string
     */
    private $date;

    /**
     * @var AvailabilityRoom[]
     */
    private $rooms;

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate(string $date): void
    {
        $this->date = $date;
    }

    /**
     * @return AvailabilityRoom[]
     */
    public function getRooms(): array
    {
        return $this->rooms;
    }

    /**
     * @param AvailabilityRoom[] $rooms
     */
    public function setRooms(array $rooms): void
    {
        $this->rooms = $rooms;
    }

    /**
     * @param AvailabilityRoom $room
     * @param string|null $key
     */
    public function addRoom(AvailabilityRoom $room, string $key = null): void
    {
        if (!is_null($key)) {
            $this->rooms[$key] = $room;
        } else {
            $this->rooms[] = $room;
        }
    }
}
