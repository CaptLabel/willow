<?php

namespace App\Model;

use App\Calculator\BreakdownRule\BreakdownRuleCalculatorInterface;

class BreakdownParameters
{
    /**
     * @var int
     */
    private $paxNumber;

    /**
     * @var int
     */
    private $biggestCapacity;

    /**
     * @var BreakdownRoom[]
     */
    private $rooms;

    /**
     * @var int
     */
    private $totalBedsAvailable;

    /**
     * @var int
     */
    private $totalPAXRounddown;

    /**
     * @var int
     */
    private $leftToAssign;

    /**
     * @var array
     */
    private $roomsOrderByCapacity;

    /**
     * @return int
     */
    public function getPaxNumber(): int
    {
        return $this->paxNumber;
    }

    /**
     * @param int $paxNumber
     */
    public function setPaxNumber(int $paxNumber): void
    {
        $this->paxNumber = $paxNumber;
    }

    /**
     * @return int
     */
    public function getBiggestCapacity(): int
    {
        return $this->biggestCapacity;
    }

    /**
     * @param int $biggestCapacity
     */
    public function setBiggestCapacity(int $biggestCapacity): void
    {
        if (is_null($this->biggestCapacity) || $this->biggestCapacity < $biggestCapacity) {
            $this->biggestCapacity = $biggestCapacity;
        }
    }

    /**
     * @return BreakdownRoom[]
     */
    public function getRooms(): array
    {
        return $this->rooms;
    }

    /**
     * @param BreakdownRoom[] $rooms
     */
    public function setRooms(array $rooms): void
    {
        $this->rooms = $rooms;
    }

    /**
     * @return int
     */
    public function getTotalBedsAvailable(): int
    {
        return $this->totalBedsAvailable;
    }

    /**
     * @param int $totalBedsAvailable
     */
    public function incrementTotalBedsAvailable(int $totalBedsAvailable): void
    {
        $this->totalBedsAvailable += $totalBedsAvailable;
    }

    /**
     * @return int
     */
    public function getTotalPAXRounddown(): int
    {
        return $this->totalPAXRounddown;
    }

    /**
     * @param int $totalPAXRounddown
     */
    public function incrementTotalPAXRounddown(int $totalPAXRounddown): void
    {
        $this->totalPAXRounddown += $totalPAXRounddown;
    }

    /**
     * @param int $number
     */
    public function decrementLeftToAssign(int $number)
    {
        $this->leftToAssign -= $number;
    }

    /**
     * @param int $number
     */
    public function incrementLeftToAssign(int $number)
    {
        $this->leftToAssign += $number;
    }

    /**
     * @return int
     */
    public function getLeftToAssign(): int
    {
        return $this->leftToAssign;
    }

    /**
     * @param int $leftToAssign
     */
    public function setLeftToAssign(int $leftToAssign): void
    {
        $this->leftToAssign = $leftToAssign;
    }

    /**
     * @return array
     */
    public function getRoomsOrderByCapacity(): array
    {
        return $this->roomsOrderByCapacity;
    }

    /**
     * @param array $roomsOrderByCapacity
     */
    public function setRoomsOrderByCapacity(array $roomsOrderByCapacity): void
    {
        $this->roomsOrderByCapacity = $roomsOrderByCapacity;
    }

    public function resetLeftToAssign()
    {
        $this->leftToAssign = $this->paxNumber;
    }

    public function applyBreakdownRule(string $room, BreakdownRuleCalculatorInterface $breakdownRule)
    {
        $this->getRooms()[$room]->incrementRoomsPicked($breakdownRule->getRoomsPickedToAdd());
        $this->getRooms()[$room]->incrementRoomsBreakdown($breakdownRule->getRoomsBreakdownToAdd());
        $this->decrementLeftToAssign($breakdownRule->getLeftToAssignDecrement());

        $this->getRooms()[$room]->decrementAvailability($breakdownRule->getRoomsToRemove());
        $this->getRooms()[$room]->decrementBedsAvailable($breakdownRule->getBedsToRemove());

        $this->paxNumber -= $breakdownRule->getPaxNumberToDecrement();
    }
}
