<?php

namespace App\Model;

class OccupancyParameter
{
    /**
     * @var OccupancyRoom[]
     */
    private $rooms;

    /**
     * @var OccupancyOverall[]
     */
    private $overallOccupancy;

    /**
     * @return OccupancyRoom[]
     */
    public function getRooms(): array
    {
        return $this->rooms;
    }

    /**
     * @param OccupancyRoom[] $rooms
     */
    public function setRooms(array $rooms): void
    {
        $this->rooms = $rooms;
    }

    /**
     * @return OccupancyOverall[]
     */
    public function getOverallOccupancy(): ?array
    {
        return $this->overallOccupancy;
    }

    /**
     * @param OccupancyOverall[] $overallOccupancy
     */
    public function setOverallOccupancy(?array $overallOccupancy): void
    {
        $this->overallOccupancy = $overallOccupancy;
    }
}
