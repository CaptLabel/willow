<?php

namespace App\Model;

class OccupancyOverall
{
    /**
     * @var \DateTimeInterface
     */
    private $date;

    /**
     * @var float
     */
    private $occupancyTotal;

    /**
     * @return \DateTimeInterface
     */
    public function getDate(): \DateTimeInterface
    {
        return $this->date;
    }

    /**
     * @param \DateTimeInterface $date
     */
    public function setDate(\DateTimeInterface $date): void
    {
        $this->date = $date;
    }

    /**
     * @return ?float
     */
    public function getOccupancyTotal(): ?float
    {
        return $this->occupancyTotal;
    }

    /**
     * @param float|null $occupancyTotal
     */
    public function setOccupancyTotal(float $occupancyTotal): void
    {
        $this->occupancyTotal = $occupancyTotal;
    }
}