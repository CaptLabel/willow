<?php

namespace App\Requestor;

use App\Entity\Company;
use App\Entity\Enquiry;
use App\Model\Availability;
use App\Model\AvailabilityRoom;
use App\Model\Rate;
use App\Model\RateRoom;
use App\Requestor\Model\Mews\MewsGetAllModel;
use App\Requestor\Model\Mews\MewsGetAvailabilityModel;
use App\Requestor\Model\Mews\MewsGetCompaniesPreModel;
use App\Requestor\Model\Mews\MewsGetPricingModel;

class MockRequestor extends AbstractRequestor
{
    /**
     * @param Enquiry $enquiry
     * @return Availability[]
     */
    public function getAvailabilities(Enquiry $enquiry): array
    {
        $servicesAvailability =  $this->getServicesAvailability();
        $roomTypes = $this->findRoomType($enquiry);
        $availabilities = [];
        foreach ($servicesAvailability->getDatesUtc() as $dateKey => $dateFormatted) {
            $availability = new Availability();
            $availability->setDate($dateFormatted);
            foreach ($servicesAvailability->getCategoryAvailabilities() as $categoryAvailability) {
                if (!array_key_exists($categoryAvailability->getCategoryId(), $roomTypes)) {
                    continue;
                }
                $availabilityRoom = new AvailabilityRoom();
                $availabilityRoom->setAvailability($categoryAvailability->getAvailabilities()[$dateKey]);
                $label = ($roomTypes[$categoryAvailability->getCategoryId()])->getLabel();
                $availabilityRoom->setLabel($label);
                $availability->addRoom($availabilityRoom, $label);
            }
            $dateFormattedKey = (new \DateTime($dateFormatted))->format('d/m/Y');
            $availabilities[$dateFormattedKey] = $availability;
        }

        return  $availabilities;
    }

    /**
     * @param Enquiry $enquiry
     * @param string $id
     * @return Rate[]
     * @throws \Exception
     */
    public function getRates(Enquiry $enquiry, $rateId): array
    {
        $princing = $this->getPricing($rateId);
        $rates = [];
        foreach ($princing->getDatesUtc() as $dateKey => $date) {
            $rate = new Rate();
            $rate->setDate(new \DateTime($date));
            $rateRooms = [];
            foreach ($princing->getCategoryPrices() as $categoryPriceModel) {
                $rateRoom = new RateRoom();
                $rateRoom->setLabel($categoryPriceModel->getCategoryId());
                $rateRoom->setRate($categoryPriceModel->getPrices()[$dateKey]);
                $rateRooms[$categoryPriceModel->getCategoryId()] = $rateRoom;
            }
            $rate->setRooms($rateRooms);
            $dateFormattedKey = (new \DateTime($date))->format('d/m/Y');
            $rates[$dateFormattedKey] = $rate;
        }

        return $rates;
    }

    private function getServicesAvailability(): MewsGetAvailabilityModel
    {
        $response = file_get_contents(__DIR__ . './mock/services/getAvailability.json');
        /** @var MewsGetAvailabilityModel $availability */
        $availability = $this->serializer->deserialize($response, MewsGetAvailabilityModel::class, 'json');

        return $availability;
    }

    private function getRessourcesAll(): MewsGetAllModel
    {
        $response = file_get_contents(__DIR__ . './mock/resources/getAll.json');
        $response = str_replace(['en-US'], ['enUS'], $response);
        /** @var MewsGetAllModel $allModel */
        $allModel = $this->serializer->deserialize($response, MewsGetAllModel::class, 'json');

        return $allModel;
    }

    private function getPricing($rateId): MewsGetPricingModel
    {
        $rate = $this->entityManager->getRepository('App:RateType')->findOneBy(['id' => $rateId]);
        $rateLabel = $rate->getLabel();
        $response = file_get_contents(__DIR__ . "./mock/rates/getPricing_{$rateLabel}.json");
        /** @var MewsGetPricingModel $pricing */
        $pricing = $this->serializer->deserialize($response, MewsGetPricingModel::class, 'json');

        return $pricing;
    }

    public function getRateList($brandToken, $propertyToken): array
    {
        return ['no need'];
    }

    public function getRoomList($brandToken, $propertyToken): array
    {
        return ['no need'];
    }

    public function getCompanyList(): array
    {
        return ['no need'];
    }

    public function createCompany(Company $company): MewsGetCompaniesPreModel
    {
        return new MewsGetCompaniesPreModel();
    }

    public function updateCompany(Company $company): MewsGetCompaniesPreModel
    {
        return new MewsGetCompaniesPreModel();
    }

    public function createCustomer($enquiry, $company)
    {
        return 'no need';
    }

    public function createReservationOptional($enquiry, $status, $optionDate)
    {
        return 'no need';
    }

    public function createReservationConfirmed($enquiry, $status)
    {
        return 'no need';
    }
}
