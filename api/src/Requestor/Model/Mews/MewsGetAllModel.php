<?php

namespace App\Requestor\Model\Mews;

class MewsGetAllModel
{
    /**
     * @var ?array
     */
    private $resources;

    /**
     * @var ?array
     */
    private $resourceCategoryAssignments;

    /**
     * @var ?array
     */
    private $resourceCategoryImageAssignments;

    /**
     * @var ?array
     */
    private $resourceFeatures;

    /**
     * @var ?array
     */
    private $resourceFeatureAssignments;

    /**
     * @var ?MewsResourceCategoriesModel[]
     */
    private $resourceCategories;

    /**
     * @return array|null
     */
    public function getResources(): ?array
    {
        return $this->resources;
    }

    /**
     * @param array|null $resources
     */
    public function setResources(?array $resources): void
    {
        $this->resources = $resources;
    }

    /**
     * @return array|null
     */
    public function getResourceCategoryAssignments(): ?array
    {
        return $this->resourceCategoryAssignments;
    }

    /**
     * @param array|null $resourceCategoryAssignments
     */
    public function setResourceCategoryAssignments(?array $resourceCategoryAssignments): void
    {
        $this->resourceCategoryAssignments = $resourceCategoryAssignments;
    }

    /**
     * @return array|null
     */
    public function getResourceCategoryImageAssignments(): ?array
    {
        return $this->resourceCategoryImageAssignments;
    }

    /**
     * @param array|null $resourceCategoryImageAssignments
     */
    public function setResourceCategoryImageAssignments(?array $resourceCategoryImageAssignments): void
    {
        $this->resourceCategoryImageAssignments = $resourceCategoryImageAssignments;
    }

    /**
     * @return array|null
     */
    public function getResourceFeatures(): ?array
    {
        return $this->resourceFeatures;
    }

    /**
     * @param array|null $resourceFeatures
     */
    public function setResourceFeatures(?array $resourceFeatures): void
    {
        $this->resourceFeatures = $resourceFeatures;
    }

    /**
     * @return array|null
     */
    public function getResourceFeatureAssignments(): ?array
    {
        return $this->resourceFeatureAssignments;
    }

    /**
     * @param array|null $resourceFeatureAssignments
     */
    public function setResourceFeatureAssignments(?array $resourceFeatureAssignments): void
    {
        $this->resourceFeatureAssignments = $resourceFeatureAssignments;
    }

    /**
     * @return MewsResourceCategoriesModel[]|null
     */
    public function getResourceCategories(): ?array
    {
        return $this->resourceCategories;
    }

    /**
     * @param MewsResourceCategoriesModel[]|null $resourceCategories
     */
    public function setResourceCategories(?array $resourceCategories): void
    {
        $this->resourceCategories = $resourceCategories;
    }
}
