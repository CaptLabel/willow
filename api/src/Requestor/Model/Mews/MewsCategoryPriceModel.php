<?php

namespace App\Requestor\Model\Mews;

class MewsCategoryPriceModel
{
    /**
     * @var string
     */
    private $categoryId;

    /**
     * @var array
     */
    private $prices;

    /**
     * @return string
     */
    public function getCategoryId(): string
    {
        return $this->categoryId;
    }

    /**
     * @param string $categoryId
     */
    public function setCategoryId(string $categoryId): void
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @return array
     */
    public function getPrices(): array
    {
        return $this->prices;
    }

    /**
     * @param array $prices
     */
    public function setPrices(array $prices): void
    {
        $this->prices = $prices;
    }
}
