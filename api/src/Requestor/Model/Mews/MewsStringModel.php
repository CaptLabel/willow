<?php

namespace App\Requestor\Model\Mews;

class MewsStringModel
{
    /**
     * @var string
     */
    private $enGB;

    /**
     * @var string
     */
    private $enUS;

    /**
     * @return string
     */
    public function getEnGB(): ?string
    {
        return $this->enGB;
    }

    /**
     * @param string $enGB
     */
    public function setEnGB(string $enGB): void
    {
        $this->enGB = $enGB;
    }

    /**
     * @return string
     */
    public function getEnUS(): ?string
    {
        return $this->enUS;
    }

    /**
     * @param string $enUS
     */
    public function setEnUS(string $enUS): void
    {
        $this->enUS = $enUS;
    }
}
