<?php

namespace App\Requestor\Model\Mews;

class MewsRatesModel
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $groupId;

    /**
     * @var string
     */
    private $serviceId;

    /**
     * @var ?string
     */
    private $baseRateId;

    /**
     * @var ?string
     */
    private $businessSegmentId;

    /**
     * @var bool
     */
    private $isActive;

    /**
     * @var bool
     */
    private $isEnabled;

    /**
     * @var bool
     */
    private $isPublic;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $name;

    /**
     * @var ?string
     */
    private $shortName;

    /**
     * @var ?array
     */
    private $externalNames;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getGroupId(): string
    {
        return $this->groupId;
    }

    /**
     * @param string $groupId
     */
    public function setGroupId(string $groupId): void
    {
        $this->groupId = $groupId;
    }

    /**
     * @return string
     */
    public function getServiceId(): string
    {
        return $this->serviceId;
    }

    /**
     * @param string $serviceId
     */
    public function setServiceId(string $serviceId): void
    {
        $this->serviceId = $serviceId;
    }

    /**
     * @return string|null
     */
    public function getBaseRateId(): ?string
    {
        return $this->baseRateId;
    }

    /**
     * @param string|null $baseRateId
     */
    public function setBaseRateId(?string $baseRateId): void
    {
        $this->baseRateId = $baseRateId;
    }

    /**
     * @return string|null
     */
    public function getBusinessSegmentId(): ?string
    {
        return $this->businessSegmentId;
    }

    /**
     * @param string|null $businessSegmentId
     */
    public function setBusinessSegmentId(?string $businessSegmentId): void
    {
        $this->businessSegmentId = $businessSegmentId;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->isEnabled;
    }

    /**
     * @param bool $isEnabled
     */
    public function setIsEnabled(bool $isEnabled): void
    {
        $this->isEnabled = $isEnabled;
    }

    /**
     * @return bool
     */
    public function isPublic(): bool
    {
        return $this->isPublic;
    }

    /**
     * @param bool $isPublic
     */
    public function setIsPublic(bool $isPublic): void
    {
        $this->isPublic = $isPublic;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getShortName(): ?string
    {
        return $this->shortName;
    }

    /**
     * @param string|null $shortName
     */
    public function setShortName(?string $shortName): void
    {
        $this->shortName = $shortName;
    }

    /**
     * @return array|null
     */
    public function getExternalNames(): ?array
    {
        return $this->externalNames;
    }

    /**
     * @param array|null $externalNames
     */
    public function setExternalNames(?array $externalNames): void
    {
        $this->externalNames = $externalNames;
    }
}
