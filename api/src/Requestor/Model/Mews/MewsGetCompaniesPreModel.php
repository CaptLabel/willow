<?php

namespace App\Requestor\Model\Mews;

class MewsGetCompaniesPreModel
{
    /**
     * @var ?MewsGetAllCompaniesModel[]
     */
    private $companies;

    /**
     * @var ?string
     */
    private $cursor;

    /**
     * @return MewsGetAllCompaniesModel[]|null
     */
    public function getCompanies(): ?array
    {
        return $this->companies;
    }

    /**
     * @param MewsGetAllCompaniesModel[]|null $companies
     */
    public function setCompanies(?array $companies): void
    {
        $this->companies = $companies;
    }

    /**
     * @return string|null
     */
    public function getCursor(): ?string
    {
        return $this->cursor;
    }

    /**
     * @param string|null $cursor
     */
    public function setCursor(?string $cursor): void
    {
        $this->cursor = $cursor;
    }
}
