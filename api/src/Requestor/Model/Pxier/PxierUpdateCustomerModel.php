<?php

namespace App\Requestor\Model\Pxier;

class PxierUpdateCustomerModel
{
    /**
     * @var bool
     */
    private $error;

    /**
     * @var PxierDataUpdateCustomer[]
     */
    private $data;

    /**
     * @return bool
     */
    public function getError(): bool
    {
        return $this->error;
    }

    /**
     * @param bool $error
     */
    public function setError(bool $error): void
    {
        $this->error = $error;
    }

    /**
     * @return PxierDataUpdateCustomer[]
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param PxierDataUpdateCustomer[] $data
     */
    public function setDataCustomer(array $data): void
    {
        $this->data = $data;
    }
}
