<?php

namespace App\Decorator;

use App\Entity\RoomType;
use App\Exception\RoomTypeNotFoundException;
use App\Model\Availability;

class BookingsDecorator
{
    /**
     * @var Availability[]
     */
    private $bookings;

    /**
     * BookingsDecorator constructor.
     * @param Availability[] $bookings
     */
    public function __construct(
        array $bookings
    ) {
        $this->bookings = $bookings;
    }

    /**
     * @param RoomType[] $roomTypes
     * @return Availability[]
     */
    public function decorateWithRoomTypes(array $roomTypes): array
    {
        return array_map(function ($booking) use ($roomTypes) {
            $ter = array_map(function ($room) use ($roomTypes) {
                $category = $room->getLabel();
                if (array_key_exists($category, $roomTypes)) {
                    $room->setCapacity($roomTypes[$category]->getCapacity());
                    $room->setGroupOffers($roomTypes[$category]->getGroupOffers());
                    $room->setPriority($roomTypes[$category]->getPriority());
                }

                return $room;
            }, $booking->getRooms());
            $booking->setRooms($ter);
            return $booking;
        }, $this->bookings);
    }
}
