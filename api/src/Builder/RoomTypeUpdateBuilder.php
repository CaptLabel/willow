<?php

namespace App\Builder;

use App\Entity\Property;
use App\Entity\RoomType;
use App\Factory\RequestorFactory;
use Doctrine\Persistence\ManagerRegistry;

class RoomTypeUpdateBuilder
{
    /**
     * @var RequestorFactory
     */
    private $requestorFactory;

    private $doctrine;

    public function __construct(
        RequestorFactory $requestorFactory,
        ManagerRegistry $doctrine
    ) {
        $this->requestorFactory = $requestorFactory;
        $this->doctrine = $doctrine;
    }

    public function updateRoom(Property $property, $roomType)
    {
        $brand = $property->getBrand();
        $requestor = $this->requestorFactory->create($brand);
        $brandToken = $brand->getApiKeyGetData();
        $propertyToken = $property->getApiKey();
        $roomsProvider = $requestor->getRoomList($brandToken, $propertyToken);
        foreach ($roomType as $roomWillow) {
            if (array_key_exists($roomWillow->getProviderId(), $roomsProvider)) {
                $roomWillow->setLabel($roomsProvider[$roomWillow->getProviderId()]->getName());
                $roomWillow->setCapacity($roomsProvider[$roomWillow->getProviderId()]->getCapacity());
                // TODO ** add quantity via API after Mews explanation **
                $roomWillow->setQuantity(20);
            } else {
                $roomWillow->setActivated(false);
                $roomWillow->setGroupOffers(false);
                $roomWillow->setPriority(null);
            }
        }

        $roomTypeWillowId = array_map(function ($r) {
            return $r->getProviderId();
        }, $roomType);

        foreach ($roomsProvider as $rP) {
            if (!in_array($rP->getId(), $roomTypeWillowId)) {
                $roomTypeNew = new RoomType();
                $roomTypeNew->setLabel($rP->getName());
                $roomTypeNew->setActivated(true);
                $roomTypeNew->setProperty($property);
                $roomTypeNew->setProviderId($rP->getId());
                $roomTypeNew->setCapacity($rP->getCapacity());
                // TODO ** add quantity via API after Mews explanation **
                $roomTypeNew->setQuantity(20);
                $this->doctrine->getManager()->persist($roomTypeNew);
            }
        }
        $this->doctrine->getManager()->flush();
    }
}
