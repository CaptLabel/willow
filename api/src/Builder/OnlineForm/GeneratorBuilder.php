<?php

namespace App\Builder\OnlineForm;

use App\Entity\Enquiry;
use App\Model\OnlineForm\NewRequestGenerator;
use App\Service\NewEnquirySetUp;
use DateInterval;
use Doctrine\Persistence\ManagerRegistry;

class GeneratorBuilder
{
    private $entityManager;

    private $onlineEnquirySetUp;

    public function __construct(ManagerRegistry $doctrine, NewEnquirySetUp $onlineEnquirySetUp)
    {
        $this->entityManager = $doctrine->getManager();
        $this->onlineEnquirySetUp = $onlineEnquirySetUp;
    }

    public function newEnquiry($form)
    {
        $enquiry = new Enquiry();
        /** @var NewRequestGenerator $form */
        $property = $this->entityManager->getRepository('App:Property')->findByCode($form->getPropertyCode());
        if (!$property) {
            throw new \Exception('Property not found');
        }
        $enquiry->setProperty($property);
        $age = $this->entityManager->getRepository('App:Age')->findByCode($form->getAge());
        if ($age) {
            $enquiry->setAge($age);
        }
        $enquiry->setWishes(
            "Number of Persons: " . (strval($form->getNumberOfPerson())) .
            "<br/>Check In: " . $form->getCheckInDate() .
            "<br/>Nights: " . (strval($form->getNights())) .
            "<br/>Check In Flexibility: " . $form->getCheckInFlexibility() .
            "<br/>Nights Flexibility: " . $form->getNightsFlexibility() .
            "<br/>Group Type: " . $form->getGroupType() .
            "<br/>Group Name: " . $form->getGroupName() .
            "<br/>Contact First Name: " . $form->getContactFirstName() .
            "<br/>Contact Last Name: " . $form->getContactLastName() .
            "<br/>Phone Number: " . $form->getPhoneNumber() .
            "<br/>Email Address: " . $form->getEmailAddress() .
            "<br/>Address: " . $form->getAddress() .
            "<br/>City: " . $form->getCity() .
            "<br/>Postal Code: " . $form->getPostalCode() .
            "<br/>Country: " . $form->getCountry() .
            "<br/>Age: " . $form->getAge() .
            "<br/>Client Comment: " . $form->getClientComment()
        );
        $enquiry->setNumberOfPersons($form->getNumberOfPerson());
        $checkIn = new \DateTimeImmutable($form->getCheckInDate());
        $enquiry->setCheckInDate($checkIn);
        $checkOut = $checkIn->add(new \DateInterval('P' . $form->getNights() . 'D'));
        $enquiry->setCheckOutDate($checkOut);
        $enquiry->setGroupName($form->getGroupName());
        $enquiry->setContactFirstName($form->getContactFirstName());
        $enquiry->setContactLastName($form->getContactLastName());
        $enquiry->setPhoneNumber($form->getPhoneNumber());
        $enquiry->setEmailAddress($form->getEmailAddress());
        $country = $this->entityManager->getRepository('App:Country')->findOneBy(['label' => $form->getCountry()]);
        if ($country) {
            $enquiry->setCountry($country);
        }
        $enquiry->setClientComment($form->getClientComment());
        $resNumber = $this->onlineEnquirySetUp->createOnline($enquiry);

        return $resNumber;
    }
}
