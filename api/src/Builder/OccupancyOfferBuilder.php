<?php

namespace App\Builder;

use App\Factory\RequestorFactory;
use App\Formater\ResultFormater;
use App\Model\OccupancyDates;
use App\Model\OccupancyOverall;
use App\Model\OccupancyParameter;
use App\Model\OccupancyRoom;
use DateInterval;
use Doctrine\ORM\EntityManagerInterface;

class OccupancyOfferBuilder
{
    /**
     * @var RequestorFactory
     */
    private $requestorFactory;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ResultFormater
     */
    private $resultFormater;

    public function __construct(
        EntityManagerInterface $entityManager,
        ResultFormater $resultFormater,
        RequestorFactory $requestorFactory
    ) {
        $this->requestorFactory = $requestorFactory;
        $this->entityManager = $entityManager;
        $this->resultFormater = $resultFormater;
    }

    public function getOccupancy($enquiry)
    {
        $brand = $enquiry->getProperty()->getBrand();
        $requestor = $this->requestorFactory->create($brand);
        $availabilities = $requestor->getAvailabilities($enquiry);
        $this->initParameters();
        $this->findRoomType($enquiry);
        $this->initRooms($this->roomTypes);
        $this->initAvailability($enquiry, $availabilities);
        $this->setGeneralOccupancy($enquiry, $availabilities);
        $this->setLowestAvailability($enquiry, $availabilities);
        return $this->parameters;
    }

    public function getRoomsOverbooked($enquiry)
    {
        $occupancyTable = $this->getOccupancy($enquiry);
        if (!$enquiry->getOffer()) {
            $overbookedRooms = [];
        } else {
            $roomsBooked = $enquiry->getOffer()->getRooms();
            $overbookedRooms = [];
            foreach ($roomsBooked as $room) {
                $label = $room->getRoomType()->getLabel();
                if (array_key_exists($label, $occupancyTable)) {
                    $ava = $room->getRoomQuantity() - $occupancyTable[$label]->getLowestAvailability();
                    if ($ava > 0) {
                        $overbookedRooms[$room->getRoomType()->getLabel()] = $ava;
                    }
                }
            }
        }
        return $overbookedRooms;
    }

    private function setLowestAvailability($enquiry, $availabilities)
    {
        $period = new \DatePeriod(
            $enquiry->getCheckInDate(),
            new \DateInterval('P1D'),
            $enquiry->getCheckOutDate()
        );
        foreach ($period as $date) {
            foreach ($this->parameters->getRooms() as $room) {
                if (!array_key_exists($date->format('d/m/Y'), $availabilities)) {
                    continue;
                }
                if (
                    !array_key_exists(
                        $room->getRoomType()->getLabel(),
                        $availabilities[$date->format('d/m/Y')]->getRooms()
                    )
                ) {
                    $this->removeUndefinedRoom($room);
                    continue;
                }
                $label = $room->getRoomType()->getLabel();
                $roomAvailable = $availabilities[$date->format('d/m/Y')]->getRooms()[$label];
                $room->setLowestAvailability($roomAvailable->getAvailability());
            }
        }
    }

    public function findRoomType($enquiry): void
    {
        $roomType = $this->entityManager->getRepository('App:RoomType')->findRoomsForOccupancy($enquiry);
        $this->roomTypes = $this->resultFormater->changeKeyByName($roomType);
    }

    private function initRooms($roomTypes): void
    {
        $rooms = [];
        foreach ($roomTypes as $roomType) {
            $room = new OccupancyRoom();
            $room->setRoomType($roomType);
            $roomsCapacity[$roomType->getLabel()] = $roomType->getCapacity();
            $rooms[$roomType->getLabel()] = $room;
        }
        $this->parameters->setRooms($rooms);
    }

    private function initParameters(): void
    {
        $parameters = new OccupancyParameter();

        $this->parameters = $parameters;
    }

    private function initAvailability($enquiry, $availabilities)
    {
        $checkIn = new \DateTime($enquiry->getCheckInDate()->format('Y-m-d'));
        $checkOut = new \DateTime($enquiry->getCheckOutDate()->format('Y-m-d'));
        $start = $checkIn->sub(new DateInterval('P14D'));
        $end = $checkOut->add(new DateInterval('P14D'));
        $period = new \DatePeriod($start, new \DateInterval('P1D'), $end);
        foreach ($this->parameters->getRooms() as $room) {
            $occupancyDates = [];
            foreach ($period as $date) {
                if (!array_key_exists($date->format('d/m/Y'), $availabilities)) {
                    continue;
                }
                if (
                    !array_key_exists(
                        $room->getRoomType()->getLabel(),
                        $availabilities[$date->format('d/m/Y')]->getRooms()
                    )
                ) {
                    $this->removeUndefinedRoom($room);
                    continue;
                }

                $occupancyDay = new OccupancyDates();
                $occupancyDay->setDate($date);
                $label = $room->getRoomType()->getLabel();
                $roomAvailable = $availabilities[$date->format('d/m/Y')]->getRooms()[$label];
                $occupancyDay->setAvailability($roomAvailable->getAvailability());
                $occupancyDates[] = $occupancyDay;
            }
            $room->setOccupancyDates($occupancyDates);
        }
    }

    private function setGeneralOccupancy($enquiry, $availabilities)
    {
        $checkIn = new \DateTime($enquiry->getCheckInDate()->format('Y-m-d'));
        $checkOut = new \DateTime($enquiry->getCheckOutDate()->format('Y-m-d'));
        $start = $checkIn->sub(new DateInterval('P14D'));
        $end = $checkOut->add(new DateInterval('P14D'));
        $periods = new \DatePeriod($start, new \DateInterval('P1D'), $end);
        $generalOccupancy = [];
        foreach ($periods as $period) {
            if (!array_key_exists($period->format('d/m/Y'), $availabilities)) {
                continue;
            }
            $beds = 0;
            $capacity = 0;
            foreach ($this->parameters->getRooms() as $room) {
                foreach ($room->getOccupancyDates() as $occupancyDate) {
                    if ($occupancyDate->getDate()->format('d/m/Y') === $period->format('d/m/Y')) {
                        $beds += $occupancyDate->getAvailability() * $room->getRoomType()->getCapacity();
                        $capacity += $room->getRoomType()->getCapacity() * $room->getRoomType()->getQuantity();
                    }
                }
            }
            $result = (1 - ($beds / $capacity)) * 100;
            $occupancy = $result;
            $overallOccupancy = new OccupancyOverall();
            $overallOccupancy->setDate($period);
            $overallOccupancy->setOccupancyTotal($occupancy);
            $generalOccupancy[] = $overallOccupancy;
        }

        $this->parameters->setOverallOccupancy($generalOccupancy);
    }

    private function removeUndefinedRoom(OccupancyRoom $room): void
    {
        $arrayOld = $this->parameters->getRooms();
        unset($arrayOld[$room->getRoomType()->getLabel()]);
        $this->parameters->setRooms($arrayOld);
    }
}