<?php

namespace App\Builder;

use App\Model\ProposalModel;

class ProposalTemplateBuilder
{
    private array $variables = [];

    public function create(ProposalModel $proposal): string
    {
        $body = $proposal->getBody();
        $this->map($proposal);

        return $this->replaceVariables($body);
    }

    private function map(ProposalModel $proposal): void
    {
        $this->addVariable('{presentation_content}', $proposal->getPresentationContent());
        $this->addVariable('{presentation_feature}', $proposal->getPresentationFeature());
        $this->addVariable('{condition_content}', $proposal->getConditionContent());
        $this->addVariable('{table_room}', $proposal->getTableRoom());
        $this->addVariable('{table_extra}', $proposal->getTableExtra());
        $this->addVariable('{total_proposal}', $proposal->getTotalProposal());
        $this->addVariable('{brand_label}', $proposal->getBrandLabel());
        $this->addVariable('{property_label}', $proposal->getPropertyLabel());
        $this->addVariable('{proposal_date}', $proposal->getProposalDate());
        $this->addVariable('{proposal_address_line_1}', $proposal->getProposalAddressModel()->getAddressLine1());
        $this->addVariable('{proposal_address_line_2}', $proposal->getProposalAddressModel()->getAddressLine2());
        $this->addVariable('{proposal_city}', $proposal->getProposalAddressModel()->getCity());
        $this->addVariable('{proposal_phone_number}', $proposal->getProposalAddressModel()->getPhoneNumber());
        $this->addVariable('{proposal_country_label}', $proposal->getProposalAddressModel()->getCountryLabel());
        $this->addVariable('{proposal_postal_code}', $proposal->getProposalAddressModel()->getPostalCode());
        $this->addVariable('{proposal_state}', $proposal->getProposalAddressModel()->getState());
    }

    private function addVariable(string $key, string $value): void
    {
        $this->variables[$key] = $value;
    }

    private function replaceVariables(string $content): string
    {
        return str_replace(
            array_keys($this->variables),
            array_values($this->variables),
            $content
        );
    }
}