<?php

namespace App\Builder;

use App\Entity\Enquiry;
use App\Model\ProposalAddressModel;

class ProposalAddressBuilder
{
    public function create(Enquiry $enquiry): ProposalAddressModel
    {
        $model = new ProposalAddressModel();
        if ($line1 = $enquiry->getProperty()->getAddress()->getAddressLine1()) {
            $model->setAddressLine1($line1);
        }
        if ($line2 = $enquiry->getProperty()->getAddress()->getAddressLine2()) {
            $model->setAddressLine2($line2);
        }
        if ($city = $enquiry->getProperty()->getAddress()->getCity()) {
            $model->setCity($city);
        }
        if ($country = $enquiry->getProperty()->getAddress()->getCountry()->getLabel()) {
            $model->setCountryLabel($country);
        }
        if ($phone = $enquiry->getProperty()->getAddress()->getPhoneNumber()) {
            $model->setPhoneNumber($phone);
        }
        if ($postalCode = $enquiry->getProperty()->getAddress()->getPostalCode()) {
            $model->setPostalCode($postalCode);
        }
        if ($state = $enquiry->getProperty()->getAddress()->getState()) {
            $model->setState($state);
        }

        return $model;
    }
}