<?php

namespace App\Builder;

use App\Entity\Enquiry;
use App\Factory\ProposalTemplateFactory;
use App\Model\ProposalModel;

class ProposalBuilder
{
    private ProposalTemplateFactory $proposalTemplateFactory;
    private ProposalAddressBuilder $proposalAddressBuilder;

    public function __construct(
        ProposalTemplateFactory $proposalTemplateFactory,
        ProposalAddressBuilder $proposalAddressBuilder
    ) {
        $this->proposalTemplateFactory = $proposalTemplateFactory;
        $this->proposalAddressBuilder = $proposalAddressBuilder;
    }

    public function create(Enquiry $enquiry): ProposalModel
    {
        $model = new ProposalModel();
        $body = $enquiry->getTemplateProposal()->getContent();
        $model->setBody($body);
        if ($condition = $enquiry->getTemplateCondition()) {
            $model->setConditionContent($condition->getContent());
        }
        if ($presentationContent = $enquiry->getTemplateProposal()->getTemplatePresentation()) {
            $model->setPresentationContent($presentationContent->getContent());
        }
        if ($presentationFeature = $enquiry->getTemplateProposal()->getTemplatePresentation()) {
            $model->setPresentationFeature($presentationFeature->getFeatures());
        }
        $offer = $enquiry->getOffer();
        if (null !== $offer) {
            $tableRoom = $this->proposalTemplateFactory->make('table_room')
                ->getHtml($offer->getRooms()->toArray());
            $model->setTableRoom($tableRoom);
            $tableExtra = $this->proposalTemplateFactory->make('table_extra')
                ->getHtml($offer->getOfferExtras()->toArray());
            $model->setTableExtra($tableExtra);
            $totalProposal = $this->proposalTemplateFactory->make('total_proposal')
                ->getHtml($offer);
            $model->setTotalProposal($totalProposal);
        }
        $model->setProposalDate(
            $enquiry->getEnquiryDate()->format('d/m/Y')
        );
        $model->setPropertyLabel($enquiry->getProperty()->getLabel());
        $model->setBrandLabel($enquiry->getProperty()->getBrand()->getLabel());
        $addressModel = $this->proposalAddressBuilder->create($enquiry);
        $model->setProposalAddressModel($addressModel);

        return $model;
    }
}