<?php

namespace App\Builder;

use App\Calculator\Breakdown\BreakdownCalculator;
use App\Calculator\Extra\ExtraCalculator;
use App\Calculator\Rate\RateCalculator;
use App\Entity\Enquiry;
use App\Entity\Offer;
use App\Entity\OfferRoom;
use App\Factory\RequestorFactory;
use App\Model\BreakdownRoom;
use App\Model\Rate;
use App\Service\HistoryManager;
use Doctrine\ORM\EntityManagerInterface;

class OfferBuilder
{
    /**
     * @var Offer
     */
    private $offer;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ExtraCalculator
     */
    private $extraCalculator;

    /**
     * @var BreakdownCalculator
     */
    private $breakdownCalculator;

    /**
     * @var RateCalculator
     */
    private $rateCalculator;

    /**
     * @var RequestorFactory
     */
    private $requestorFactory;

    /**
     * @var OccupancyBuilder
     */
    private $occupancyBuilder;

    private $historyManager;

    public function __construct(
        EntityManagerInterface $entityManager,
        ExtraCalculator $extraCalculator,
        BreakdownCalculator $breakdownCalculator,
        RateCalculator $rateCalculator,
        RequestorFactory $requestorFactory,
        OccupancyBuilder $occupancyBuilder,
        HistoryManager $historyManager
    ) {
        $this->offer = new Offer();
        $this->entityManager = $entityManager;
        $this->extraCalculator = $extraCalculator;
        $this->breakdownCalculator = $breakdownCalculator;
        $this->rateCalculator = $rateCalculator;
        $this->requestorFactory = $requestorFactory;
        $this->occupancyBuilder = $occupancyBuilder;
        $this->historyManager = $historyManager;
    }

    /**
     * @param Enquiry $enquiry
     * @return void
     * @throws \Exception
     */
    public function build(Enquiry $enquiry): void
    {
        $property = $enquiry->getProperty();
        $brand = $property->getBrand();
        $requestor = $this->requestorFactory->create($brand);
        $availabilities = $requestor->getAvailabilities($enquiry);
        $this->breakdownCalculator->setEnquiry($enquiry);
        $this->breakdownCalculator->setAvailabilities($availabilities);
        $this->breakdownCalculator->calculate();
        $occupancy = $this->breakdownCalculator->getOccupancy();
        $breakdownRooms = $this->breakdownCalculator->result();
        $rates = $this->rateCalculator->calculate($enquiry, $breakdownRooms, $occupancy);
        $this->offer->setEnquiry($enquiry);
        $this->offer->setOccupancy($this->occupancyBuilder->findOccupancy($enquiry));
        if (!empty($this->breakdownCalculator->errors())) {
            $this->offer->setErrors($this->breakdownCalculator->errors());
        }
        foreach ($this->createOfferRoom($enquiry, $rates, $breakdownRooms) as $offerRoom) {
            $this->offer->addRoom($offerRoom);
            $newOfferUpdate = $this->historyManager->newOfferRoomUpdate($offerRoom);
            $offerRoom->addOfferRoomUpdate($newOfferUpdate);
        }
        $this->extraCalculator->calculate($enquiry, $breakdownRooms, $occupancy);
        foreach ($this->extraCalculator->result() as $offerExtra) {
            $this->offer->addOfferExtra($offerExtra);
            $newExtraUpdate = $this->historyManager->newOfferExtraUpdate($offerExtra);
            $offerExtra->addOfferExtraUpdate($newExtraUpdate);
        }
    }

    /**
     * @param Enquiry $enquiry
     * @return void
     * @throws \Exception
     */
    public function buildWithBreakdown(Enquiry $enquiry): void
    {
        $property = $enquiry->getProperty();
        $brand = $property->getBrand();
        $requestor = $this->requestorFactory->create($brand);
        $availabilities = $requestor->getAvailabilities($enquiry);
        $this->breakdownCalculator->setEnquiry($enquiry);
        $this->breakdownCalculator->setAvailabilities($availabilities);
        $this->breakdownCalculator->calculateWithBreakdown();
        $occupancy = $this->breakdownCalculator->getOccupancy();
        $breakdownRooms = $this->breakdownCalculator->result();
        $rates = $this->rateCalculator->calculate($enquiry, $breakdownRooms, $occupancy);
        $this->offer->setEnquiry($enquiry);
        $this->offer->setOccupancy($this->occupancyBuilder->findOccupancy($enquiry));
        if (!empty($this->breakdownCalculator->errors())) {
            $this->offer->setErrors($this->breakdownCalculator->errors());
        }
        foreach ($this->createOfferRoom($enquiry, $rates, $breakdownRooms) as $offerRoom) {
            $this->offer->addRoom($offerRoom);
            $newOfferUpdate = $this->historyManager->newOfferRoomUpdate($offerRoom);
            $offerRoom->addOfferRoomUpdate($newOfferUpdate);
        }
        $this->extraCalculator->calculate($enquiry, $breakdownRooms, $occupancy);
        foreach ($this->extraCalculator->result() as $offerExtra) {
            $this->offer->addOfferExtra($offerExtra);
            $newExtraUpdate = $this->historyManager->newOfferExtraUpdate($offerExtra);
            $offerExtra->addOfferExtraUpdate($newExtraUpdate);
        }
    }

    /**
     * @return void
     */
    public function save(): void
    {
        $this->entityManager->persist($this->offer);
        $this->entityManager->flush();
    }

    /**
     * @return Offer
     */
    public function get(): Offer
    {
        return $this->offer;
    }

    /**
     * @param Enquiry $enquiry
     * @param Rate[] $rates
     * @param BreakdownRoom[] $breakdownRooms
     * @return array
     */
    private function createOfferRoom(Enquiry $enquiry, array $rates, array $breakdownRooms): array
    {
        return array_map(function ($roomType, $rate) use ($enquiry, $breakdownRooms) {
            if (!array_key_exists($roomType, $breakdownRooms)) {
                throw new \Exception("weird $roomType does not exist in breakdown");
            }
            $breakdownRoom = $breakdownRooms[$roomType];
            $offerRoom = new OfferRoom();
            $offerRoom->setRoomQuantity($breakdownRoom->getRoomsPicked());
            $offerRoom->setRoomType($breakdownRoom->getRoomType());
            $offerRoom->setCheckInDate($enquiry->getCheckInDate());
            $offerRoom->setCheckOutDate($enquiry->getCheckOutDate());
            $nbPersons = ($breakdownRoom->getRoomType()->getCapacity() * $breakdownRoom->getRoomsPicked())
                - $breakdownRoom->getEmptyBeds();
            $offerRoom->setPersonsInTheRoom($nbPersons);
            foreach ($rate as $r) {
                $offerRoom->addRate($r);
                $prices[] = $r->getPrice();
            }
            $sum = array_sum($prices);
            $days = count($offerRoom->getRates());
            $average = round($sum / $days, 2);
            $offerRoom->setAverageRate($average);

            return $offerRoom;
        }, array_keys($rates), $rates);
    }
}
