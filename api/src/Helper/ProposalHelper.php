<?php

namespace App\Helper;

use App\Entity\TemplateCondition;
use App\Entity\Enquiry;
use App\Entity\TemplateProposal;
use Doctrine\ORM\EntityManagerInterface;

class ProposalHelper
{
    private EntityManagerInterface $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    )
    {
        $this->entityManager = $entityManager;
    }

    public function hydrateDefaultConditionAndTemplate(Enquiry $enquiry): Enquiry
    {
        if (null === $enquiry->getTemplateCondition()) {
            $default = $this->entityManager->getRepository(TemplateCondition::class)->findOneBy([
               'property' => $enquiry->getProperty()->getId(),
               'defaultSelection' => 1
            ]);
            $enquiry->setTemplateCondition($default);
        }

        if (null === $enquiry->getTemplateProposal()) {
            $default = $this->entityManager->getRepository(TemplateProposal::class)->findOneBy([
                'property' => $enquiry->getProperty()->getId(),
                'defaultSelection' => 1
            ]);
            $enquiry->setTemplateProposal($default);
        }

        return $enquiry;
    }

    public function hydrate()
    {
//        $presentation = $enquiry->getProposalTemplate()->getPresentation();
//        $condition = $enquiry->getConditions();
//        $offer = $enquiry->getOffer();
//        $tableRoom = "";
//        if (null !== $offer){
//            $tableRoom = $this->proposalTemplateFactory->make('table_room')->getHtml($offer->getRooms()->toArray());
//        }
    }
}