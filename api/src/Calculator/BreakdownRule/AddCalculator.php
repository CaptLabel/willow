<?php

namespace App\Calculator\BreakdownRule;

use App\Entity\BreakdownRuleAdd;

class AddCalculator extends AbstractBreakdownRuleCalculator
{
    public function __construct(BreakdownRuleAdd $entity)
    {
        $this->entityQuantity = $entity->getQuantity() ?? 0;
        $this->entityCapacity = $entity->getRoomToAdd()->getCapacity();
        $this->entityRatio = $entity->getRatio() ?? 0;
    }

    public function calculate(): void
    {
        $quantityFromEntity = $this->getEntityQuantity();
        $quantityFromRatio = 0;
        if (0 < $this->getEntityRatio()) {
            $quantityFromRatio = $this->getPaxNumber() / $this->getEntityRatio();
        }
        $quantity = floor(max($quantityFromRatio, $quantityFromEntity));
        if ($this->getRoomAvailability() < $quantity) {
            $quantity = $this->getRoomAvailability();
        }
        $numberOfBeds = $this->getEntityCapacity() * $quantity;

        $this->roomsPickedToAdd = $quantity;
        $this->roomsBreakdownToAdd = $quantity;
        $this->leftToAssignDecrement = $numberOfBeds;

        $this->roomsToRemove = $quantity;
        $this->bedsToRemove = $numberOfBeds;

        $this->paxNumberToDecrement = $numberOfBeds;
    }
}
