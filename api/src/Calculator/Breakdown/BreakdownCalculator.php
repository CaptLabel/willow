<?php

namespace App\Calculator\Breakdown;

use App\Calculator\BreakdownRule\BreakdownRuleCalculator;
use App\Entity\Enquiry;
use App\Entity\OfferRoom;
use App\Entity\RoomType;
use App\Formater\ResultFormater;
use App\Model\Availability;
use App\Model\BreakdownParameters;
use App\Model\BreakdownRoom;
use Doctrine\ORM\EntityManagerInterface;

class BreakdownCalculator
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var Enquiry
     */
    private $enquiry;

    /**
     * @var Availability[]
     */
    private $availabilities;

    /**
     * @var RoomType[]
     */
    private $roomTypes;

    /**
     * @var OfferRoom[]
     */
    private $result;

    /**
     * @var array
     */
    private $errors;

    /**
     * @var int
     */
    private $occupancy;

    /**
     * @var ResultFormater
     */
    private $resultFormater;

    /**
     * @var BreakdownParameters $parameters
     */
    private $parameters;

    /**
     * @var BreakdownRuleCalculator
     */
    private $breakdownRuleCalculator;

    public function __construct(
        EntityManagerInterface $entityManager,
        ResultFormater $resultFormater,
        BreakdownRuleCalculator $breakdownRuleCalculator
    ) {
        $this->breakdownRuleCalculator = $breakdownRuleCalculator;
        $this->entityManager = $entityManager;
        $this->resultFormater = $resultFormater;
    }

    public function calculate(): void
    {
        if (is_null($this->enquiry)) {
            throw new \Exception('Enquiry must be set');
        }
        if (is_null($this->availabilities)) {
            throw new \Exception('Availabilities must be set');
        }
        $this->findRoomType();
        $this->initParameters();
        $this->initRooms();
        $this->initAvailability();
        $this->calculateOccupancy();
        $this->breakdownRuleCalculator->setOccupancy($this->occupancy);
        $this->breakdownRuleCalculator->setEnquiry($this->enquiry);
        $this->breakdownRuleCalculator->calculate();
        $this->applyFirstSavedRule();
        $this->applyAddRules();
        $this->initTotalBedsAvailable();
        $this->addRoomRounddown();
        $this->initBiggestCapacity();
        $pLeftToAssign = $this->parameters->getPaxNumber() - $this->parameters->getLeftToAssign();
        if ($pLeftToAssign < $this->parameters->getBiggestCapacity()) {
            $this->resetRoomRounddown();
        }
        if (null === current($this->roomTypes)->getPriority()) {
            $this->createPriorityForRooms();
        }
        $this->addRoomPriority();
        if ($this->parameters->getLeftToAssign() > 0) {
            $this->addRoomTooBig();
        }
        $this->emptyBedsBreakdown();
        $this->result = $this->transformedBreakdown();
    }

    public function findRoomType(): void
    {
        $roomType = $this->entityManager->getRepository('App:RoomType')->findOrderByPriority($this->enquiry);
        $this->roomTypes = $this->resultFormater->changeKeyByName($roomType);
    }

    private function initParameters(): void
    {
        $parameters = new BreakdownParameters();
        $parameters->setPaxNumber($this->enquiry->getNumberOfPersons());
        $parameters->setLeftToAssign($this->enquiry->getNumberOfPersons());

        $this->parameters = $parameters;
    }

    private function addRoomTooBig()
    {
        $roomsOrderByCapacity = $this->removeUnavailableRooms(
            $this->parameters->getRoomsOrderByCapacity()
        );
        $smallestCapacity = current($roomsOrderByCapacity);
        $greatestCapacity = end($roomsOrderByCapacity);
        $smallestLabel = array_key_first($roomsOrderByCapacity);
        $numberMaxSmallestRoomPicked = floor($greatestCapacity / $smallestCapacity);
        $v = $this->parameters->getRooms()[$smallestLabel]->getAvailability();
        $smallestRoomAvailabilities = $v - $this->parameters->getRooms()[$smallestLabel]->getRoomsPicked();
        $max = ($numberMaxSmallestRoomPicked > $smallestRoomAvailabilities)
            ? $smallestRoomAvailabilities : $numberMaxSmallestRoomPicked;
        $resultCombination = [];
        for ($i = 3; $i <= $max; $i++) {
            $resultCombination[($i * $smallestCapacity)] = [
                $smallestLabel => $i
            ];
        }
        foreach ($roomsOrderByCapacity as $label => $capacity) {
            $resultCombination[$capacity] = [
                $label => 1
            ];
            $resultCombination[$capacity + $smallestCapacity] = [
                $label => 1,
                $smallestLabel => 1
            ];

            if ($label === $smallestLabel) {
                if ($smallestRoomAvailabilities >= 2) {
                    $resultCombination[$capacity + $smallestCapacity] = [
                        $label => 2,
                    ];
                } else {
                    unset($resultCombination[$capacity + $smallestCapacity]);
                }
            }
        }
        ksort($resultCombination);
        $solutionKey = '';
        foreach ($resultCombination as $key => $value) {
            if ($this->parameters->getLeftToAssign() - $key <= 0) {
                $solutionKey = $key;
                break;
            }
        }
        foreach ($this->parameters->getRooms() as $room) {
            if (array_key_exists($room->getRoomType()->getLabel(), $resultCombination[$solutionKey])) {
                $room->setRoomsTooBig($resultCombination[$solutionKey][$room->getRoomType()->getLabel()]);
                $room->incrementRoomsPicked($resultCombination[$solutionKey][$room->getRoomType()->getLabel()]);
                $number = $resultCombination[$solutionKey][$room->getRoomType()->getLabel()];
                $this->parameters->decrementLeftToAssign($number * $room->getRoomType()->getCapacity());
            }
        }
    }

    private function removeUnavailableRooms(array $roomsOrderByCapacity): array
    {
        foreach ($this->parameters->getRooms() as $room) {
            if ($room->getAvailability() <= $room->getRoomsPicked()) {
                unset($roomsOrderByCapacity[$room->getRoomType()->getLabel()]);
            }
        }

        return $roomsOrderByCapacity;
    }

    private function initRooms(): void
    {
        $rooms = [];
        foreach ($this->roomTypes as $roomType) {
            $room = new BreakdownRoom();
            $room->setRoomType($roomType);
            $roomsCapacity[$roomType->getLabel()] = $roomType->getCapacity();
            $rooms[$roomType->getLabel()] = $room;
        }
        $this->parameters->setRooms($rooms);
        asort($roomsCapacity);
        $this->parameters->setRoomsOrderByCapacity($roomsCapacity);
    }

    private function initAvailability(): void
    {
        $period = new \DatePeriod(
            $this->enquiry->getCheckInDate(),
            new \DateInterval('P1D'),
            $this->enquiry->getCheckOutDate()
        );
        foreach ($period as $date) {
            foreach ($this->parameters->getRooms() as $room) {
                if (
                    !array_key_exists(
                        $room->getRoomType()->getLabel(),
                        $this->availabilities[$date->format('d/m/Y')]->getRooms()
                    )
                ) {
                    $this->removeUndefinedRoom($room);
                    continue;
                }
                $label = $room->getRoomType()->getLabel();
                $roomAvailable = $this->availabilities[$date->format('d/m/Y')]->getRooms()[$label];
                $room->setAvailabilityAndBeds($roomAvailable->getAvailability());
            }
        }
    }

    private function addRoomRounddown(): void
    {
        foreach ($this->parameters->getRooms() as $room) {
            $room->calcPercentageAvailable($this->parameters->getTotalBedsAvailable());
            $room->calcPaxNumberPerPercentage($this->parameters->getPaxNumber());
            $room->calcRoomsRounddown();
            $room->calcPAXRounddown();
            $room->incrementRoomsPicked($room->getRoomsRounddown());
            $this->parameters->incrementTotalPAXRounddown($room->getPAXRounddown());
            $this->parameters->decrementLeftToAssign($room->getPAXRounddown());
        }
    }

    private function resetRoomRounddown(): void
    {
        foreach ($this->parameters->getRooms() as $room) {
            $room->resetRoomsRounddown();
            $room->resetRoomsPicked();
            //on sauve les rooms breakDownPicked
            $room->incrementRoomsPicked($room->getRoomsBreakdown());
        }
        $this->parameters->setLeftToAssign($this->parameters->getPaxNumber());
    }

    private function createPriorityForRooms(): void
    {
        $rooms = $this->parameters->getRooms();
        uasort($rooms, function ($a, $b) {
            if ($a->getPercentageAvailable() == $b->getPercentageAvailable()) {
                return 0;
            }
            return ($a->getPercentageAvailable() > $b->getPercentageAvailable()) ? -1 : 1;
        });
        $this->parameters->setRooms($rooms);
    }

    private function addRoomPriority(): void
    {
        foreach ($this->parameters->getRooms() as $room) {
            $removeIndex = $this->parameters->getRoomsOrderByCapacity();
            unset($removeIndex[$room->getRoomType()->getLabel()]);
            $getSmallestCapacityWithoutCurrentRoom = (current(array_values($removeIndex)));
            $picked = (int)floor($this->parameters->getLeftToAssign() / $room->getRoomType()->getCapacity());
            $reste = fmod($this->parameters->getLeftToAssign(), $room->getRoomType()->getCapacity());
            if ($picked === 0) {
                continue;
            }
            $calc = ($this->parameters->getLeftToAssign() - $getSmallestCapacityWithoutCurrentRoom);
            if ($reste != 0 && $reste < $getSmallestCapacityWithoutCurrentRoom) {
                $picked = (int)floor($calc / $room->getRoomType()->getCapacity());
            }
            if ($picked > $room->getAvailability() - $room->getRoomsRounddown()) {
                $picked = $room->getAvailability() - $room->getRoomsRounddown();
                $var = $this->parameters->getLeftToAssign() / $room->getRoomType()->getCapacity();
                $reste = (int)floor($var - $picked);
                if ($reste != 0 && $reste < $getSmallestCapacityWithoutCurrentRoom) {
                    $picked = (int)floor($calc / $room->getRoomType()->getCapacity());
                    if ($picked > $room->getAvailability() - $room->getRoomsRounddown()) {
                        $picked = $room->getAvailability() - $room->getRoomsRounddown();
                    }
                }
            }
            if ($picked < 0) {
                $picked = 0;
            }
            $room->setRoomsPriority($picked);
            $room->incrementRoomsPicked($picked);
            $this->parameters->decrementLeftToAssign($room->getRoomType()->getCapacity() * $picked);
        }
    }

    private function initTotalBedsAvailable(): void
    {
        foreach ($this->parameters->getRooms() as $room) {
            $this->parameters->incrementTotalBedsAvailable($room->getBedsAvailable());
        }
    }

    private function transformedBreakdown(): array
    {
        return array_filter($this->parameters->getRooms(), function ($breakdownRoom) {
            if (0 === $breakdownRoom->getRoomsPicked()) {
                return null;
            }
            return $breakdownRoom;
        });
    }

    private function emptyBedsBreakdown()
    {
        $emptyBedsToAssign = abs($this->parameters->getLeftToAssign());
        $selectedRooms = [];
        foreach ($this->parameters->getRooms() as $room) {
            if (0 === $room->getRoomsPicked()) {
                continue;
            }
            $selectedRooms[$room->getRoomType()->getLabel()] = [
                'capacity' => $room->getRoomType()->getCapacity(),
                'picked' => $room->getRoomsPicked()
            ];
        }
        uasort($selectedRooms, function ($a, $b) {
            if ($a['capacity'] == $b['capacity']) {
                return 0;
            }
            return ($a['capacity'] > $b['capacity']) ? -1 : 1;
        });
        $max = $emptyBedsToAssign;
        for ($i = 1; $i <= $max; $i++) {
            foreach ($selectedRooms as $label => $values) {
                if (0 >= $emptyBedsToAssign) {
                    break;
                }
                $sub = $values['picked'];
                if ($values['picked'] > $emptyBedsToAssign) {
                    $sub = $emptyBedsToAssign;
                }
                $emptyBedsToAssign -= $sub;

                $this->parameters->getRooms()[$label]->incrementEmptyBeds($sub);
            }
            if (0 >= $emptyBedsToAssign) {
                break;
            }
        }
    }

    private function removeUndefinedRoom(BreakdownRoom $room): void
    {
        $arrayOld = $this->parameters->getRooms();
        unset($arrayOld[$room->getRoomType()->getLabel()]);
        $this->parameters->setRooms($arrayOld);
        $arOld = $this->parameters->getRoomsOrderByCapacity();
        unset($arOld[$room->getRoomType()->getLabel()]);
        $this->parameters->setRoomsOrderByCapacity($arOld);
    }

    private function initBiggestCapacity()
    {
        foreach ($this->parameters->getRooms() as $room) {
            if (0 === $room->getAvailability()) {
                continue;
            }
            $this->parameters->setBiggestCapacity($room->getRoomType()->getCapacity());
        }
    }

    private function applyAddRules(): void
    {
        $addRules = $this->breakdownRuleCalculator->getAdds();
        foreach ($addRules as $roomLabel => $calculators) {
            foreach ($calculators as $calculator) {
                $calculator->setRoomAvailability($this->parameters->getRooms()[$roomLabel]->getAvailability());
                $calculator->setPaxNumber($this->parameters->getPaxNumber());
                $calculator->calculate();
                $this->parameters->applyBreakdownRule($roomLabel, $calculator);
            }
        }
    }

    private function applyFirstSavedRule(): void
    {
        $savedRules = $this->breakdownRuleCalculator->getSaved();
        foreach ($savedRules as $roomLabel => $calculators) {
            $calculatorsTransformed = [];
            foreach ($calculators as $calculator) {
                $calculator->setRoomAvailability($this->parameters->getRooms()[$roomLabel]->getAvailability());
                $calculator->calculate();
                $calculatorsTransformed[] = $calculator;
            }
            uasort($calculatorsTransformed, function ($a, $b) {
                if ($a->getRoomsToRemove() == $b->getRoomsToRemove()) {
                    return 0;
                }
                return ($a->getRoomsToRemove() > $b->getRoomsToRemove()) ? -1 : 1;
            });
            $this->parameters->applyBreakdownRule($roomLabel, current($calculatorsTransformed));
        }
    }

    public function setEnquiry(Enquiry $enquiry)
    {
        $this->enquiry = $enquiry;
    }

    public function setAvailabilities(array $availabilities)
    {
        $this->availabilities = $availabilities;
    }

    /**
     * @return int
     */
    public function getOccupancy(): int
    {
        return $this->occupancy;
    }

    public function calculateOccupancy(): void
    {
        $beds = 0;
        $capacity = 0;
        foreach ($this->parameters->getRooms() as $room) {
            $beds += $room->getBedsAvailable();
            $capacity += $room->getRoomType()->getCapacity() * $room->getRoomType()->getQuantity();
        }
        $result = (1 - ($beds / $capacity)) * 100;

        $this->occupancy = $result;
    }

    /**
     * @return BreakdownRoom[]
     */
    public function result(): array
    {
        return $this->result;
    }

    /**
     * @return array
     */
    public function errors(): ?array
    {
        return $this->errors;
    }
}
