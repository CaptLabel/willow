<?php

namespace App\Calculator\Rate;

use App\Entity\Enquiry;
use App\Entity\OfferRoomRate;
use App\Entity\RateRule;
use App\Factory\RequestorFactory;
use App\Model\BreakdownRoom;
use App\Model\Rate;
use Doctrine\ORM\EntityManagerInterface;

class RateCalculator
{
    /**
     * @var array
     */
    private $requestedRates = [];

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var RequestorFactory
     */
    private $requestorFactory;

    public function __construct(
        EntityManagerInterface $entityManager,
        RequestorFactory $requestorFactory
    ) {
        $this->entityManager = $entityManager;
        $this->requestorFactory = $requestorFactory;
    }


    /**
     * @param Enquiry $enquiry
     * @param BreakdownRoom[] $breakdown
     * @return Rate[]
     */
    public function calculate(Enquiry $enquiry, array $breakdown, $occupancy): array
    {
        $period = new \DatePeriod(
            $enquiry->getCheckInDate(),
            new \DateInterval('P1D'),
            $enquiry->getCheckOutDate()
        );
        $dateRates = [];
        foreach ($breakdown as $roomType => $breakdownRoom) {
            foreach ($period as $date) {
                $rateRules = $this->entityManager
                    ->getRepository('App:RateRule')
                    ->findRates($enquiry, $date, $occupancy);
                if (empty($rateRules)) {
                    throw new \Exception('rate rule not found for the date : ' . $date->format('d/m/Y'));
                }
                /** @var RateRule $rateRule */
                $rateRule = current($rateRules);
                $rateLabel = $rateRule->getRateType()->getLabel();
                $rateId = $rateRule->getRateType()->getId();
                $rates = $this->findRates($enquiry, $rateId);
                $mappedRate = $rates[$date->format('d/m/Y')];
                $roomLabel = $breakdownRoom->getRoomType()->getLabel();
                $providerId = $breakdownRoom->getRoomType()->getProviderId();
                if (!array_key_exists($providerId, $mappedRate->getRooms())) {
                    $d = $date->format('d/m/Y');
                    throw new \Exception("{$roomLabel}: for this room, the rate rule 
                    {$rateLabel} was not found for the date : {$d}");
                }
                $price = $mappedRate->getRooms()[$providerId]->getRate();
                $priceAfterRule = $this->applyRule($rateRule, $price);
                $priceAfterEmptyBeds = $this->applyEmptyBeds($priceAfterRule, $breakdownRoom, $enquiry);
                $rateModel = new OfferRoomRate();
                $rateModel->setPrice($priceAfterEmptyBeds);
                $rateModel->setDate($date);
                $dateRates[$roomType][] = $rateModel;
            }
        }
        return $dateRates;
    }

    /**
     * @param Enquiry $enquiry
     * @param string $id
     * @return Rate[]
     */
    private function findRates(Enquiry $enquiry, $rateId): array
    {
        $brand = $enquiry->getProperty()->getBrand();
        $requestor = $this->requestorFactory->create($brand);
        $rates = $requestor->getRates($enquiry, $rateId);
        $this->requestedRates[$rateId] = $rates;

        return $rates;
    }

    private function applyRule($rateRule, $price): int
    {
        $percentage = $rateRule->getBaseRateVariable();
        if ($percentage !== 0 && $percentage !== null) {
            $price = $price * $percentage / 100;
        }
        $fix = $rateRule->getBaseRateFix();
        if ($fix !== null) {
            $price = $price + $fix;
        }

        return $price;
    }

    private function applyEmptyBeds($priceAfterRule, $breakdownRoom, $enquiry): int
    {
        if (
            $enquiry->getEmptyBedsCharged() === false ||
            (!$enquiry->getEmptyBedsCharged() && $enquiry->getProperty()->getEmptyBedsCharged() === false)
        ) {
            if ($breakdownRoom->getEmptyBeds() > 0) {
                $paxTotal = $breakdownRoom->getRoomsPicked() * $breakdownRoom->getRoomType()->getCapacity();
                $paxWithEmpty = $paxTotal - $breakdownRoom->getEmptyBeds();
                $priceAfterRule = $priceAfterRule * $paxWithEmpty / $paxTotal;
            }
        }

        return $priceAfterRule;
    }
}
