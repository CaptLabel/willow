<?php

namespace App\Formater;

class BrandRepositoryFormater
{
    public function changeKeyById($result)
    {
        $formated = [];
        foreach ($result as $item) {
            $formated[$item['id']] = $item;
        }
        return $formated;
    }
}
