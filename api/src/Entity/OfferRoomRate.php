<?php

namespace App\Entity;

use App\Repository\OfferRoomRateRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OfferRoomRateRepository::class)
 */
class OfferRoomRate
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity=OfferRoom::class, inversedBy="rates")
     * @ORM\JoinColumn(nullable=false)
     */
    private $offerRoom;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getOfferRoom(): ?OfferRoom
    {
        return $this->offerRoom;
    }

    public function setOfferRoom(?OfferRoom $offerRoom): self
    {
        $this->offerRoom = $offerRoom;

        return $this;
    }
}
