<?php

namespace App\Entity;

use App\Repository\OfferExtraRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OfferExtraRepository::class)
 */
class OfferExtra
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\ManyToOne(targetEntity=Offer::class, inversedBy="offerExtras")
     * @ORM\JoinColumn(nullable=false)
     */
    private $offer;

    /**
     * @ORM\ManyToOne(targetEntity=Extra::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $extra;

    /**
     * @ORM\Column(type="datetime")
     */
    private $startDate;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="datetime")
     */
    private $endDate;

    /**
     * @ORM\OneToMany(targetEntity=OfferExtraUpdates::class, mappedBy="offerExtra", orphanRemoval=true)
     */
    private $offerExtraUpdates;

    public function __construct()
    {
        $this->offerExtraUpdates = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getOffer(): ?Offer
    {
        return $this->offer;
    }

    public function setOffer(?Offer $offer): self
    {
        $this->offer = $offer;

        return $this;
    }

    public function getExtra(): ?Extra
    {
        return $this->extra;
    }

    public function setExtra(?Extra $extra): self
    {
        $this->extra = $extra;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(?\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @return Collection<int, OfferExtraUpdates>
     */
    public function getOfferExtraUpdates(): Collection
    {
        return $this->offerExtraUpdates;
    }

    public function addOfferExtraUpdate(OfferExtraUpdates $offerExtraUpdate): self
    {
        if (!$this->offerExtraUpdates->contains($offerExtraUpdate)) {
            $this->offerExtraUpdates[] = $offerExtraUpdate;
            $offerExtraUpdate->setOfferExtra($this);
        }

        return $this;
    }

    public function removeOfferExtraUpdate(OfferExtraUpdates $offerExtraUpdate): self
    {
        if ($this->offerExtraUpdates->removeElement($offerExtraUpdate)) {
            // set the owning side to null (unless already changed)
            if ($offerExtraUpdate->getOfferExtra() === $this) {
                $offerExtraUpdate->setOfferExtra(null);
            }
        }

        return $this;
    }
}
