<?php

namespace App\Entity;

use App\Repository\OfferHistoryRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OfferHistoryRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class History
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="histories")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $oldValue;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $newValue;

    /**
     * @ORM\ManyToOne(targetEntity=Enquiry::class, inversedBy="histories")
     * @ORM\JoinColumn(nullable=false)
     */
    private $enquiry;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fieldEdit;

    public function __construct()
    {
        $this->date = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getOldValue(): ?string
    {
        return $this->oldValue;
    }

    public function setOldValue(?string $oldValue): self
    {
        $this->oldValue = $oldValue;

        return $this;
    }

    public function getNewValue(): ?string
    {
        return $this->newValue;
    }

    public function setNewValue(?string $newValue): self
    {
        $this->newValue = $newValue;

        return $this;
    }

    public function getEnquiry(): ?Enquiry
    {
        return $this->enquiry;
    }

    public function setEnquiry(?Enquiry $enquiry): self
    {
        $this->enquiry = $enquiry;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setHistoryDateValue(): void
    {
        $this->date = new \DateTime();
    }

    public function getFieldEdit(): ?string
    {
        return $this->fieldEdit;
    }

    public function setFieldEdit(string $fieldEdit): self
    {
        $this->fieldEdit = $fieldEdit;

        return $this;
    }
}
