<?php

namespace App\Entity;

use App\Repository\FollowUpRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FollowUpRepository::class)
 */
class FollowUp
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $provide_date;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\ManyToOne(targetEntity=Enquiry::class, inversedBy="followUps")
     */
    private $enquiry;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $executedDate;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $number;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="followUps")
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProvideDate(): ?\DateTimeInterface
    {
        return $this->provide_date;
    }

    public function setProvideDate(\DateTimeInterface $provide_date): self
    {
        $this->provide_date = $provide_date;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getEnquiry(): ?Enquiry
    {
        return $this->enquiry;
    }

    public function setEnquiry(?Enquiry $enquiry): self
    {
        $this->enquiry = $enquiry;

        return $this;
    }

    public function getExecutedDate(): ?\DateTimeInterface
    {
        return $this->executedDate;
    }

    public function setExecutedDate(?\DateTimeInterface $executedDate): self
    {
        $this->executedDate = $executedDate;

        return $this;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(?int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
