<?php

namespace App\Entity;

use App\Repository\ProposalTemplateRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProposalTemplateRepository::class)
 */
class TemplateProposal
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $defaultSelection;

    /**
     * @ORM\ManyToOne(targetEntity=Language::class)
     */
    private $language;

    /**
     * @ORM\ManyToOne(targetEntity=Property::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $property;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;

    /**
     * @ORM\ManyToOne(targetEntity=TemplatePresentation::class)
     */
    private $templatePresentation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getDefaultSelection(): ?bool
    {
        return $this->defaultSelection;
    }

    public function setDefaultSelection(?bool $defaultSelection): self
    {
        $this->defaultSelection = $defaultSelection;

        return $this;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getProperty(): ?Property
    {
        return $this->property;
    }

    public function setProperty(?Property $property): self
    {
        $this->property = $property;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getTemplatePresentation(): ?TemplatePresentation
    {
        return $this->templatePresentation;
    }

    public function setTemplatePresentation(?TemplatePresentation $templatePresentation): self
    {
        $this->templatePresentation = $templatePresentation;

        return $this;
    }
}
