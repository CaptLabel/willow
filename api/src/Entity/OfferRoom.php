<?php

namespace App\Entity;

use App\Repository\OfferRoomRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=OfferRoomRepository::class)
 */
class OfferRoom
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("offer_history")
     */
    private $check_in_date;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("offer_history")
     */
    private $check_out_date;

    /**
     * @ORM\Column(type="integer")
     * @Groups("offer_history")
     */
    private $persons_in_the_room;

    /**
     * @ORM\Column(type="integer")
     * @Groups("offer_history")
     */
    private $room_quantity;

    /**
     * @ORM\ManyToOne(targetEntity=Offer::class, inversedBy="rooms")
     * @ORM\JoinColumn(nullable=false)
     */
    private $offer;

    /**
     * @ORM\ManyToOne(targetEntity=RoomType::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups("offer_history")
     */
    private $roomType;

    /**
     * @ORM\OneToMany(targetEntity=OfferRoomRate::class, mappedBy="offerRoom", cascade={"persist", "remove"})
     */
    private $rates;

    /**
     * @ORM\Column(type="float")
     */
    private $averageRate;

    /**
     * @ORM\OneToMany(targetEntity=OfferRoomUpdates::class, mappedBy="offerRoom", orphanRemoval=true)
     */
    private $offerRoomUpdates;

    public function __construct()
    {
        $this->rates = new ArrayCollection();
        $this->offerRoomUpdates = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCheckInDate(): ?\DateTimeInterface
    {
        return $this->check_in_date;
    }

    public function setCheckInDate(\DateTimeInterface $check_in_date): self
    {
        $this->check_in_date = $check_in_date;

        return $this;
    }

    public function getCheckOutDate(): ?\DateTimeInterface
    {
        return $this->check_out_date;
    }

    public function setCheckOutDate(\DateTimeInterface $check_out_date): self
    {
        $this->check_out_date = $check_out_date;

        return $this;
    }

    public function getPersonsInTheRoom(): ?int
    {
        return $this->persons_in_the_room;
    }

    public function setPersonsInTheRoom(int $persons_in_the_room): self
    {
        $this->persons_in_the_room = $persons_in_the_room;

        return $this;
    }

    public function getRoomQuantity(): ?int
    {
        return $this->room_quantity;
    }

    public function setRoomQuantity(int $room_quantity): self
    {
        $this->room_quantity = $room_quantity;

        return $this;
    }

    public function getOffer(): ?Offer
    {
        return $this->offer;
    }

    public function setOffer(?Offer $offer): self
    {
        $this->offer = $offer;

        return $this;
    }

    public function getRoomType(): ?RoomType
    {
        return $this->roomType;
    }

    public function setRoomType(?RoomType $roomType): self
    {
        $this->roomType = $roomType;

        return $this;
    }

    /**
     * @return Collection|OfferRoomRate[]
     */
    public function getRates(): Collection
    {
        return $this->rates;
    }

    public function addRate(OfferRoomRate $rate): self
    {
        if (!$this->rates->contains($rate)) {
            $this->rates[] = $rate;
            $rate->setOfferRoom($this);
        }

        return $this;
    }

    public function removeRate(OfferRoomRate $rate): self
    {
        if ($this->rates->removeElement($rate)) {
            // set the owning side to null (unless already changed)
            if ($rate->getOfferRoom() === $this) {
                $rate->setOfferRoom(null);
            }
        }

        return $this;
    }

    public function getAverageRate(): ?float
    {
        return $this->averageRate;
    }

    public function setAverageRate(float $averageRate): self
    {
        $this->averageRate = $averageRate;

        return $this;
    }

    /**
     * @return Collection<int, OfferRoomUpdates>
     */
    public function getOfferRoomUpdates(): Collection
    {
        return $this->offerRoomUpdates;
    }

    public function addOfferRoomUpdate(OfferRoomUpdates $offerRoomUpdate): self
    {
        if (!$this->offerRoomUpdates->contains($offerRoomUpdate)) {
            $this->offerRoomUpdates[] = $offerRoomUpdate;
            $offerRoomUpdate->setOfferRoom($this);
        }

        return $this;
    }

    public function removeOfferRoomUpdate(OfferRoomUpdates $offerRoomUpdate): self
    {
        if ($this->offerRoomUpdates->removeElement($offerRoomUpdate)) {
            // set the owning side to null (unless already changed)
            if ($offerRoomUpdate->getOfferRoom() === $this) {
                $offerRoomUpdate->setOfferRoom(null);
            }
        }

        return $this;
    }
}
