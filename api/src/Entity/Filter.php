<?php

namespace App\Entity;

use App\Repository\FilterRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FilterRepository::class)
 */
class Filter
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $pax_max;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $pax_min;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $night_min;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $night_max;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_max;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_min;

    /**
     * @ORM\ManyToOne(targetEntity=Property::class, inversedBy="filters")
     * @ORM\JoinColumn(nullable=false)
     */
    private $property;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $occupancy_min;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $occupancy_max;

    /**
     * @ORM\ManyToOne(targetEntity=Age::class, inversedBy="filters")
     */
    private $age_min;

    /**
     * @ORM\ManyToOne(targetEntity=Age::class, inversedBy="filters")
     */
    private $age_max;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $daySelection = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPaxMax(): ?int
    {
        return $this->pax_max;
    }

    public function setPaxMax(?int $pax_max): self
    {
        $this->pax_max = $pax_max;

        return $this;
    }

    public function getPaxMin(): ?int
    {
        return $this->pax_min;
    }

    public function setPaxMin(?int $pax_min): self
    {
        $this->pax_min = $pax_min;

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getNightMin(): ?int
    {
        return $this->night_min;
    }

    public function setNightMin(?int $night_min): self
    {
        $this->night_min = $night_min;

        return $this;
    }

    public function getNightMax(): ?int
    {
        return $this->night_max;
    }

    public function setNightMax(?int $night_max): self
    {
        $this->night_max = $night_max;

        return $this;
    }

    public function getDateMax(): ?\DateTimeInterface
    {
        return $this->date_max;
    }

    public function setDateMax(?\DateTimeInterface $date_max): self
    {
        $this->date_max = $date_max;

        return $this;
    }

    public function getDateMin(): ?\DateTimeInterface
    {
        return $this->date_min;
    }

    public function setDateMin(?\DateTimeInterface $date_min): self
    {
        $this->date_min = $date_min;

        return $this;
    }

    public function getProperty(): ?Property
    {
        return $this->property;
    }

    public function setProperty(?Property $property): self
    {
        $this->property = $property;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getOccupancyMin(): ?int
    {
        return $this->occupancy_min;
    }

    public function setOccupancyMin(?int $occupancy_min): self
    {
        $this->occupancy_min = $occupancy_min;

        return $this;
    }

    public function getOccupancyMax(): ?int
    {
        return $this->occupancy_max;
    }

    public function setOccupancyMax(?int $occupancy_max): self
    {
        $this->occupancy_max = $occupancy_max;

        return $this;
    }

    public function getAgeMin(): ?Age
    {
        return $this->age_min;
    }

    public function setAgeMin(?Age $age_min): self
    {
        $this->age_min = $age_min;

        return $this;
    }

    public function getAgeMax(): ?Age
    {
        return $this->age_max;
    }

    public function setAgeMax(?Age $age_max): self
    {
        $this->age_max = $age_max;

        return $this;
    }

    public function getDaySelection(): ?array
    {
        return $this->daySelection;
    }

    public function setDaySelection(?array $daySelection): self
    {
        $this->daySelection = $daySelection;

        return $this;
    }
}
