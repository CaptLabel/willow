<?php

namespace App\Entity;

use App\Repository\AgeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AgeRepository::class)
 */
class Age
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\OneToMany(targetEntity=Enquiry::class, mappedBy="Age")
     */
    private $enquiry;

    /**
     * @ORM\ManyToOne(targetEntity=Brand::class, inversedBy="ages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $brand;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $min;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $max;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $formCode;

    public function __construct()
    {
        $this->enquiry = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection|Enquiry[]
     */
    public function getEnquiry(): Collection
    {
        return $this->enquiry;
    }

    public function addEnquiry(Enquiry $enquiry): self
    {
        if (!$this->enquiry->contains($enquiry)) {
            $this->enquiry[] = $enquiry;
            $enquiry->setAge($this);
        }

        return $this;
    }

    public function removeEnquiry(Enquiry $enquiry): self
    {
        if ($this->enquiry->removeElement($enquiry)) {
            // set the owning side to null (unless already changed)
            if ($enquiry->getAge() === $this) {
                $enquiry->setAge(null);
            }
        }

        return $this;
    }

    public function getBrand(): ?Brand
    {
        return $this->brand;
    }

    public function setBrand(?Brand $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    public function getMin(): ?int
    {
        return $this->min;
    }

    public function setMin(?int $min): self
    {
        $this->min = $min;

        return $this;
    }

    public function getMax(): ?int
    {
        return $this->max;
    }

    public function setMax(?int $max): self
    {
        $this->max = $max;

        return $this;
    }

    public function getFormCode(): ?string
    {
        return $this->formCode;
    }

    public function setFormCode(?string $formCode): self
    {
        $this->formCode = $formCode;

        return $this;
    }
}
