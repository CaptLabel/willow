<?php

namespace App\Entity;

use App\Repository\OfferRoomUpdatesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OfferRoomUpdatesRepository::class)
 */
class OfferRoomUpdates
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=RoomType::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $roomType;

    /**
     * @ORM\Column(type="integer")
     */
    private $roomQuantity;

    /**
     * @ORM\Column(type="datetime")
     */
    private $checkInDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $checkOutDate;

    /**
     * @ORM\Column(type="integer")
     */
    private $persons;

    /**
     * @ORM\Column(type="float")
     */
    private $average;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity=OfferRoom::class, inversedBy="offerRoomUpdates")
     * @ORM\JoinColumn(nullable=false)
     */
    private $offerRoom;

    public function __construct()
    {
        $this->updatedAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRoomType(): ?RoomType
    {
        return $this->roomType;
    }

    public function setRoomType(?RoomType $roomType): self
    {
        $this->roomType = $roomType;

        return $this;
    }

    public function getRoomQuantity(): ?int
    {
        return $this->roomQuantity;
    }

    public function setRoomQuantity(int $roomQuantity): self
    {
        $this->roomQuantity = $roomQuantity;

        return $this;
    }

    public function getCheckInDate(): ?\DateTimeInterface
    {
        return $this->checkInDate;
    }

    public function setCheckInDate(\DateTimeInterface $checkInDate): self
    {
        $this->checkInDate = $checkInDate;

        return $this;
    }

    public function getCheckOutDate(): ?\DateTimeInterface
    {
        return $this->checkOutDate;
    }

    public function setCheckOutDate(\DateTimeInterface $checkOutDate): self
    {
        $this->checkOutDate = $checkOutDate;

        return $this;
    }

    public function getPersons(): ?int
    {
        return $this->persons;
    }

    public function setPersons(int $persons): self
    {
        $this->persons = $persons;

        return $this;
    }

    public function getAverage(): ?float
    {
        return $this->average;
    }

    public function setAverage(float $average): self
    {
        $this->average = $average;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getOfferRoom(): ?OfferRoom
    {
        return $this->offerRoom;
    }

    public function setOfferRoom(?OfferRoom $offerRoom): self
    {
        $this->offerRoom = $offerRoom;

        return $this;
    }
}
