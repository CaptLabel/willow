<?php

namespace App\Entity;

use App\Repository\BrandRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BrandRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Brand
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\OneToMany(targetEntity=Property::class, mappedBy="brand")
     */
    private $properties;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="brand")
     */
    private $users;

    /**
     * @ORM\Column(type="datetime")
     */
    private $creation_date;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\OneToMany(targetEntity=Age::class, mappedBy="brand")
     */
    private $ages;

    /**
     * @ORM\OneToMany(targetEntity=Status::class, mappedBy="brand", cascade={"persist"})
     */
    private $statuses;

    /**
     * @ORM\OneToMany(targetEntity=Company::class, mappedBy="brand", orphanRemoval=true)
     */
    private $companies;

    /**
     * @ORM\ManyToOne(targetEntity=PropertyManagementSystem::class)
     */
    private $pmsGetData;

    /**
     * @ORM\ManyToOne(targetEntity=PropertyManagementSystem::class)
     */
    private $pmsPostData;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $apiKeyGetData;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $apiKeyPostData;

    /**
     * @ORM\OneToMany(targetEntity=BreakdownPreferences::class, mappedBy="brand", orphanRemoval=true)
     */
    private $breakdownPreferences;

    public function __construct()
    {
        $this->properties = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->ages = new ArrayCollection();
        $this->statuses = new ArrayCollection();
        $this->companies = new ArrayCollection();
        $this->breakdownPreferences = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection|Property[]
     */
    public function getProperties(): Collection
    {
        return $this->properties;
    }

    public function addProperty(Property $property): self
    {
        if (!$this->properties->contains($property)) {
            $this->properties[] = $property;
            $property->setBrand($this);
        }

        return $this;
    }

    public function removeProperty(Property $property): self
    {
        if ($this->properties->removeElement($property)) {
            // set the owning side to null (unless already changed)
            if ($property->getBrand() === $this) {
                $property->setBrand(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setBrand($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getBrand() === $this) {
                $user->setBrand(null);
            }
        }

        return $this;
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creation_date;
    }

    public function setCreationDate(\DateTimeInterface $creation_date): self
    {
        $this->creation_date = $creation_date;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue(): void
    {
        $this->creation_date = new \DateTime();
    }

    /**
     * @return Collection|Age[]
     */
    public function getAges(): Collection
    {
        return $this->ages;
    }

    public function addAge(Age $age): self
    {
        if (!$this->ages->contains($age)) {
            $this->ages[] = $age;
            $age->setBrand($this);
        }

        return $this;
    }

    public function removeAge(Age $age): self
    {
        if ($this->ages->removeElement($age)) {
            // set the owning side to null (unless already changed)
            if ($age->getBrand() === $this) {
                $age->setBrand(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Status[]
     */
    public function getStatuses(): Collection
    {
        return $this->statuses;
    }

    public function addStatus(Status $status): self
    {
        if (!$this->statuses->contains($status)) {
            $this->statuses[] = $status;
            $status->setBrand($this);
        }

        return $this;
    }

    public function removeStatus(Status $status): self
    {
        if ($this->statuses->removeElement($status)) {
            // set the owning side to null (unless already changed)
            if ($status->getBrand() === $this) {
                $status->setBrand(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Company[]
     */
    public function getCompanies(): Collection
    {
        return $this->companies;
    }

    public function addCompany(Company $company): self
    {
        if (!$this->companies->contains($company)) {
            $this->companies[] = $company;
            $company->setBrand($this);
        }

        return $this;
    }

    public function removeCompany(Company $company): self
    {
        if ($this->companies->removeElement($company)) {
            // set the owning side to null (unless already changed)
            if ($company->getBrand() === $this) {
                $company->setBrand(null);
            }
        }

        return $this;
    }

    public function getPmsGetData(): ?PropertyManagementSystem
    {
        return $this->pmsGetData;
    }

    public function setPmsGetData(?PropertyManagementSystem $pmsGetData): self
    {
        $this->pmsGetData = $pmsGetData;

        return $this;
    }

    public function getPmsPostData(): ?PropertyManagementSystem
    {
        return $this->pmsPostData;
    }

    public function setPmsPostData(?PropertyManagementSystem $pmsPostData): self
    {
        $this->pmsPostData = $pmsPostData;

        return $this;
    }

    public function getApiKeyGetData(): ?string
    {
        return $this->apiKeyGetData;
    }

    public function setApiKeyGetData(?string $apiKeyGetData): self
    {
        $this->apiKeyGetData = $apiKeyGetData;

        return $this;
    }

    public function getApiKeyPostData(): ?string
    {
        return $this->apiKeyPostData;
    }

    public function setApiKeyPostData(?string $apiKeyPostData): self
    {
        $this->apiKeyPostData = $apiKeyPostData;

        return $this;
    }

    /**
     * @return Collection<int, BreakdownPreferences>
     */
    public function getBreakdownPreferences(): Collection
    {
        return $this->breakdownPreferences;
    }

    public function addBreakdownPreference(BreakdownPreferences $breakdownPreference): self
    {
        if (!$this->breakdownPreferences->contains($breakdownPreference)) {
            $this->breakdownPreferences[] = $breakdownPreference;
            $breakdownPreference->setBrand($this);
        }

        return $this;
    }

    public function removeBreakdownPreference(BreakdownPreferences $breakdownPreference): self
    {
        if ($this->breakdownPreferences->removeElement($breakdownPreference)) {
            // set the owning side to null (unless already changed)
            if ($breakdownPreference->getBrand() === $this) {
                $breakdownPreference->setBrand(null);
            }
        }

        return $this;
    }
}
