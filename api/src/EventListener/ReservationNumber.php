<?php

namespace App\EventListener;

use App\Entity\Enquiry;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class ReservationNumber
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if ($entity instanceof Enquiry) {
            $property = $entity->getProperty();
            $propertyId = $property->getId();
            $lastEnquiryNb = $this->entityManager->getRepository('App:Enquiry')->findLastOneCreated($propertyId);
            $brandId = $property->getBrand()->getId();
            if ($lastEnquiryNb != null) {
                $starting_nb = $lastEnquiryNb->getReservationNumber();
                $reservationNumber = $starting_nb + 1;
            } else {
                $reservationNumber = $brandId . $propertyId . 1;
            }
            $entity->setReservationNumber($reservationNumber);
        }
    }
}
